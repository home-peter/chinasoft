package com.eat.service;

import com.eat.dao.ItemDao;
import com.eat.dao.OrderItemDao;
import com.eat.dao.ShopDao;
import com.eat.dao.UserDao;
import com.eat.dao.impl.ItemDaoImpl;
import com.eat.dao.impl.OrderItemDaoImpl;
import com.eat.dao.impl.ShopDaoImpl;
import com.eat.dao.impl.UserDaoImpl;
import com.eat.domain.*;
import com.eat.service.impl.OrderItemServiceImpl;
import com.eat.service.impl.OrderServiceImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public class OrderServiceTest {
    private UserDao userDao = new UserDaoImpl();
    private ShopDao shopDao = new ShopDaoImpl();
    private ItemDao itemDao = new ItemDaoImpl();
    private OrderService orderService = new OrderServiceImpl();
    private OrderItemService orderItemService = new OrderItemServiceImpl();

    @Test
    public void mockBuy() {
        // 用户登录
        User user = userDao.findOne(4L);
        // 用户选择商店
        Shop shop = shopDao.findOne(3L);

        // 用户选择菜品,并产生订单项目
        List<OrderItem> orderItemList = new ArrayList<>();
        OrderItem orderItem1 = orderItemService.createOrderItem(itemDao.findOne(3L), 1);
        OrderItem orderItem2 = orderItemService.createOrderItem(itemDao.findOne(4L), 1);
        orderItemList.add(orderItem1);
        orderItemList.add(orderItem2);
        // 确认购买,产生订单和订单项目
        orderService.createFoodOrder(user,shop,orderItemList);
    }

    @Test
    public void mockDelete() {
        orderService.deleteByUserId(4L);
    }

    @Test
    public void showDetail() {
        List<OrderInfo> orderInfoList = orderService.findByOrderId(12L);
        for (OrderInfo orderInfo : orderInfoList) {
            System.out.println(orderInfo);
        }
    }
}
