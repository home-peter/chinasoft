package com.eat.dao;

import com.eat.dao.impl.OrderItemDaoImpl;
import com.eat.domain.OrderItem;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public class OrderItemDaoTest {
    private OrderItemDao orderItemDao = new OrderItemDaoImpl();

    @Test
    public void insert() {
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderId(1L);
        orderItem.setItemId(2L);
        orderItem.setCount(1);
        orderItem.setItemPrice(new BigDecimal(50));
        orderItemDao.insert(orderItem);
    }

    @Test
    public void update() {
        OrderItem orderItem = orderItemDao.findOne(3L);
        orderItem.setItemPrice(new BigDecimal(100));
        orderItemDao.update(orderItem);
    }

    @Test
    public void findOne() {
        OrderItem orderItem = orderItemDao.findOne(3L);
    }

    @Test
    public void findAll() {
        List<OrderItem> orderItemList = orderItemDao.findAll();
    }

    @Test
    public void delete() {
        orderItemDao.delete(3L);
    }
}
