package com.eat.dao;

import com.eat.dao.impl.OrderDaoImpl;
import com.eat.dao.impl.ShopDaoImpl;
import com.eat.domain.Order;
import com.eat.domain.Shop;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * Created by Fang on 2018/4/17.
 */
public class ShopServiceDaoTest {
    private ShopDao shopDao = new ShopDaoImpl();

    @Test
    public void findOne() {
        Shop shop = shopDao.findOne(1L);
        System.out.println(shop);
    }
    @Test
    public void findAll() {
        List<Shop> List = shopDao.getAllShop();
        System.out.println(List);
    }
    @Test
    public void add(){
        Shop shop = new Shop();
        shop.setShopName("王姐");
        shop.setShopAddress("水院");
        shop.setShopDescription("便宜");
        shopDao.add(shop);
    }
    @Test
    public void update(){
        Shop shop = shopDao.findOne(1L);
        shop.setShopName("xxx");
        shopDao.update(shop);
    }
    @Test
    public void delete(){
        shopDao.delete(3L);
    }
}
