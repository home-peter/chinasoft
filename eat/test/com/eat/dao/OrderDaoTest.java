package com.eat.dao;

import com.eat.dao.impl.OrderDaoImpl;
import com.eat.domain.Order;
import com.eat.domain.OrderInfo;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public class OrderDaoTest {
    private OrderDao orderDao = new OrderDaoImpl();

    @Test
    public void insert() {
        Order order = new Order();
        order.setUserId(1L);
        order.setShopId(1L);
        order.setAddress("7号楼");
        order.setStatus(1);
        order.setDate(new Date());
        order.setPrice(new BigDecimal(500));
        orderDao.insert(order);
    }

    @Test
    public void update() {
        Order order = orderDao.findOne(1L);
        order.setAddress("9号楼");
        orderDao.update(order);
    }

    @Test
    public void findOne() {
        Order order = orderDao.findOne(1L);
    }

    @Test
    public void findAll() {
        List<Order> orderList = orderDao.findAll();
    }
    @Test
    public void findByOrderId(){
        List<OrderInfo> orderInfoList = orderDao.findByOrderId(1L);
    }
}
