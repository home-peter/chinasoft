package com.eat.dao;

import com.eat.dao.impl.ShopItemDaoImpl;
import com.eat.domain.Item;
import com.eat.domain.ShopItem;
import org.junit.Test;

import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public class ShopItemDaoTest {
    private ShopItemDao shopItemDao = new ShopItemDaoImpl();

    @Test
    public void findOne() {
        ShopItem shopItem = shopItemDao.findOne(4L);
    }

    @Test
    public void findAll() {
        List<ShopItem> shopItemList = shopItemDao.findAll();
    }

    @Test
    public void findByShopId() {
        List<Item> itemList = shopItemDao.findByShopId(1L);
    }

    @Test
    public void insert() {
        ShopItem shopItem = new ShopItem();
        shopItem.setShopId(1L);
        shopItem.setItemId(3L);
        shopItemDao.insert(shopItem);
    }

    @Test
    public void update() {
        ShopItem shopItem = shopItemDao.findOne(4L);
        shopItem.setShopId(5L);
        shopItemDao.update(shopItem);
    }

    @Test
    public void delete() {
        shopItemDao.delete(4L);
    }

    @Test
    public void deleteByShopId() {
        shopItemDao.deleteByShopId(2);
    }
}
