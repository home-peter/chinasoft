package com.eat.dao;

import com.eat.dao.impl.ItemDaoImpl;
import com.eat.domain.Item;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by liufeiyang on 2018/4/17.
 */

public class ItemDaoTest {
    ItemDao itemDao= new ItemDaoImpl();
    @Test
    public void insert(){
        Item item=new Item();
        item.setItemName("大萝卜");
        item.setItemPrice(new BigDecimal(500));
        item.setItemDescription("一颗大白菜");
        Date date=new Date();
        item.setGmtCreate(date);
        itemDao.insert(item);
    }
    @Test
    public void findone(){
        Item item=itemDao.findOne(3L);
        System.out.println(item.toString());
    }
    @Test
    public void findall(){
        List<Item> itemList=itemDao.findAll();
        for(int i=0;i<itemList.size();i++){
        System.out.println(itemList.get(i));
        }
    }
    @Test
    public void update(){
        Item item=new Item();
        item.setItemId(3L);
        item.setItemPrice(new BigDecimal(300));
        item.setItemDescription("超级大白菜");
        itemDao.update(item);
        Item newitem=new Item();
        newitem=itemDao.findOne(3L);
        System.out.println(newitem.toString());
    }
    @Test
    public void delete(){
        itemDao.delete(3L);
    }
}
