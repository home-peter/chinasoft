package com.eat.dao;

import com.eat.dao.impl.OrderDaoImpl;
import com.eat.dao.impl.UserDaoImpl;
import com.eat.domain.Order;
import com.eat.domain.User;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ZH on 2018/4/17.
 */
public class UserDaoTest {
    private UserDao userDao = new UserDaoImpl();
    @Test
    public void findOne() {
        User user = userDao.findOne(1L);
    }
    @Test
    public void findAll(){
        List<User> list = new ArrayList<User>();
        list = userDao.findAll();
    }
    @Test
    public void insert() {
        User user = new User();
        user.setAccountName("1");
        user.setPassword("1");
        user.setUserName("zmy");
        user.setUserPhone("11111");
        user.setUserAddress("111");
        user.setGmtCreate(new Date());
        userDao.insert(user);
    }
    @Test
    public void delete() {
        User user = new User();
        userDao.delete(1L);
    }
    @Test
    public void update() {
        User user = userDao.findOne(2L);
        user.setUserAddress("9号楼");
        userDao.update(user);
    }
}
