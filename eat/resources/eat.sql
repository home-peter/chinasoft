/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.14-log : Database - eat_dev
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`eat_dev` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `eat_dev`;

/*Table structure for table `item` */

DROP TABLE IF EXISTS `item`;

CREATE TABLE `item` (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `item_name` varchar(255) DEFAULT NULL COMMENT '商品名',
  `item_price` decimal(5,1) DEFAULT NULL COMMENT '商品价格(最高9999.9)',
  `item_description` varchar(255) DEFAULT NULL COMMENT '商品描述',
  `gmt_create` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `item` */

insert  into `item`(`item_id`,`item_name`,`item_price`,`item_description`,`gmt_create`,`gmt_modified`) values (3,'鸡腿堡+鸡肉卷+可乐','88.0','难吃','2018-04-19 08:49:13','2018-04-19 08:49:17'),(4,'人气甜辣炸鸡套餐+可口可乐','40.9','5只小鸡腿+8-10根年糕+可口可乐','2018-04-17 20:57:07',NULL),(5,'奥尔良堡+奥尔良烤翅+可乐','39.9','人气套餐','2018-04-17 20:58:43',NULL),(6,'孜然鸡排咖喱饭+烤肠','50.8','一份淋在饭上的咖喱汁再加上秘制鸡排','2018-04-17 21:00:35',NULL),(7,'鳕鱼排咖喱饭+烤肠','50.8','一份淋在饭上的咖喱汁再加上秘制鱼排','2018-04-17 21:01:28',NULL),(8,'荷包蛋','2.0','单点不配送','2018-04-17 21:02:07',NULL),(9,'双热狗+双配菜+半鸡蛋+冰红茶盖浇饭','23.0','12','2018-04-19 15:23:07','2018-04-19 15:23:11'),(10,'秘制卤肉饭+双配菜+卤蛋','37.2','好评如潮','2018-04-17 21:05:05',NULL),(13,'盐水鸭','57.6','一只盐水鸭','2018-04-19 10:24:53',NULL),(14,'黄金品','19.0','黄金品很好吃','2018-04-20 09:39:40',NULL);

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单项编号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户编号',
  `shop_id` int(11) DEFAULT NULL COMMENT '商家编号',
  `address` varchar(255) DEFAULT NULL COMMENT '送餐地址',
  `status` int(11) DEFAULT NULL COMMENT '订单状态',
  `date` date DEFAULT NULL COMMENT '送餐时间',
  `price` decimal(10,1) DEFAULT NULL COMMENT '订单总金额',
  `gmt_create` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '更改时间',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `order` */

insert  into `order`(`order_id`,`user_id`,`shop_id`,`address`,`status`,`date`,`price`,`gmt_create`,`gmt_modified`) values (7,4,4,'9号楼606',0,'2018-04-17','39.9','2018-04-17 21:19:26','2018-04-17 21:19:23'),(9,2,3,'8号楼101',1,'2018-04-19','169.8','2018-04-19 09:39:02','2018-04-19 09:39:02'),(10,2,5,'8号楼101',3,'2018-04-19','216.4','2018-04-20 09:07:35','2018-04-20 09:07:36'),(11,2,3,'8号楼101',1,'2018-04-19','176.0','2018-04-19 15:17:07','2018-04-19 15:17:08'),(12,2,5,'8号楼101',0,'2018-04-20','171.8','2018-04-20 09:30:35','2018-04-20 09:30:35'),(13,2,5,'8号楼101',0,'2018-04-20','115.2','2018-04-20 09:30:56','2018-04-20 09:30:57'),(14,2,5,'8号楼101',2,'2018-04-20','23.0','2018-04-20 09:34:18','2018-04-20 09:34:19');

/*Table structure for table `orderitem` */

DROP TABLE IF EXISTS `orderitem`;

CREATE TABLE `orderitem` (
  `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单项编号',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单项所属订单',
  `item_id` bigint(20) DEFAULT NULL COMMENT '商品编号',
  `count` int(11) DEFAULT NULL COMMENT '商品数量',
  `item_price` decimal(5,1) DEFAULT NULL COMMENT '商品价格',
  PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `orderitem` */

insert  into `orderitem`(`order_item_id`,`order_id`,`item_id`,`count`,`item_price`) values (9,7,5,1,'39.9'),(12,9,3,1,'88.0'),(13,9,4,2,'81.8'),(14,10,9,2,'67.6'),(15,10,10,4,'148.8'),(16,11,3,2,'176.0'),(17,12,9,1,'23.0'),(18,12,10,4,'148.8'),(19,13,13,2,'115.2'),(20,14,9,1,'23.0');

/*Table structure for table `shop` */

DROP TABLE IF EXISTS `shop`;

CREATE TABLE `shop` (
  `shop_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商家编号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '商家的用户帐户',
  `shop_name` varchar(255) DEFAULT NULL COMMENT '商家名',
  `shop_address` varchar(255) DEFAULT NULL COMMENT '商家地址',
  `shop_description` varchar(255) DEFAULT NULL COMMENT '商家详情介绍',
  `gmt_create` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `shop` */

insert  into `shop`(`shop_id`,`user_id`,`shop_name`,`shop_address`,`shop_description`,`gmt_create`,`gmt_modified`) values (3,4,'汉堡工坊','理工店','汉堡店','2018-04-18 10:53:10',NULL),(4,5,'吉膳客','金沙店','咖喱饭','2018-04-18 12:19:17',NULL),(5,6,'洋总麻辣烫','水院','洋总亲手伸锅里捞出来的，好吃','2018-04-19 00:00:00','2018-04-19 15:16:15'),(6,13,'k','k','k','2018-04-20 09:53:19',NULL);

/*Table structure for table `shop_item` */

DROP TABLE IF EXISTS `shop_item`;

CREATE TABLE `shop_item` (
  `shop_item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商家-商品 中间表编号',
  `shop_id` bigint(20) DEFAULT NULL COMMENT '商家编号',
  `item_id` bigint(20) DEFAULT NULL COMMENT '商品编号',
  PRIMARY KEY (`shop_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `shop_item` */

insert  into `shop_item`(`shop_item_id`,`shop_id`,`item_id`) values (3,3,3),(4,3,4),(5,3,5),(6,4,6),(7,4,7),(8,4,8),(9,5,9),(10,5,10),(13,5,13),(14,5,14);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `accountname` varchar(20) DEFAULT NULL COMMENT '账号',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  `username` varchar(10) DEFAULT NULL COMMENT '用户姓名',
  `type` int(11) DEFAULT NULL COMMENT '用户类型(0普通用户,1商家用户,2管理员)',
  `user_phone` varchar(11) DEFAULT NULL COMMENT '用户手机号码',
  `user_address` varchar(50) DEFAULT NULL COMMENT '用户地址',
  `gmt_create` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '更改时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`user_id`,`accountname`,`password`,`username`,`type`,`user_phone`,`user_address`,`gmt_create`,`gmt_modified`) values (2,'adm','adm','张明远先生',0,'18100177470','8号楼101','2018-04-19 14:17:36',NULL),(3,'hhh','hhh','汉堡工坊老板',1,'18100177491','8号楼202','2018-04-18 12:18:56',NULL),(4,'axe','axe','方挺强先生',2,'18100177111','8号楼303','2018-04-19 14:17:45',NULL),(5,'jjj','jjj','吉膳客老板',1,'18100177222','金沙湖','2018-04-18 12:18:01',NULL),(6,'lll','lll','龙猫美食老板',1,'18100177333','下沙天街','2018-04-19 14:19:46',NULL),(12,'lfy','123456',NULL,0,'18757559999','浙江水利水电','2018-04-19 16:16:15',NULL),(13,'kkk','kkk','k',1,'k','k','2018-04-20 09:53:16',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
