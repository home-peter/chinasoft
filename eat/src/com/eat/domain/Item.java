package com.eat.domain;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zmy on 2018/4/17.
 */
public class Item {
    // 商品编号
    private long itemId;
    // 商品名
    private String itemName;
    // 商品价格
    private BigDecimal itemPrice;
    // 商品详细描述
    private String itemDescription;
    // 产生商品时间
    private Date gmtCreate;
    // 修改商品时间
    private Date gmtModified;

    public Item() {
    }

    public Item(long itemId, String itemName, BigDecimal itemPrice, String itemDescription, Date gmtCreate, Date gmtModified) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemDescription = itemDescription;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemId=" + itemId +
                ", itemName='" + itemName + '\'' +
                ", itemPrice=" + itemPrice +
                ", itemDescription='" + itemDescription + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}
