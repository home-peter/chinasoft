package com.eat.domain;

import java.math.BigDecimal;

/**
 * 订单项
 * Created by zmy on 2018/4/17.
 */
public class OrderItem {
    private long orderItemId;
    private long orderId;
    private long itemId;
    private int count;
    private BigDecimal itemPrice;

    public OrderItem() {
    }

    public OrderItem(long orderItemId, long orderId, long itemId, int count, BigDecimal itemPrice) {
        this.orderItemId = orderItemId;
        this.orderId = orderId;
        this.itemId = itemId;
        this.count = count;
        this.itemPrice = itemPrice;
    }

    public long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "orderItemId=" + orderItemId +
                ", orderId=" + orderId +
                ", itemId=" + itemId +
                ", count=" + count +
                ", itemPrice=" + itemPrice +
                '}';
    }
}
