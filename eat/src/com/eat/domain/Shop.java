package com.eat.domain;

import java.util.Date;

/**
 * Created by Fang on 2018/4/17.
 */
public class Shop {
    // 商店id
    private long shopId;
    // 商家对应的用户
    private long userId;
    //商店名字
    private String shopName;
    //商店地址
    private String shopAddress;
    //商店描述
    private String shopDescription;
    //商店建立日期
    private Date gmtCreate;
    //商店修改日期
    private Date gmtModified;

    public Shop() {
    }

    public Shop(long shopId, String shopName, String shopAddress, String shopDescription, Date gmtCreate, Date gmtModified) {
        this.shopId = shopId;
        this.shopName = shopName;
        this.shopAddress = shopAddress;
        this.shopDescription = shopDescription;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public Shop(long shopId, long userId, String shopName, String shopAddress, String shopDescription, Date gmtCreate, Date gmtModified) {
        this.shopId = shopId;
        this.userId = userId;
        this.shopName = shopName;
        this.shopAddress = shopAddress;
        this.shopDescription = shopDescription;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public Shop(long userId, String shopName, String shopAddress, String shopDescription, Date gmtCreate) {
        this.userId = userId;
        this.shopName = shopName;
        this.shopAddress = shopAddress;
        this.shopDescription = shopDescription;
        this.gmtCreate = gmtCreate;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopDescription() {
        return shopDescription;
    }

    public void setShopDescription(String shopDescription) {
        this.shopDescription = shopDescription;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "shopId=" + shopId +
                ", userId=" + userId +
                ", shopName='" + shopName + '\'' +
                ", shopAddress='" + shopAddress + '\'' +
                ", shopDescription='" + shopDescription + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}
