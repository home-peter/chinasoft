package com.eat.domain;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zmy on 2018/4/17.
 */
public class OrderInfo {
    // 商品名
    private String itemName;
    // 商品价格
    private BigDecimal itemPrice;
    // 商品数量
    private int count;
    //　价格小计
    private BigDecimal total;
    // 商品详情信息
    private String itemDescription;
    // 订单创建日期
    private Date gmtCreate;
    // 订单修改日期
    private Date gmtModified;

    public OrderInfo() {
    }

    public OrderInfo(String itemName, BigDecimal itemPrice, int count, BigDecimal total, String itemDescription, Date gmtCreate, Date gmtModified) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.count = count;
        this.total = total;
        this.itemDescription = itemDescription;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "OrderInfo{" +
                "itemName='" + itemName + '\'' +
                ", itemPrice=" + itemPrice +
                ", count=" + count +
                ", total=" + total +
                ", itemDescription='" + itemDescription + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}
