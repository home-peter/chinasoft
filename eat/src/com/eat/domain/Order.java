package com.eat.domain;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Created by liufeiyang on 2018/4/17.
 */
public class Order {
    //订单号
    private long orderId;
    //用户ID
    private long userId;
    //店铺ID
    private long shopId;
    //送餐地址
    private String address;
    //状态
    private int status;
    //日期
    private Date date;
    //订单价格
    private BigDecimal price;
    //订单生产时间
    private Date gmtCreate;
    //订单完成时间
    private Date gmtModified;

    public Order() {
    }

    public Order(long orderId, long userId, long shopId, String address, int statues, Date date, BigDecimal price, Date gmtCreate, Date gmtModified) {
        this.orderId = orderId;
        this.userId = userId;
        this.shopId = shopId;
        this.address = address;
        this.status = statues;
        this.date = date;
        this.price = price;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date GmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", userId=" + userId +
                ", shopId=" + shopId +
                ", address='" + address + '\'' +
                ", status=" + status +
                ", date=" + date +
                ", price=" + price +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}



