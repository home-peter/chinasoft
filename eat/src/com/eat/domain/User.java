package com.eat.domain;

import java.util.Date;

/**
 * Created by ZH on 2018/4/17.
 */
public class User {
    //用户ID
    private long userID;
    //用户账号
    private String accountName;
    //用户密码
    private String password;
    //用户姓名
    private String userName;
    // 用户类型(0普通用户,1商家用户,2管理员)
    private int type;
    //用户电话
    private String userPhone;
    //用户地址
    private String userAddress;
    //创建时间
    private Date gmtCreate;
    //更改时间
    private Date gmtModified;

    public User() {
    }

    public User(long userID, String accountName, String password, String userName, String userPhone, String userAddress, Date gmtCreate, Date gmtModified) {
        this.userID = userID;
        this.accountName = accountName;
        this.password = password;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userAddress = userAddress;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public User(long userID, String accountName, String password, String userName, int type, String userPhone, String userAddress, Date gmtCreate, Date gmtModified) {
        this.userID = userID;
        this.accountName = accountName;
        this.password = password;
        this.userName = userName;
        this.type = type;
        this.userPhone = userPhone;
        this.userAddress = userAddress;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
    }

    public User(String accountName, String password, String userName, int type, String userPhone, String userAddress, Date gmtCreate) {
        this.accountName = accountName;
        this.password = password;
        this.userName = userName;
        this.type = type;
        this.userPhone = userPhone;
        this.userAddress = userAddress;
        this.gmtCreate = gmtCreate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                ", accountName='" + accountName + '\'' +
                ", password='" + password + '\'' +
                ", userName='" + userName + '\'' +
                ", type=" + type +
                ", userPhone='" + userPhone + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}

