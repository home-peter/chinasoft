package com.eat.domain;

/**
 * 商店-商品中间表
 * Created by zmy on 2018/4/17.
 */
public class ShopItem {
    // 商店-商品编号
    private long shopItemId;
    // 商店编号
    private long shopId;
    // 商品编号
    private long itemId;

    public ShopItem() {
    }

    public ShopItem(long shopItemId, long shopId, long itemId) {
        this.shopItemId = shopItemId;
        this.shopId = shopId;
        this.itemId = itemId;
    }

    public long getShopItemId() {
        return shopItemId;
    }

    public void setShopItemId(long shopItemId) {
        this.shopItemId = shopItemId;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    @Override
    public String toString() {
        return "ShopItem{" +
                "shopItemId=" + shopItemId +
                ", shopId=" + shopId +
                ", itemId=" + itemId +
                '}';
    }
}
