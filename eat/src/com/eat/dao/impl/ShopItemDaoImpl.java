package com.eat.dao.impl;

import com.eat.core.BaseDao;
import com.eat.dao.ShopItemDao;
import com.eat.domain.Item;
import com.eat.domain.ShopItem;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public class ShopItemDaoImpl extends BaseDao implements ShopItemDao {
    @Override
    public int insert(ShopItem ShopItem) {
        String sql = "insert into shop_item(shop_id,item_id) values(?,?)";
        long shopId = ShopItem.getShopId();
        long itemId = ShopItem.getItemId();
        return update(sql, shopId, itemId);
    }

    @Override
    public int update(ShopItem ShopItem) {
        String sql = "update shop_item set shop_id = ? , item_id = ? where shop_item_id = ?";
        long shopId = ShopItem.getShopId();
        long itemId = ShopItem.getItemId();
        long shopItemId = ShopItem.getShopItemId();
        return update(sql, shopId, itemId, shopItemId);
    }

    @Override
    public int delete(long ShopItemId) {
        String sql = "delete from shop_item where shop_item_id = ?";
        return update(sql, ShopItemId);
    }

    @Override
    public int deleteByShopId(long shopId) {
        String sql = "delete from shop_item where shop_id = ?";
        return update(sql, shopId);
    }

    @Override
    public ShopItem findOne(long ShopItemId) {
        String sql = "select * from shop_item where shop_item_id = ?";
        ResultSet resultSet = executeQuery(sql, ShopItemId);
        ShopItem shopItem = null;
        try {
            while (resultSet.next()) {
                long shopItemId = resultSet.getLong("shop_item_id");
                long shopId = resultSet.getLong("shop_id");
                long itemId = resultSet.getLong("item_id");
                shopItem = new ShopItem(shopItemId, shopId, itemId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return shopItem;
    }

    @Override
    public List<Item> findByShopId(long shopId) {
        String sql = "SELECT item.* " +
                "FROM shop_item si,item " +
                "WHERE si.`item_id` = item.`item_id` " +
                "AND si.`shop_id` = ?";
        ResultSet resultSet = executeQuery(sql, shopId);
        Item item = null;
        List<Item> itemList = new ArrayList<>();
        try {
            while (resultSet.next()) {
                long itemId = resultSet.getLong("item_id");
                String itemName = resultSet.getString("item_name");
                BigDecimal itemPrice = resultSet.getBigDecimal("item_price");
                String itemDescription = resultSet.getString("item_description");
                Date gmtCreate = resultSet.getDate("gmt_create");
                Date gmtModified = resultSet.getDate("gmt_modified");
                item = new Item(itemId, itemName, itemPrice, itemDescription, gmtCreate, gmtModified);
                itemList.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return itemList;
    }

    @Override
    public List<ShopItem> findAll() {
        String sql = "select * from shop_item";
        ResultSet resultSet = executeQuery(sql);
        ShopItem shopItem = null;
        List<ShopItem> shopItemList = new ArrayList<>();
        try {
            while (resultSet.next()) {
                long shopItemId = resultSet.getLong("shop_item_id");
                long shopId = resultSet.getLong("shop_id");
                long itemId = resultSet.getLong("item_id");
                shopItem = new ShopItem(shopItemId, shopId, itemId);
                shopItemList.add(shopItem);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return shopItemList;
    }
}
