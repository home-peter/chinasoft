package com.eat.dao.impl;

import com.eat.core.BaseDao;
import com.eat.dao.OrderDao;
import com.eat.domain.Order;
import com.eat.domain.OrderInfo;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public class OrderDaoImpl extends BaseDao implements OrderDao {
    @Override
    public int insert(Order order) {
        String sql = "insert into `order`(user_id,shop_id,address,status,date,price,gmt_create)" +
                "values(?,?,?,?,?,?,current_timestamp)";
        long userId = order.getUserId();
        long shopId = order.getShopId();
        String address = order.getAddress();
        int status = order.getStatus();
        Date date = order.getDate();
        BigDecimal price = order.getPrice();
        return update(sql, userId, shopId, address, status, date, price);
    }

    @Override
    public int update(Order order) {
        String sql = "update `order` set user_id = ?,shop_id = ?,address = ?,status = ?,date = ?,price = ?,gmt_modified = ?" +
                "where order_id = ?";
        return update(sql, order.getUserId(), order.getShopId(),
                order.getAddress(), order.getStatus(), order.getDate(),
                order.getPrice(), new Date(), order.getOrderId());
    }

    @Override
    public int delete(long orderId) {
        String sql = "delete from `order` where order_id = ?";
        return update(sql, orderId);
    }

    @Override
    public int deleteByUserId(long userId) {
        String sql = "delete from `order` where user_id = ?";
        return update(sql, userId);
    }

    @Override
    public Order findOne(long orderId) {
        String sql = "select * from `order` where order_id = ?";
        ResultSet resultSet = executeQuery(sql, orderId);
        Order order = null;
        try {
            while (resultSet.next()) {
                long oId = resultSet.getLong("order_id");
                long userId = resultSet.getLong("user_id");
                long shopId = resultSet.getLong("shop_id");
                String address = resultSet.getString("address");
                int status = resultSet.getInt("status");
                Date date = resultSet.getDate("date");
                BigDecimal price = resultSet.getBigDecimal("price");
                Date gmtCreate = resultSet.getDate("gmt_create");
                Date gmtModified = resultSet.getDate("gmt_modified");
                order = new Order(oId, userId, shopId, address, status, date, price, gmtCreate, gmtModified);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return order;
    }

    @Override
    public List<OrderInfo> findByOrderId(long orderId) {
        String sql = "SELECT i.item_name,i.item_price,i.item_description, " +
                "oi.count,oi.item_price total, " +
                "o.gmt_create,o.gmt_modified " +
                "FROM `order` o,orderitem oi,item i " +
                "WHERE o.`order_id` = oi.`order_id` " +
                "AND oi.item_id = i.`item_id` " +
                "AND o.`order_id` = ?";
        ResultSet resultSet = executeQuery(sql, orderId);
        List<OrderInfo> orderInfoList = new ArrayList<>();
        OrderInfo orderInfo = null;
        try {
            while (resultSet.next()) {
                String itemName = resultSet.getString("item_name");
                BigDecimal itemPrice = resultSet.getBigDecimal("item_price");
                String itemDescription = resultSet.getString("item_description");
                int count = resultSet.getInt("count");
                BigDecimal total = resultSet.getBigDecimal("total");
                Date gmtCreate = resultSet.getDate("gmt_create");
                Date gmtModified = resultSet.getDate("gmt_modified");
                orderInfo = new OrderInfo(itemName, itemPrice, count, total, itemDescription, gmtCreate, gmtModified);
                orderInfoList.add(orderInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return orderInfoList;
    }

    @Override
    public List<Order> findByUserId(long userId) {
        String sql = "select * from `order` where user_id = ?";
        ResultSet resultSet = executeQuery(sql, userId);
        List<Order> orderList = new ArrayList<>();
        Order order = null;
        try {
            while (resultSet.next()) {
                long orderId = resultSet.getLong("order_id");
                long userId1 = resultSet.getLong("user_id");
                long shopId = resultSet.getLong("shop_id");
                String address = resultSet.getString("address");
                int status = resultSet.getInt("status");
                Date date = resultSet.getDate("date");
                BigDecimal price = resultSet.getBigDecimal("price");
                Date gmtCreate = resultSet.getDate("gmt_create");
                Date gmtModified = resultSet.getDate("gmt_modified");
                order = new Order(orderId, userId1, shopId, address, status, date, price, gmtCreate, gmtModified);
                orderList.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return orderList;
    }

    @Override
    public List<Order> findAll() {
        String sql = "select * from `order`";
        ResultSet resultSet = executeQuery(sql);
        List<Order> orderList = new ArrayList<>();
        Order order = null;
        try {
            while (resultSet.next()) {
                long oId = resultSet.getLong("order_id");
                long userId = resultSet.getLong("user_id");
                long shopId = resultSet.getLong("shop_id");
                String address = resultSet.getString("address");
                int status = resultSet.getInt("status");
                Date date = resultSet.getDate("date");
                BigDecimal price = resultSet.getBigDecimal("price");
                Date gmtCreate = resultSet.getDate("gmt_create");
                Date gmtModified = resultSet.getDate("gmt_modified");
                order = new Order(oId, userId, shopId, address, status, date, price, gmtCreate, gmtModified);
                orderList.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return orderList;
    }
}
