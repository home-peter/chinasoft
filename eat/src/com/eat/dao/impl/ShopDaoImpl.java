package com.eat.dao.impl;

import com.eat.core.BaseDao;
import com.eat.dao.ShopDao;
import com.eat.domain.Item;
import com.eat.domain.Order;
import com.eat.domain.Shop;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Fang on 2018/4/17.
 */
public class ShopDaoImpl extends BaseDao implements ShopDao {
    //添加商店
    public int add(Shop shop) {
        String sql = "insert into shop(user_id,shop_name,shop_address,shop_description,gmt_create) value(?,?,?,?,?)";
        return super.update(sql, shop.getUserId(), shop.getShopName(), shop.getShopAddress(), shop.getShopDescription(), new Date());
    }

    //按序号来删除商店
    public int delete(long shopId) {
        String sql = "delete from shop where shop_id = ?";
        return super.update(sql, shopId);
    }

    //显示所有商店
    public List<Shop> getAllShop() {
        String sql = "select * from shop";
        ResultSet rst = super.executeQuery(sql);
        List<Shop> list = new ArrayList<Shop>();
        try {
            while (rst.next()) {
                int shopId = rst.getInt("shop_id");
                long userId = rst.getLong("user_id");
                String shopName = rst.getString("shop_name");
                String shopAddress = rst.getString("shop_address");
                String shopDescription = rst.getString("shop_description");
                Date gmtCreate = rst.getDate("gmt_create");
                Date gmtModified = rst.getDate("gmt_modified");
                Shop shop = new Shop(shopId, userId, shopName, shopAddress, shopDescription, gmtCreate, gmtModified);
                list.add(shop);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return list;
    }

    //更新商店的修改时间
    public int update(Shop shop) {
        String sql = "update `shop` set user_id = ?,shop_name = ?,shop_address = ?,shop_description = ?,gmt_create = ?,gmt_modified = ?" +
                "where shop_id = ?";
        return update(sql, shop.getUserId(), shop.getShopName(), shop.getShopAddress(), shop.getShopDescription(), shop.getGmtCreate(), shop.getGmtModified(), shop.getShopId());
    }

    //根据序号查询一个商店信息
    public Shop findOne(Long shopId) {
        String sql = "select * from `shop` where shop_id = ?";
        ResultSet rst = executeQuery(sql, shopId);
        Shop shop = null;
        try {
            while (rst.next()) {
                int sod = rst.getInt("shop_id");
                long userId = rst.getLong("user_id");
                String shopName = rst.getString("shop_name");
                String shopAddress = rst.getString("shop_address");
                String shopDescription = rst.getString("shop_description");
                Date gmtCreate = rst.getDate("gmt_create");
                Date gmtModified = rst.getDate("gmt_modified");
                shop = new Shop(sod, userId, shopName, shopAddress, shopDescription, gmtCreate, gmtModified);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return shop;
    }

    @Override
    public Shop findByUserId(long userId) {

        String sql = "select * from `shop` where user_id = ?";
        ResultSet rst = executeQuery(sql, userId);
        Shop shop = null;
        try {
            while (rst.next()) {
                int sod = rst.getInt("shop_id");
                long uId = rst.getLong("user_id");
                String shopName = rst.getString("shop_name");
                String shopAddress = rst.getString("shop_address");
                String shopDescription = rst.getString("shop_description");
                Date gmtCreate = rst.getDate("gmt_create");
                Date gmtModified = rst.getDate("gmt_modified");
                shop = new Shop(sod, uId, shopName, shopAddress, shopDescription, gmtCreate, gmtModified);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return shop;
    }

    //店家查看订单内容
    public List<Item> getAllItem(long orderId) {
        String sql = "SELECT item.* " +
                "FROM `item`,`order`,`orderitem` " +
                "where `order`.order_id =`orderitem`.order_id " +
                "AND `item`.item_id =`orderitem`.item_id" +
                "AND `order`.order_id = ?";
        ResultSet rst = super.executeQuery(sql, orderId);
        List<Item> list = new ArrayList<Item>();
        try {
            while (rst.next()) {
                Item item = new Item(rst.getLong("item_id"), rst.getString("item_name"), rst.getBigDecimal("item_price"), rst.getString("item_description"),
                        rst.getDate("gmt_create"), rst.getDate("gmt_modified"));
                list.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return list;

    }

    //店家查看订单
    public Order findOrder(long shopId) {
        String sql = "select * from `order` where shop_id = ?";
        ResultSet resultSet = executeQuery(sql, shopId);
        Order order = null;
        try {
            while (resultSet.next()) {
                long orderId = resultSet.getLong("order_id");
                long userId = resultSet.getLong("user_id");
                long sId = resultSet.getLong("shop_id");
                String address = resultSet.getString("address");
                int status = resultSet.getInt("status");
                Date date = resultSet.getDate("date");
                BigDecimal price = resultSet.getBigDecimal("price");
                Date gmtCreate = resultSet.getDate("gmt_create");
                Date gmtModified = resultSet.getDate("gmt_modified");
                order = new Order(orderId, userId, sId, address, status, date, price, gmtCreate, gmtModified);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return order;
    }
}
