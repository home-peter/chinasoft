package com.eat.dao.impl;

import com.eat.core.BaseDao;
import com.eat.dao.UserDao;
import com.eat.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ZH on 2018/4/17.
 */
public class UserDaoImpl extends BaseDao implements UserDao {
    @Override
    public int insert(User user) {
        String sql = "insert into user(accountname,password,username,type,user_phone,user_address,gmt_create) VALUES(?,?,?,?,?,?,current_timestamp);";
        return super.update(sql, user.getAccountName(), user.getPassword(), user.getUserName(), user.getType(), user.getUserPhone(), user.getUserAddress(), new Date());
    }

    @Override
    public int delete(Long userId) {
        String sql = "delete from user where user_id=?";
        return super.update(sql, userId);
    }

    @Override
    public int update(User user) {
        String sql = "update user set accountname=?,password=?,username=?,type=?,user_phone=?,user_address=?,gmt_modified=? where user_id=?";
        return super.update(sql, user.getAccountName(), user.getPassword(), user.getUserName(), user.getType(), user.getUserPhone(), user.getUserAddress(), new Date(), user.getUserID());
    }

    @Override
    public int count(String accountName, String password) {
        String sql = "select count(*) from user where accountname=? and password=?";
        ResultSet rst = super.executeQuery(sql, accountName, password);
        try {
            if (rst.next()) {
                return rst.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return 0;
    }

    @Override
    public int count1(String accountName) {
        String sql = "select count(*) from user where accountname=?";
        ResultSet rst = super.executeQuery(sql, accountName);
        try {
            if (rst.next()) {
                return rst.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return 0;
    }

    @Override
    public User findOne(Long userId) {
        String sql = "select * from user where user_id=?";
        ResultSet rst = super.executeQuery(sql, userId);
        User user = null;
        try {
            while (rst.next()) {
                long user_id = rst.getLong("user_id");
                String accountname = rst.getString("accountname");
                String password = rst.getString("password");
                String username = rst.getString("username");
                int type = rst.getInt("type");
                String user_phone = rst.getString("user_phone");
                String user_address = rst.getString("user_address");
                Date gmt_create = rst.getDate("gmt_create");
                Date gmt_modified = rst.getDate("gmt_modified");
                user = new User(user_id, accountname, password, username, type, user_phone, user_address, gmt_create, gmt_modified);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return user;
    }

    @Override
    public User findByAccountName(String accountName) {
        String sql = "select * from user where accountname=?";
        ResultSet rst = super.executeQuery(sql, accountName);
        User user = null;
        try {
            while (rst.next()) {
                long user_id = rst.getLong("user_id");
                String accountname = rst.getString("accountname");
                String password = rst.getString("password");
                String username = rst.getString("username");
                int type = rst.getInt("type");
                String user_phone = rst.getString("user_phone");
                String user_address = rst.getString("user_address");
                Date gmt_create = rst.getDate("gmt_create");
                Date gmt_modified = rst.getDate("gmt_modified");
                user = new User(user_id, accountname, password, username, type, user_phone, user_address, gmt_create, gmt_modified);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        String sql = "select * from user";
        ResultSet rst = super.executeQuery(sql);
        ArrayList<User> list = new ArrayList<User>();
        User user;
        try {
            while (rst.next()) {
                long user_id = rst.getLong("user_id");
                String accountname = rst.getString("accountname");
                String password = rst.getString("password");
                String username = rst.getString("username");
                int type = rst.getInt("type");
                String user_phone = rst.getString("user_phone");
                String user_address = rst.getString("user_address");
                Date gmt_create = rst.getDate("gmt_create");
                Date gmt_modified = rst.getDate("gmt_modified");
                user = new User(user_id, accountname, password, username, type, user_phone, user_address, gmt_create, gmt_modified);
                list.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return list;
    }
}
