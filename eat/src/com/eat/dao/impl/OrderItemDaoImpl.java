package com.eat.dao.impl;

import com.eat.core.BaseDao;
import com.eat.dao.OrderItemDao;
import com.eat.domain.OrderItem;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public class OrderItemDaoImpl extends BaseDao implements OrderItemDao {
    @Override
    public int insert(OrderItem orderItem) {
        String sql = "insert into orderitem(order_id,item_id,count,item_price) values(?,?,?,?)";
        long orderId = orderItem.getOrderId();
        long itemId = orderItem.getItemId();
        int count = orderItem.getCount();
        BigDecimal itemPrice = orderItem.getItemPrice();
        return update(sql, orderId, itemId, count, itemPrice);
    }

    @Override
    public int update(OrderItem orderItem) {
        String sql = "update orderitem set order_id = ?,item_id = ?,count = ?,item_price = ? where order_item_id";
        long orderId = orderItem.getOrderId();
        long itemId = orderItem.getItemId();
        int count = orderItem.getCount();
        BigDecimal itemPrice = orderItem.getItemPrice();
        return update(sql, orderId, itemId, count, itemPrice);
    }

    @Override
    public int delete(long orderItemId) {
        String sql = "delete from orderitem where order_item_id = ?";
        return update(sql, orderItemId);
    }

    @Override
    public int deleteByOrderId(long orderId) {
        String sql = "delete from orderitem where order_id = ?";
        return update(sql, orderId);
    }

    @Override
    public OrderItem findOne(long orderItemId) {
        String sql = "select * from orderitem where order_item_id = ?";
        ResultSet resultSet = executeQuery(sql, orderItemId);
        OrderItem orderItem = null;
        try {
            while (resultSet.next()) {
                long orderItemId1 = resultSet.getLong("order_item_id");
                long orderId = resultSet.getLong("order_id");
                long itemId = resultSet.getLong("item_id");
                int count = resultSet.getInt("count");
                BigDecimal itemPrice = resultSet.getBigDecimal("item_price");
                orderItem = new OrderItem(orderItemId1, orderId, itemId, count, itemPrice);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return orderItem;
    }

    @Override
    public List<OrderItem> findAll() {
        String sql = "select * from orderitem";
        ResultSet resultSet = executeQuery(sql);
        OrderItem orderItem = null;
        List<OrderItem> orderItemList = new ArrayList<>();
        try {
            while (resultSet.next()) {
                long orderItemId1 = resultSet.getLong("order_item_id");
                long orderId = resultSet.getLong("order_id");
                long itemId = resultSet.getLong("item_id");
                int count = resultSet.getInt("count");
                BigDecimal itemPrice = resultSet.getBigDecimal("item_price");
                orderItem = new OrderItem(orderItemId1, orderId, itemId, count, itemPrice);
                orderItemList.add(orderItem);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            release();
        }
        return orderItemList;
    }
}
