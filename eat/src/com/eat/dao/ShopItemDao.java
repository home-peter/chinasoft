package com.eat.dao;

import com.eat.domain.Item;
import com.eat.domain.ShopItem;

import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public interface ShopItemDao {
    // 插入一个商店-商品中间表
    int insert(ShopItem ShopItem);

    // 更新一个商店-商品中间表
    int update(ShopItem ShopItem);

    // 删除一个商店-商品中间表
    int delete(long ShopItemId);

    // 根据商店编号删除该店商品
    int deleteByShopId(long shopId);

    // 根据id查询一个商店-商品中间表
    ShopItem findOne(long ShopItemId);

    // 根据商店id查询该商店拥有的商品
    List<Item> findByShopId(long shopId);

    // 查询所有订单
    List<ShopItem> findAll();
}
