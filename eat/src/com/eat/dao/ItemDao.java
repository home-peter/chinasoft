package com.eat.dao;

import com.eat.domain.Item;

import java.sql.ResultSet;
import java.util.List;

/**
 * Created by liufeiyang on 2018/4/17.
 */
public interface ItemDao {
     //插入商品
     int insert(Item item);
     //删除商品
     int delete(long itemId);
     //更新商品信息
     int update(Item item);
     //查找一个商品
     Item findOne(long itemId);
     //查找所有商品
     List<Item> findAll();
}
