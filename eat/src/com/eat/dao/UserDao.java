package com.eat.dao;

import com.eat.domain.User;

import java.util.List;

/**
 * Created by ZH on 2018/4/17.
 */
public interface UserDao {
    //增加用户
    int insert(User user);

    //删除用户
    int delete(Long userId);

    //更改用户
    int update(User user);

    //用户登录
    int count(String accountName, String password);

    //判断用户名是否重复
    int count1(String accountName);

    //根据用户Id查询
    User findOne(Long userId);

    // 根据用户名查询用户
    User findByAccountName(String accountName);

    //查询所有用户
    List<User> findAll();
}
