package com.eat.dao;

import com.eat.domain.Order;
import com.eat.domain.OrderInfo;

import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public interface OrderDao {
    // 插入一个订单
    int insert(Order order);

    // 更新一个订单
    int update(Order order);

    // 删除一个订单
    int delete(long orderId);

    // 根据用户编号删除订单
    int deleteByUserId(long userId);

    // 根据订单id查询一个订单
    Order findOne(long orderId);

    // 根据订单编号查询订单项详情信息
    List<OrderInfo> findByOrderId(long orderId);

    // 根据用户编号查询所有订单
    List<Order> findByUserId(long userId);

    // 查询所有订单
    List<Order> findAll();
}
