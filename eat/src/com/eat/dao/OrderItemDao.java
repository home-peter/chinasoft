package com.eat.dao;

import com.eat.domain.Item;
import com.eat.domain.Order;
import com.eat.domain.OrderItem;

import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public interface OrderItemDao {
    // 插入一个订单项
    int insert(OrderItem orderItem);

    // 更新一个订单项
    int update(OrderItem orderItem);

    // 删除一个订单项
    int delete(long orderItemId);

    // 根据订单编号删除订单项
    int deleteByOrderId(long orderId);

    // 根据订单项id查询一个订单项
    OrderItem findOne(long orderItemId);

    // 查询所有订单项
    List<OrderItem> findAll();
}
