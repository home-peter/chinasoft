package com.eat.dao;

import com.eat.domain.Item;
import com.eat.domain.Order;
import com.eat.domain.Shop;

import java.security.PublicKey;
import java.util.Date;
import java.util.List;

/**
 * Created by Fang on 2018/4/17.
 */
public interface ShopDao {
    //显示所有商店
    List<Shop> getAllShop();

    //添加商店
    int add(Shop shop);

    //按序号来删除商店
    int delete(long shopId);

    //更新商店的修改时间
    int update(Shop shop);

    //根据序号查询一个商店信息
    Shop findOne(Long shopId);

    // 根据商家用户编号查询商店
    Shop findByUserId(long userId);

    //店家查看订单
    Order findOrder(long shopId);

    //店家查看订单内容
    List<Item> getAllItem(long orderId);
}
