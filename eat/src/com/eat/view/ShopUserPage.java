package com.eat.view;

import com.eat.constant.OrderConstant;
import com.eat.dao.ItemDao;
import com.eat.dao.OrderDao;
import com.eat.dao.ShopDao;
import com.eat.dao.impl.ItemDaoImpl;
import com.eat.dao.impl.OrderDaoImpl;
import com.eat.dao.impl.ShopDaoImpl;
import com.eat.domain.*;
import com.eat.exception.CheckException;
import com.eat.service.ItemService;
import com.eat.service.OrderService;
import com.eat.service.ShopService;
import com.eat.service.impl.ItemServiceImpl;
import com.eat.service.impl.OrderServiceImpl;
import com.eat.service.impl.ShopServiceImpl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class ShopUserPage extends AbstractPage {
    // 当前商家用户
    private User currentUser = null;
    // 当前商家
    private Shop currentShop = null;
    private Item item = null;
    private ShopService shopService = new ShopServiceImpl();
    private ItemService itemService = new ItemServiceImpl();
    private OrderService orderService = new OrderServiceImpl();
    private OrderDao orderDao = new OrderDaoImpl();
    private ShopDao shopDao = new ShopDaoImpl();
    private ItemDao id = new ItemDaoImpl();

    public ShopUserPage(User user) {
        // 当前用户
        currentUser = user;
        // 当前商家
        currentShop = shopService.findByUserId(user.getUserID());
        init();
    }

    @Override
    void init() {
        // 插入菜单
        menuMap.put(0, "返回首页");
        menuMap.put(1, "查看订单");
        menuMap.put(2, "处理订单");
        menuMap.put(3, "装修店铺");
        menuMap.put(4, "修改菜谱");
        menuMap.put(5, "退出系统");
    }

    @Override
    protected void printHeader() {
        System.out.println("欢迎商家 " + currentUser.getUserName() + " 登录本系统,您的支持是我们进步的最大动力");
        System.out.println("**********************");
    }

    @Override
    boolean doChoose(Scanner scanner) {
        int index = scanner.nextInt();
        boolean isExit = false;
        switch (index) {
            case 0: {
                returnToIndexPage(scanner);
                return true;
            }
            case 1: {
                showOrders(scanner, null);
                return false;
            }
            case 2: {
                processItem(scanner);
                return false;
            }
            case 3: {
                fixtures(scanner);
                return false;
            }
            case 4: {
                fixitem(scanner);
                return false;
            }
            case 5: {
                doExit();
                return true;
            }
            default: {
                System.out.println("输入错误");
                return true;
            }
        }
    }

    // 展示当前商店的订单
    private boolean showOrders(Scanner scanner, Integer status) {
        List<Order> orderList = filterOrder(status);
        if (orderList.size() > 0) {
            System.out.println("订单编号\t\t订单总价\t\t订单状态\t\t下单日期");
            BigDecimal total = new BigDecimal(0);
            for (Order order : orderList) {
                String state = "";
                if (OrderConstant.NOT_PAYED == order.getStatus()) {
                    state = "用户未支付";
                } else if (OrderConstant.PAYED_NOT_DELIVER == order.getStatus()) {
                    state = "未送出";
                } else if (OrderConstant.PAYED_ON_THE_WAY == order.getStatus()) {
                    state = "已送出";
                } else if (OrderConstant.PAYED_CHECKED == order.getStatus()) {
                    state = "订单完成";
                }
                total = total.add(order.getPrice());
                SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:SS");
                String orderDate = format.format(order.getGmtCreate());
                System.out.println(order.getOrderId() + "\t\t" + order.getPrice() + "\t\t" + state + "\t\t" + orderDate);
            }
            System.out.println("总金额:" + total);
            block(scanner);
        } else {
            System.out.println("暂无订单,继续努力");
            block(scanner);
            return false;
        }
        printFooter();
        return true;
    }

    // 根据订单状态筛选订单
    private List<Order> filterOrder(Integer status) {
        List<Order> orderList = orderService.findByShopId(currentShop.getShopId());
        // 如果状态为空,默认查出所有状态的订单
        if (status == null) {
            return orderList;
        }
        // 筛选出某个状态的订单
        Iterator<Order> iterator = orderList.iterator();
        while (iterator.hasNext()) {
            Order order = iterator.next();
            if (order.getStatus() != status) {
                iterator.remove();
            }
        }
        return orderList;
    }

    private void processItem(Scanner scanner) {
        // 打印输出这些订单
        boolean continued = showOrders(scanner, OrderConstant.PAYED_NOT_DELIVER);
        if (!continued){
            return;
        }
        System.out.println("请选择送出的订单：");
        int selectOrder = scanner.nextInt();
        try {
            Order order = orderDao.findOne(selectOrder);
            order.setStatus(2);
            orderDao.update(order);
        } catch (CheckException e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("成功送出");
        block(scanner);
    }

    private void fixtures(Scanner scanner) {
        Shop shop = shopDao.findOne(currentShop.getShopId());
        System.out.println("请输入修改后的店名：");
        String name = scanner.next();
        System.out.println("请输入修改后的地址：");
        String address = scanner.next();
        System.out.println("请输入修改后的店铺描述：");
        String descrition = scanner.next();
        shop.setShopName(name);
        shop.setShopAddress(address);
        shop.setShopDescription(descrition);
        shop.setGmtModified(new Date());
        try {
            shopDao.update(shop);
        } catch (CheckException e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("修改成功");
        block(scanner);
    }

    private void printItems(long shopId) {
        // 根据商家id查询商品
        List<Item> itemList = shopService.findByShopId(shopId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss");
        System.out.println("商品编号\t\t商品名\t\t商品价格\t\t商品描述\t\t上架日期");
        for (Item item1 : itemList) {
            String createTime = format.format(item1.getGmtCreate());
            System.out.println(item1.getItemId() + "\t\t" + item1.getItemName() + "\t\t" +
                    item1.getItemPrice() + "\t\t" + item1.getItemDescription()
                    + "\t\t" + createTime);
        }
    }

    private void fixitem(Scanner scanner) {
        boolean is = false;
        do {
            System.out.println("请选择对菜品的操作");
            System.out.println("1.修改菜品价格与描述");
            System.out.println("2.添加菜品");
            System.out.println("3.删除菜品");
            System.out.println("4.返回上层");
            int select = scanner.nextInt();

            switch (select) {
                case 1: {
                    modifyItem(scanner);
                    is = true;
                    break;
                }
                case 2: {
                    addItem(scanner);
                    is = true;
                    break;
                }
                case 3: {
                    deleteItem(scanner);
                    is = true;
                    break;
                }
                case 4: {
                    run(scanner);
                    is = true;
                    break;
                }
                default:
                    System.out.println("输入错误");
            }
        } while (is == false);

    }

    private void modifyItem(Scanner scanner) {
        printItems(currentShop.getShopId());
        System.out.println("请选择对哪个菜品进行修改");
        Long select = scanner.nextLong();
        item = id.findOne(select);
        System.out.println("请输入修改的价格");
        BigDecimal price = scanner.nextBigDecimal();
        System.out.println("请输入修改的菜品描述");
        String description = scanner.next();
        item.setItemPrice(price);
        item.setItemDescription(description);
        try {
            itemService.updateItem(item);
        } catch (CheckException e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("成功修改");
        isContinue(scanner);
    }

    public void addItem(Scanner scanner) {
        System.out.println("请输入菜名");
        String name = scanner.next();
        System.out.println("请输入价格");
        BigDecimal price = scanner.nextBigDecimal();
        System.out.println("请输入菜品描述");
        String description = scanner.next();
        Item item = new Item();
        item.setItemName(name);
        item.setItemPrice(price);
        item.setItemDescription(description);
        ShopItem shopItem = new ShopItem();
        shopItem.setShopId(currentShop.getShopId());
        try {
            itemService.addItem(shopItem, item);
        } catch (CheckException e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("添加成功");
        block(scanner);
    }

    public void isContinue(Scanner scanner) {
        System.out.println("是否继续操作（y/n）：");
        String isContinue = scanner.next();
        if ("y".equals(isContinue)) {
            run(scanner);
        }
    }

    public void deleteItem(Scanner scanner) {
        printItems(currentShop.getShopId());
        System.out.println("请输入要删除的菜品编号");
        Long choose = scanner.nextLong();
        try {
            itemService.deleteItem(choose);
        } catch (CheckException e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("删除成功");
        isContinue(scanner);
    }
}