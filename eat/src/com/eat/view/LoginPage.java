package com.eat.view;

import com.eat.domain.User;
import com.eat.exception.CheckException;
import com.eat.service.UserService;
import com.eat.service.impl.UserServiceImpl;

import java.util.Scanner;

/**
 * Created by zmy on 2018/4/18.
 */
public class LoginPage extends AbstractPage {
    private UserService userService = new UserServiceImpl();

    public LoginPage() {
        init();
    }

    @Override
    void init() {
        menuMap.put(1, "用户登录");
        menuMap.put(2, "用户注册");
        menuMap.put(3, "退出系统");
    }

    @Override
    protected void printHeader() {
        System.out.println("欢迎使用吃货联盟订餐系统");
        System.out.println("**********************");
    }

    @Override
    boolean doChoose(Scanner scanner) {
        int index = scanner.nextInt();
        boolean isExit = false;
        switch (index) {
            case 1: {
                login(scanner);
                isExit = true;
                break;
            }
            case 2: {
                register(scanner);
                isExit = false;
                break;
            }
            case 3: {
                doExit();
                isExit = true;
                break;
            }
        }
        return isExit;
    }

    private void login(Scanner scanner) {
        System.out.println("请输入用户名");
        String accountName = scanner.next();
        System.out.println("请输入密码");
        String password = scanner.next();
        //　获取登录的用户
        User user = userService.login(accountName, password);
        if (user != null) {
            System.out.println("登录成功");
            printFooter();
            // 转向其他页面
            if (userService.isAdminUser(user)) {
                // todo 转入管理员页面
                new AdminUserPage(user).run(scanner);
                return;
            }
            if (userService.isShopUser(user)) {
                // todo 转入商家页面
                new ShopUserPage(user).run(scanner);
                return;
            }
            if (userService.isNormalUser(user)) {
                // todo 转入普通用户页面
                new NormalUserPage(user).run(scanner);
                return;
            }
        } else {
            System.out.println("帐号或密码错误");
        }
    }

    private void register(Scanner scanner) {
        System.out.println("请输入用户名");
        String accountName = scanner.next();
        System.out.println("请输入密码");
        String password = scanner.next();
        System.out.println("请输入收货地址");
        String address = scanner.next();
        System.out.println("请输入联系方式");
        String phone = scanner.next();
        // 设置用户
        User user = new User();
        user.setAccountName(accountName);
        user.setPassword(password);
        user.setUserAddress(address);
        user.setUserPhone(phone);
        // 执行注册
        try {
            userService.normalUserRegister(user);
        } catch (CheckException e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("注册成功");
    }

    public static void main(String[] args) {
        LoginPage main = new LoginPage();
        Scanner scanner = new Scanner(System.in);
        main.run(scanner);
    }
}
