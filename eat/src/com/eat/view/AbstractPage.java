package com.eat.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by zmy on 2018/4/19.
 */
public abstract class AbstractPage {
    protected Map<Integer, String> menuMap = new HashMap<>();

    // 页面初始化
    abstract void init();

    // 打印头
    protected void printHeader() {
    }

    // 打印结尾
    protected void printFooter() {
        System.out.println("**********************");
    }

    // 打印菜单
    protected void printMenu() {
        printHeader();
        for (Map.Entry<Integer, String> entry : menuMap.entrySet()) {
            Integer index = entry.getKey();
            String menu = entry.getValue();
            System.out.println(index + "、" + menu);
        }
        printFooter();
    }

    // 执行退出
    protected void doExit() {
        System.out.println("欢迎下次继续使用");
    }

    // 等待用户输入任意值
    protected void block(Scanner scanner) {
        System.out.println("按下任意键返回");
        scanner.next();
    }

    // 执行选择的逻辑
    abstract boolean doChoose(Scanner scanner);

    // 页面运行入口
    protected void run(Scanner scanner) {
        boolean exited;
        do {
            printMenu();
            exited = doChoose(scanner);
        } while (!exited);
    }

    // 返回首页
    protected void returnToIndexPage(Scanner scanner) {
        new LoginPage().run(scanner);
    }
}
