package com.eat.view;

import com.eat.constant.UserConstant;
import com.eat.dao.UserDao;
import com.eat.dao.impl.UserDaoImpl;
import com.eat.domain.Shop;
import com.eat.domain.User;
import com.eat.exception.CheckException;
import com.eat.service.ShopService;
import com.eat.service.UserService;
import com.eat.service.impl.ShopServiceImpl;
import com.eat.service.impl.UserServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

public class AdminUserPage extends AbstractPage {
    private User currentUser = null;
    private UserService userService = new UserServiceImpl();
    private ShopService shopService = new ShopServiceImpl();
    UserDao ud = new UserDaoImpl();

    public AdminUserPage(User user) {
        currentUser = user;
        init();
    }

    @Override
    void init() {
        // 插入菜单
        menuMap.put(0, "返回首页");
        menuMap.put(1, "查询所有用户");
        menuMap.put(2, "查询所有店铺");
        menuMap.put(3, "商家注册");
        menuMap.put(4, "商家删除");
        menuMap.put(5, "退出系统");
    }

    @Override
    protected void printHeader() {
        System.out.println("欢迎管理员 " + currentUser.getUserName() + " 登录本系统,请妥善保管好你的密码");
        System.out.println("**********************");
    }

    @Override
    boolean doChoose(Scanner scanner) {
        int index = scanner.nextInt();
        boolean isExit = false;
        switch (index) {
            case 0: {
                returnToIndexPage(scanner);
                isExit = true;
                break;
            }
            case 1: {
                findAllUser();
                isExit = false;
                break;
            }
            case 2: {
                findAllShop();
                isExit = false;
                break;
            }
            case 3: {
                registerShop(scanner);
                isExit = false;
                break;
            }
            case 4: {
                deleteShop(scanner);
                isExit = false;
                break;
            }
            case 5: {
                doExit();
                isExit = true;
                break;
            }
        }
        return isExit;
    }

    private String userStatus(Integer status) {
        if (status.equals(UserConstant.NORMAL_USER)) {
            return "普通用户";
        }
        if (status.equals(UserConstant.SHOP_USER)) {
            return "商家用户";
        }
        if (status.equals(UserConstant.ADMIN_USER)) {
            return "管理员用户";
        }
        if (status.equals(UserConstant.NORMAL_USER)) {
            return "普通用户";
        }
        throw new CheckException("错误类型");
    }

    private void findAllUser() {
        List<User> list = userService.findAll();
        // 打印详情
        System.out.println("序号\t用户ID\t账号\t\t密码\t\t姓名\t\t\t\t电话\t\t\t\t地址\t\t\t创建时间\t\t\t修改时间\t\t\t类型");
        list.forEach(user -> System.out.println(user.getUserID() + "\t\t" + user.getAccountName() + "\t\t"
                + user.getPassword() + "\t\t" + user.getUserName() + "\t\t" + user.getUserPhone() +
                "\t\t" + user.getUserAddress() + "\t\t" + user.getGmtCreate() + "\t\t" + user.getGmtModified() +
                "\t\t\t" + userStatus(user.getType())));
    }

    private void findAllShop() {
        List<Shop> list = shopService.findAll();
        System.out.println("商店ID\t商家对应的用户\t商店名字\t\t商店地址\t商店描述\t商店建立日期\t商店修改日期");
        // 打印详情
        list.forEach(shop -> System.out.println(shop.getShopId() + "\t\t" + shop.getUserId() + "\t\t\t\t"
                + shop.getShopName() + "\t\t" + shop.getShopAddress() + "\t" +
                shop.getShopDescription() + "\t" + shop.getGmtCreate() + "\t" +
                shop.getGmtModified()));
    }

    private void registerShop(Scanner input) {
        try {
            System.out.println("第一步商家用户注册");
            System.out.print("请输入账号：");
            String accountName = input.next();
            System.out.print("请输入密码：");
            String password = input.next();
            System.out.print("请输入姓名：");
            String userName = input.next();
            System.out.print("请输入电话：");
            String phone = input.next();
            System.out.print("请输入地址：");
            String adress = input.next();
            User shopUser = new User(accountName, password, userName, 2, phone, adress, new Date());
            long userId = userService.shopUserRegister(shopUser);
            System.out.println("第二步商家店铺注册");
            System.out.print("请输入商店名称：");
            String shopName = input.next();
            System.out.print("请输入商店地址：");
            String shopAdress = input.next();
            System.out.print("请输入商店介绍：");
            String shopDescription = input.next();
            Shop shop = new Shop(userId, shopName, shopAdress, shopDescription, new Date());
            shopService.add(shop);
            System.out.println("注册成功");
        } catch (CheckException e) {
            System.out.println(e.getMessage());
        }

    }

    private void deleteShop(Scanner input) {
        System.out.print("请输入要删除的商店ID：");
        long shopId = input.nextLong();
        Shop shop;
        shop = shopService.findOne(shopId);
        long userId = shop.getUserId();
        shopService.delete(shopId);
        userService.deleteByUserId(userId);
        System.out.println("删除成功");
    }
}
