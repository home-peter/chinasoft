package com.eat.exception;

/**
 * 检查异常
 * Created by zmy on 2018/4/17.
 */
public class CheckException extends RuntimeException {
    public CheckException(String message) {
        super(message);
    }

    public CheckException(String message, Throwable cause) {
        super(message, cause);
    }
}
