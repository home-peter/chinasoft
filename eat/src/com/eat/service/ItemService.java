package com.eat.service;

import com.eat.domain.Item;
import com.eat.domain.ShopItem;

import java.util.List;

/**
 * Created by liufeiyang on 2018/4/17.
 */
public interface ItemService {
    //添加菜品
    int addItem(ShopItem shopItem, Item item);

    //根据菜品编号删除菜品
    int deleteItem(long itemId);

    //更新菜品的描述和价格
    int updateItem(Item item);

    //查询一个商店的所有商品
    List<Item> findByShopId(long shopId);

    //查找所有商品
    List<Item> findAll();

    //按商品编号查找一个商品
    Item findOne(long itemId);
}
