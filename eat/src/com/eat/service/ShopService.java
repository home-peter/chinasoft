package com.eat.service;

import com.eat.domain.Item;
import com.eat.domain.Order;
import com.eat.domain.Shop;

import java.util.List;

/**
 * Created by Fang on 2018/4/17.
 */
public interface ShopService {
    //添加商店
    int add(Shop shop);

    //按序号来删除商店
    void delete(long shopId);

    //根据序号查询一个商店信息
    Shop findOne(Long shopId);

    // 找到所有商家
    List<Shop> findAll();

    //店家查看订单
    void findOrder(long shopId);

    //店家查看订单内容
    List<Item> getAllItem(long orderId);

    // 根据商家用户编号查询商店
    Shop findByUserId(long userId);

    // 获取该商家所有商品
    List<Item> findByShopId(long shopId);
}
