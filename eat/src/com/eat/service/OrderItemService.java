package com.eat.service;

import com.eat.domain.Item;
import com.eat.domain.OrderItem;

public interface OrderItemService {
    /**
     * 根据商品和商品数量创建一项订单项
     *
     * @param item
     * @param count
     * @return
     */
    OrderItem createOrderItem(Item item, int count);
}
