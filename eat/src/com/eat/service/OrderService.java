package com.eat.service;

import com.eat.domain.*;

import java.util.List;

/**
 * Created by zmy on 2018/4/17.
 */
public interface OrderService {
    // 根据订单项集合产生订单,并写入数据库
    long createFoodOrder(User user, Shop shop, List<OrderItem> orderItemList);

    // 根据订单编号查询订单详情
    List<OrderInfo> findByOrderId(long orderId);

    // 根据用户编号查询用户拥有订单
    List<Order> findByUserId(long userId);

    // 根据商家编号查询商家拥有订单
    List<Order> findByShopId(long shopId);

    // 根据订单编号删除订单
    int deleteByOrderId(long orderId);

    // 根据用户删除订单
    int deleteByUserId(long userId);

    // 改变订单状态
    void changeOrderStatus(long orderId, int status);
}
