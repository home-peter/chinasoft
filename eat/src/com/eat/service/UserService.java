package com.eat.service;

import com.eat.domain.User;

import java.util.List;

/**
 * Created by ZH on 2018/4/17.
 */
public interface UserService {
    //用户登录
    User login(String accountName, String password);

    //普通用户注册
    int normalUserRegister(User user);

    // 商家用户注册
    int shopUserRegister(User user);

    // 管理员用户注册
    int adminUserRegister(User user);

    //用户注销
    void logOut(Long userId);

    //用户删除
    int deleteByUserId(long userId);

    // 用户是否是普通用户
    boolean isNormalUser(User user);

    // 用户是否是商家用户
    boolean isShopUser(User user);

    // 用户是否是管理员用户
    boolean isAdminUser(User user);

    List<User> findAll();
}
