package com.eat.service.impl;

import com.eat.constant.OrderConstant;
import com.eat.dao.OrderDao;
import com.eat.dao.OrderItemDao;
import com.eat.dao.impl.OrderDaoImpl;
import com.eat.dao.impl.OrderItemDaoImpl;
import com.eat.domain.*;
import com.eat.service.OrderService;
import com.eat.utils.CheckUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 0未支付
 * 1已支付,未送出
 * 2已支付，正在路上
 * 3已支付,已签收
 * Created by zmy on 2018/4/17.
 */
public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao = new OrderDaoImpl();
    private OrderItemDao orderItemDao = new OrderItemDaoImpl();

    @Override
    public long createFoodOrder(User user, Shop shop, List<OrderItem> orderItemList) {
        Order emptyOrder = createEmptyOrder(user, shop);
        // 订单总价
        BigDecimal total = new BigDecimal(0);
        for (OrderItem orderItem : orderItemList) {
            // 设置订单项所属订单
            orderItem.setOrderId(emptyOrder.getOrderId());
            // 获取订单项价格
            BigDecimal subTotal = orderItem.getItemPrice();
            total = total.add(subTotal);
            // 插入订单项
            orderItemDao.insert(orderItem);
        }
        // 设置订单总价
        emptyOrder.setPrice(total);
        // 设置收货地址
        emptyOrder.setAddress(user.getUserAddress());
        // 设置送餐时间
        emptyOrder.setDate(new Date());
        // 更新订单
        orderDao.update(emptyOrder);
        return emptyOrder.getOrderId();
    }


    @Override
    public List<OrderInfo> findByOrderId(long orderId) {
        CheckUtils.check(orderId > 0, "查询订单详情时,订单编号不能为空");
        return orderDao.findByOrderId(orderId);
    }

    @Override
    public List<Order> findByUserId(long userId) {
        return orderDao.findByUserId(userId);
    }

    @Override
    public List<Order> findByShopId(long shopId) {
        List<Order> orderList = orderDao.findAll();
        Iterator<Order> iterator = orderList.iterator();
        while (iterator.hasNext()) {
            Order order = iterator.next();
            if (order.getShopId() != shopId) {
                iterator.remove();
            }
        }
        return orderList;
    }

    @Override
    public int deleteByOrderId(long orderId) {
        int delete;
        // 删除订单项
        delete = orderItemDao.deleteByOrderId(orderId);
        // 删除订单
        delete += orderDao.delete(orderId);
        return delete;
    }

    @Override
    public int deleteByUserId(long userId) {
        int delete = 0;
        List<Order> orderList = orderDao.findByUserId(userId);
        for (Order order : orderList) {
            delete += orderItemDao.deleteByOrderId(order.getOrderId());
        }
        delete += orderDao.deleteByUserId(userId);
        return delete;
    }


    @Override
    public void changeOrderStatus(long orderId, int status) {
        Order order = orderDao.findOne(orderId);
        order.setStatus(status);
        orderDao.update(order);
    }


    // 根据当前用户创建一个属于某商家的空白订单
    private Order createEmptyOrder(User user, Shop shop) {
        CheckUtils.check(user != null && user.getUserID() > 0, "创建订单时,用户编号必须存在");
        CheckUtils.check(shop != null && shop.getShopId() > 0, "创建订单时,商家编号必须存在");
        Order order = new Order();
        // 设置订单用户编号
        order.setUserId(user.getUserID());
        // 设置订单商家编号
        order.setShopId(shop.getShopId());
        // 设置订单为未支付状态
        order.setStatus(OrderConstant.NOT_PAYED);
        // 设置总价为0
        order.setPrice(new BigDecimal(0));
        // 产生某个用户并且属于某商店的订单,并获取自增主键
        int newKey = orderDao.insert(order);
        // 设置自增主键
        order.setOrderId(newKey);
        return order;
    }
}
