package com.eat.service.impl;

import com.eat.dao.ItemDao;
import com.eat.dao.ShopItemDao;
import com.eat.dao.impl.ItemDaoImpl;
import com.eat.dao.impl.ShopItemDaoImpl;
import com.eat.domain.Item;
import com.eat.domain.ShopItem;
import com.eat.service.ItemService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liufeiyang on 2018/4/17.
 */
public class ItemServiceImpl implements ItemService {
    private ItemDao itemDao = new ItemDaoImpl();
    private ShopItemDao shopItemDao = new ShopItemDaoImpl();

    //添加菜品
    @Override
    public int addItem(ShopItem shopItem, Item item) {
        // 插入一个商品
        int itemId = itemDao.insert(item);
        // 设置商品编号
        shopItem.setItemId(itemId);
        return shopItemDao.insert(shopItem);
    }

    //根据菜品编号删除菜品
    @Override
    public int deleteItem(long itemId) {
        int num = itemDao.delete(itemId) + shopItemDao.delete(itemId);
        return num;
    }

    //更新菜品的描述和价格
    @Override
    public int updateItem(Item item) {
        return itemDao.update(item);
    }

    //根据商店编号查找店下商品
    @Override
    public List<Item> findByShopId(long shopId) {
        List<Item> itemList;
        itemList = shopItemDao.findByShopId(shopId);
        return itemList;
    }

    //查找所有商品
    @Override
    public List<Item> findAll() {
        List<Item> itemList;
        itemList = itemDao.findAll();
        return itemList;
    }

    //按商品编号查找一个商品
    @Override
    public Item findOne(long itemId) {
        Item item = itemDao.findOne(itemId);
        return item;
    }


}
