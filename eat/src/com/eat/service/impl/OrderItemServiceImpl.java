package com.eat.service.impl;

import com.eat.dao.OrderItemDao;
import com.eat.domain.Item;
import com.eat.domain.OrderItem;
import com.eat.service.OrderItemService;
import com.eat.utils.CheckUtils;

import java.math.BigDecimal;

public class OrderItemServiceImpl implements OrderItemService {
    private OrderItemDao orderItemDao;

    @Override
    public OrderItem createOrderItem(Item item, int count) {
        CheckUtils.check(item != null && item.getItemId() > 0, "商品编号不能为空");
        CheckUtils.check(count > 0, "商品数量必须大于1");
        OrderItem orderItem = new OrderItem();
        orderItem.setItemId(item.getItemId());
        orderItem.setCount(count);
        // 设置价格
        BigDecimal itemPrice = item.getItemPrice();
        itemPrice = itemPrice.multiply(new BigDecimal(count));
        orderItem.setItemPrice(itemPrice);
        return orderItem;
    }
}
