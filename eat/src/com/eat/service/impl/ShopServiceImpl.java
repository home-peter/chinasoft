package com.eat.service.impl;

import com.eat.dao.*;
import com.eat.dao.impl.*;
import com.eat.domain.Item;
import com.eat.domain.Order;
import com.eat.domain.Shop;
import com.eat.domain.User;
import com.eat.service.ShopService;
import com.eat.utils.CheckUtils;
import com.eat.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Fang on 2018/4/17.
 */
public class ShopServiceImpl implements ShopService {
    private ShopDao sdi = new ShopDaoImpl();
    private ItemDao idi = new ItemDaoImpl();
    private OrderDao odi = new OrderDaoImpl();
    private ShopItemDao shopItemDao = new ShopItemDaoImpl();

    public int add(Shop shop) {
        CheckUtils.check(shop.getUserId() > 0, "id不能为空");
        CheckUtils.check(StringUtils.isNotEmpty(shop.getShopName()), "名字不能为空");
        CheckUtils.check(StringUtils.isNotEmpty(shop.getShopAddress()), "商家地址不能为空");
        return sdi.add(shop);
    }

    public void delete(long shopId) {
        Shop shop = sdi.findOne(shopId);
        int delete = sdi.delete(shopId);
        shopItemDao.deleteByShopId(shopId);
        CheckUtils.check(delete > 0, "不存在该商家");
    }

    public Shop findOne(Long shopId) {
        Shop shop = sdi.findOne(shopId);
        CheckUtils.check(shop != null, "不存在该商家");
        return shop;
    }

    @Override
    public List<Shop> findAll() {
        return sdi.getAllShop();
    }

    public void findOrder(long shopId) {
        Order order = odi.findOne(shopId);
        CheckUtils.check(order != null, "不存在该商家");
        System.out.println(order);
    }

    public List<Item> getAllItem(long orderId) {
        Item item = idi.findOne(orderId);
        List<Item> list = new ArrayList<Item>();
        CheckUtils.check(item != null, "不存在该商家");
        return list;
    }

    @Override
    public Shop findByUserId(long userId) {
        return sdi.findByUserId(userId);
    }

    @Override
    public List<Item> findByShopId(long shopId) {
        return shopItemDao.findByShopId(shopId);
    }
}
