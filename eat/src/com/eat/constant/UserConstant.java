package com.eat.constant;

/**
 * Created by zmy on 2018/4/19.
 */
public interface UserConstant {
    // 普通用户
    int NORMAL_USER = 0;
    // 商家用户
    int SHOP_USER = 1;
    // 管理员用户
    int ADMIN_USER = 2;
}
