package com.eat.constant;

/**
 * 订单常量
 */
public interface OrderConstant {
    /**
     * 未支付
     */
    int NOT_PAYED = 0;
    /**
     * 已支付未派送
     */
    int PAYED_NOT_DELIVER = 1;
    /**
     * 已支付订单正在路上
     */
    int PAYED_ON_THE_WAY = 2;
    /**
     * 支付并已签收
     */
    int PAYED_CHECKED = 3;
}
