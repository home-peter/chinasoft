package com.eat.utils;

/**
 * Created by zmy on 2018/4/17.
 */
public final class StringUtils {
    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }
}
