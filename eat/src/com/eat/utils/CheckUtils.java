package com.eat.utils;

import com.eat.exception.CheckException;

/**
 * Created by zmy on 2018/4/17.
 */
    public final class CheckUtils {
        public static void check(boolean condition, String message) {
            if (!condition) {
                fail(message);
            }
        }

    private static void fail(String message) {
        throw new CheckException(message);
    }
}
