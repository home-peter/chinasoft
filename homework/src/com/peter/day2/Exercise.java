package com.peter.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by zmy on 2018/3/21.
 */
public class Exercise {
    public static void shopping() {
        Scanner input = new Scanner(System.in);
        // 初始化商品
        List<String> itemList = new ArrayList<>();
        itemList.add("T恤");
        itemList.add("网球鞋子");
        itemList.add("网球拍");
        String isContinue = "n";
        // 执行购买
        do {
            int itemId;
            int itemNumber;
            System.out.println("请输入商品编号:");
            itemId = input.nextInt();
            System.out.println("请输入购买数量:");
            itemNumber = input.nextInt();
            // 打印购买信息
            System.out.println(itemList.get(itemId) + "\t" + "数量" + itemNumber + "\t"
                    + "合计" + itemNumber * 245);
            System.out.println("是否继续(y/n)");
            isContinue = input.next();
        } while (isContinue.equals("y"));
    }

    public static void main(String[] args) {
        shopping();
    }
}
