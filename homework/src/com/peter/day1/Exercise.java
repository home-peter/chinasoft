package com.peter.day1;

import java.util.Scanner;

/**
 * Created by zmy on 2018/3/20.
 */
public class Exercise {
    /**
     * 录入会员信息
     */
    public static void showVipContent(Scanner scanner) {
        System.out.println("请输入会员号(<4位整数>):");
        int vipNumber = scanner.nextInt();
        // 校验数据
        checkVipNumber(vipNumber);
        System.out.println("请输入会员生日(月/日<用两位数表示>)");
        String vipBirthday = scanner.next();
        System.out.println("请输入积分:");
        int vipScore = scanner.nextInt();
        // 打印录入信息
        System.out.println("已录入的会员信息是:");
        System.out.println(vipNumber + "\t" + vipBirthday + "\t" + vipScore);
    }

    private static void checkVipNumber(int vipNumber) {
        try {
            String sVipNumber = String.valueOf(vipNumber);
            if (sVipNumber.length() != 4) {
                System.out.println("录入失败");
                throw new RuntimeException("录入失败");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 执行抽奖
     */
    public static void doLuckDraw(Scanner scanner) {
        int vipNumber = scanner.nextInt();
        checkVipNumber(vipNumber);
        // 中奖数字
        int luckNumber = (int) (Math.random() * 10);
        int vipSecondNumber = vipNumber / 100 % 10;
        // 中奖数字和会员编号中的第二个号码相同则中奖
        if (luckNumber == vipSecondNumber) {
            System.out.println("恭喜你中奖了");
        } else {
            System.out.println("很遗憾你错过了大奖");
        }
    }

    public static void showSale(Scanner scanner) {
        int VipScore = scanner.nextInt();
        if (VipScore < 2000) {
            System.out.println("9折");
        } else if (VipScore < 4000 && VipScore >= 2000) {
            System.out.println("8折");
        } else if (VipScore >= 4000 && VipScore < 8000) {
            System.out.println("7折");
        } else {
            System.out.println("6折");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 录入vip信息
        showVipContent(scanner);
        // 执行抽奖
        doLuckDraw(scanner);
        // 展示折扣信息
        showSale(scanner);
    }
}
