package com.peter.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LuckDraw {
    // 菜单集合
    private List<String> menuList;
    // 当前用户
    private User user;

    public LuckDraw() {
        menuList = new ArrayList<>();
        // 初始化菜单目录
        menuList.add("注册");
        menuList.add("登录");
        menuList.add("抽奖");
        // 默认用户
        user = new User("admin", "123456", true);
    }

    private void printHeader() {
        System.out.println("*****欢迎进入奖客富翁系统*****");
        // 菜单编号
        int index = 1;
        for (String menu : menuList) {
            System.out.println(index++ + "." + menu);
        }
        System.out.println("****************************");
    }

    private boolean doChoose(Scanner input) {
        System.out.println("请选择菜单:");
        int menuIndex = input.nextInt();
        // 检查编号是否出错
        if (!checkMenuIndex(--menuIndex)) {
            System.out.println("您的输入有误!");
        }
        System.out.println("[奖客富翁系统>" + menuList.get(menuIndex) + "]");
        switch (menuList.get(menuIndex)) {
            case "注册": {
                register(input);
                break;
            }
            case "登录": {
                System.out.println("登录");
                login(input);
                break;
            }
            case "抽奖": {
                System.out.println("抽奖");
                doLuckDraw(input);
                break;
            }
        }
        System.out.println("继续吗?(y/n):");
        String isContinued = input.next();
        // 返回是否继续
        return "y".equals(isContinued);
    }

    private boolean checkMenuIndex(int menuIndex) {
        if (menuIndex < 0 || menuIndex > menuList.size() - 1) {
            return false;
        }
        return true;
    }

    private void register(Scanner input) {
        System.out.println("请填写个人注册信息:");
        System.out.println("用户名:");
        String username = input.next();
        System.out.println("密码:");
        String password = input.next();
        user = new User(username, password, true);
        // 生成vip卡号
        user.vipNumber = makeRandomNumber();
        // 打印用户信息
        System.out.println("注册成功,请记好你的会员卡号");
        System.out.println("用户名" + "\t" + "密码" + "\t" + "会员卡号");
        System.out.println(user.username + "\t" + user.password + "\t" + user.vipNumber);
    }

    private void login(Scanner input) {
        System.out.println("请输入用户名:");
        String username = input.next();
        System.out.println("请输入密码:");
        String password = input.next();
        // 不允许再登录
        if (user.canWrongTimes < 0) {
            return;
        }
        if (username.equals(user.username)) {
            // 用户名正确而密码错误
            if (!password.equals(user.password)) {
                user.canWrongTimes--;
                System.out.println("您还有" + user.canWrongTimes + "次输入机会");
            } else {
                System.out.println("欢迎您:" + user.username);
            }
        }
    }

    private int makeRandomNumber() {
        int max = 9999;
        int min = 1000;
        int randomNumber = (int) (Math.random() * (max - min) + min);
        return randomNumber;
    }

    private void doLuckDraw(Scanner input) {
        int[] luckNumbers = new int[5];
        boolean isLuckDog = false;
        System.out.println("请输入您的卡号:");
        int vipNumber = input.nextInt();
        for (int i = 0; i < 5; i++) {
            luckNumbers[i] = makeRandomNumber();
        }
        for (int luckNumber : luckNumbers) {
            if (luckNumber == vipNumber) {
                isLuckDog = true;
            }
            System.out.println("本日的幸运数字为:" + luckNumber + " ");
        }
        if (isLuckDog) {
            System.out.println("恭喜你,中大奖了");
        } else {
            System.out.println("抱歉!您不是本日的幸运会员!");
        }
    }

    public void run(Scanner input) {
        do {
            printHeader();
        } while (doChoose(input));
        System.out.println("系统退出,谢谢使用!");
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        LuckDraw luckDraw = new LuckDraw();
        luckDraw.run(input);

    }

    private class User {
        String username;
        String password;
        int vipNumber;
        int canWrongTimes;
        boolean isRegister;

        User() {
            canWrongTimes = 3;
        }

        User(String username, String password, boolean isRegister) {
            canWrongTimes = 3;
            this.username = username;
            this.password = password;
            this.isRegister = isRegister;
        }
    }
}
