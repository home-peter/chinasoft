package com.peter.day3;

import java.util.Scanner;

public class User {
    private String username;
    private Integer score;

    public User() {
    }

    public User(String username, Integer score) {
        this.username = username;
        this.score = score;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public int action(Scanner input) {
        int userAction = input.nextInt();
        return userAction;
    }
}
