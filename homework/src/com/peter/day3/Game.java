package com.peter.day3;

import java.util.*;

public class Game {
    private User userGamer;
    private Computer computerGamer;
    private int gameStartedTimes;
    private Map<Integer, String> gamePolicy = new LinkedHashMap<>();
    private List<String> roleList = new ArrayList<>();

    public void initial(Scanner input) {
        printHeader();
        // 初始化游戏策略
        gamePolicy.put(1, "剪刀");
        gamePolicy.put(2, "石头");
        gamePolicy.put(3, "布");
        // 初始化游戏角色
        roleList.add("刘备");
        roleList.add("孙权");
        roleList.add("曹操");
        setUsername(input);
        choseChallengeRole(input);
        System.out.println(userGamer.getUsername() + " vs " + computerGamer.getNickname() + " 对战");
        System.out.println("要开始吗?(y/n)");
        String isContinued = input.next();
        if ("y".equals(isContinued)) {
            do {
                startGame(input);
                System.out.println("是否开启下一轮(y/n):");
                isContinued = input.next();
            } while ("y".equals(isContinued));
            // 打印输出对战结果
            showResult();
        }
    }

    private void setUsername(Scanner input) {
        System.out.println("请输入您的名字");
        String nickname = input.next();
        userGamer = new User(nickname, 0);
    }

    private void choseChallengeRole(Scanner input) {
        System.out.println("请选择对方角色");
        for (int roleIndex = 0; roleIndex < roleList.size(); roleIndex++) {
            System.out.print(roleIndex + 1 + ":" + roleList.get(roleIndex));
        }
        System.out.println();
        int roleIndex = input.nextInt();
        String role = roleList.get(--roleIndex);
        System.out.println("您选择了" + role + "对战");
        computerGamer = new Computer(role, 0);
    }

    private void printHeader() {
        System.out.println("-------------进入游戏世界-------------\n\n");
        System.out.println("\t\t*********************");
        System.out.println("\t\t** 猜拳,开始 **");
        System.out.println("\t\t*********************\n\n");
        System.out.println("出拳规则:1:剪刀 2:石头 3:布");
    }

    private void startGame(Scanner input) {
        System.out.println("请出拳:1:剪刀 2:石头 3:布(输入相应数字):");
        int userAction = userGamer.action(input);
        int computerAction = computerGamer.action();
        System.out.println("你出拳:" + gamePolicy.get(userAction));
        System.out.println(computerGamer.getNickname() + "出拳:" + gamePolicy.get(computerAction));
        // 判断结果
        int humanWinResult = isHumanWin(userAction, computerAction);
        switch (humanWinResult) {
            case 1: {
                System.out.println("您赢了!");
                // 加上积分
                userGamer.setScore(userGamer.getScore() + 1);
                break;
            }
            case 0: {
                System.out.println("平局!");
                break;
            }
            case -1: {
                System.out.println("你这个垃圾竟然输了!");
                // 加上积分
                computerGamer.setScore(computerGamer.getScore() + 1);
                break;
            }
        }
        // 游戏对战次数加1
        gameStartedTimes++;
    }

    private void showResult() {
        System.out.println("-------------------------------");
        System.out.println(userGamer.getUsername() + " vs " + computerGamer.getNickname());
        System.out.println("对战次数: " + gameStartedTimes);
        System.out.println("姓名:" + "\t" + "得分");
        System.out.println(userGamer.getUsername() + "\t " + userGamer.getScore());
        System.out.println(computerGamer.getNickname() + "\t " + computerGamer.getScore());
        if (userGamer.getScore() < computerGamer.getScore()) {
            System.out.println("垃圾!你连电脑都玩不过!");
        } else {
            System.out.println("哇!你已经超神了!");
        }
        System.out.println("-------------------------------");
    }

    /**
     * 返回值1为人类获胜 0平局 -1人类失败
     *
     * @param userAction
     * @param computerAction
     * @return
     */
    private int isHumanWin(int userAction, int computerAction) {
        if (userAction == computerAction) {
            return 0;
        } else if (userAction == 1 && computerAction == 2) {
            return -1;
        } else if (userAction == 2 && computerAction == 3) {
            return -1;
        } else if (userAction == 3 && computerAction == 1) {
            return -1;
        }
        return 1;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Game game = new Game();
        game.initial(input);
    }
}
