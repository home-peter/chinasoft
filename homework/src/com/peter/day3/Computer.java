package com.peter.day3;

public class Computer {
    private String nickname;
    private Integer score;

    public Computer() {
    }

    public Computer(String nickname, Integer score) {
        this.nickname = nickname;
        this.score = score;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public int action() {
        return (int) (Math.random() * 3 + 1);
    }
}
