package com.peter.day3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class MiniDVDManager {
    // 存放dvd
    private List<DVD> dvdList = new ArrayList<>();
    private List<String> menuList = new ArrayList<>();
    private static final int RENT_DAY = 5;

    public MiniDVDManager() {
        // 初始化
        initial();
    }

    private void initial() {
        dvdList.add(new DVD("罗马假日", 0, "2013-07-01"));
        dvdList.add(new DVD("风声鹤唳", 1, null));
        dvdList.add(new DVD("浪漫满屋", 1, null));
        // 初始化菜单
        menuList.add("新增DVD");
        menuList.add("查看DVD");
        menuList.add("删除DVD");
        menuList.add("借出DVD");
        menuList.add("归还DVD");
        menuList.add("退出");
    }

    private void exit() {
        System.out.println("欢迎下次再次使用");
        System.exit(0);
    }

    private void printHeader() {
        System.out.println("欢迎使用迷你DVD管理器");
        System.out.println("--------------------------");
        int menuIndex = 1;
        for (String menu : menuList) {
            System.out.println(menuIndex++ + ". " + menu);
        }
        System.out.println("--------------------------");
    }

    private void startMenu(Scanner input) {
        printHeader();
        int menuIndex = input.nextInt();
        switch (menuList.get(menuIndex - 1)) {
            case "新增DVD": {
                System.out.println("--->新增DVD");
                addDvd(input);
                returnMain(input);
                break;
            }
            case "查看DVD": {
                System.out.println("--->查看DVD");
                showDvds();
                returnMain(input);
                break;
            }
            case "删除DVD": {
                System.out.println("--->删除DVD");
                remove(input);
                returnMain(input);
                break;
            }
            case "借出DVD": {
                System.out.println("--->借出DVD");
                borrow(input);
                returnMain(input);
                break;
            }
            case "归还DVD": {
                System.out.println("--->归还DVD");
                returnDvd(input);
                returnMain(input);
                break;
            }
            default: {
                exit();
            }
        }
    }

    private void returnMain(Scanner input) {
        System.out.println("--------------------------");
        System.out.println("输入0返回(其它键直接退出):");
        String isContinued = input.next();
        if ("0".equals(isContinued)) {
            startMenu(input);
        } else {
            exit();
        }
    }

    public void showDvds() {
        System.out.println("序号" + "\t" + "状态" + "\t" + "名称" + "\t" + "借出日期");
        int dvdIndex = 1;
        for (DVD dvd : dvdList) {
            String state;
            String name = "<<" + dvd.name + ">>";
            String date = dvd.date;
            if (0 == dvd.state) {
                state = "已借出";
            } else {
                state = "可借";
            }
            System.out.println(dvdIndex + "\t" + state + "\t" + name + "\t" + date);
            dvdIndex++;
        }
    }

    private void addDvd(Scanner input) {
        System.out.println("请输入DVD名称:");
        String dvdName = input.next();
        DVD newDVD = new DVD(dvdName, 1, null);
        dvdList.add(newDVD);
        System.out.println("新增 " + dvdName + " 成功");
    }

    private void remove(Scanner input) {
        // 删除之前先查看有哪些DVD
        showDvds();
        System.out.println("请输入要删除的DVD名称:");
        String dvdName = input.next();
        for (DVD dvd : dvdList) {
            int state = dvd.state;
            if (dvd.name.equals(dvdName) && state == 0) {
                System.out.println(dvd.name + "为借出状态,不能删除!");
                return;
            }
            if (dvd.name.equals(dvdName) && state == 1) {
                dvdList.remove(dvd);
                System.out.println(dvd.name + "删除成功");
                return;
            }
        }
    }

    private void borrow(Scanner input) {
        showDvds();
        System.out.println("选择你要借的DVD名称");
        String dvdName = input.next();
        // 找到我们要的dvd
        DVD targetDvd = null;
        for (DVD dvd : dvdList) {
            if (dvdName.equals(dvd.name)) {
                targetDvd = dvd;
                break;
            }
        }
        if (targetDvd != null) {
            // 执行借书
            if (targetDvd.state == 0) {
                System.out.println("此书已借出");
            } else {
                targetDvd.state = 0;
                targetDvd.date = parseDate(new Date());
                System.out.println("您已经借出 " + targetDvd.name);
            }
        } else {
            System.out.println("查无此书");
        }
    }

    private void returnDvd(Scanner input) {
        showDvds();
        System.out.println("选择你要归还的DVD名称:");
        String dvdName = input.next();
        System.out.println("请输入归还日期(例2018-02-23):");
        String returnDate = input.next();
        // 执行归还逻辑
        for (DVD dvd : dvdList) {
            int state = dvd.state;
            if (dvd.name.equals(dvdName) && state == 0) {
                // 修改状态码
                dvd.state = 1;
                int borrowedDays;
                double money;
                borrowedDays = countDays(dvd.date, returnDate);
                money = RENT_DAY * borrowedDays;
                System.out.println("归还《" + dvd.name + "》成功");
                System.out.println("借出日期:" + dvd.date);
                System.out.println("归还日期:" + returnDate);
                System.out.println("应付租金:" + money);
            }
            if (dvd.name.equals(dvdName) && state == 1) {
                System.out.println(dvd.name + "此书未借出");
            }
        }
    }

    private int countDays(String borrowDay, String returnDay) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date bDay = format.parse(borrowDay);
            Date rDay = format.parse(returnDay);
            return (int) ((rDay.getTime() - bDay.getTime()) / (24 * 60 * 60 * 1000));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private String parseDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    public void run(Scanner input) {
        startMenu(input);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        MiniDVDManager miniDVDManager = new MiniDVDManager();
        miniDVDManager.run(input);
    }

    private class DVD {
        String name;
        int state;
        String date;

        public DVD() {
        }

        public DVD(String name, int state, String date) {
            this.name = name;
            this.state = state;
            this.date = date;
        }
    }
}
