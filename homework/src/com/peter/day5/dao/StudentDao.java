package com.peter.day5.dao;

import com.peter.day5.domain.Student;

import java.util.List;

/**
 * Created by zmy on 2018/4/4.
 */
public interface StudentDao {
    int add(Student student);

    int update(Student student);

    int delete(Student student);

    Student findOne(int id);

    List<Student> findAll();
}
