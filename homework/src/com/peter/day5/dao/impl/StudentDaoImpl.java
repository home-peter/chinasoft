package com.peter.day5.dao.impl;

import com.peter.core.BaseDao;
import com.peter.day5.dao.StudentDao;
import com.peter.day5.domain.Student;

import java.util.List;

public class StudentDaoImpl extends BaseDao implements StudentDao {
    @Override
    public int add(Student student) {
        String sql = "insert into student(name,password,age,description,sex) values(?,?,?,?,?)";
        // 需要插入的字段
        String name = student.getName();
        String password = student.getPassword();
        int age = student.getAge();
        String description = student.getDescription();
        String sex = student.getSex();
        // 执行插入
        return update(sql, name, password, age, description, sex);
    }

    @Override
    public int update(Student student) {
        String sql = "update student set name=?,password=?,age=?,description=?,sex=? where id=?";
        // 需要插入的字段
        String name = student.getName();
        String password = student.getPassword();
        int age = student.getAge();
        String description = student.getDescription();
        String sex = student.getSex();
        Integer id = student.getId();
        // 执行更新
        return update(sql, name, password, age, description, sex, id);
    }

    @Override
    public int delete(Student student) {
        String sql = "delete from student where id=?";
        Integer id = student.getId();
        // 执行删除
        return update(sql, id);
    }

    @Override
    public Student findOne(int id) {
        String sql = "select * from student where id =?";
        return query(sql, Student.class, id).get(0);
    }

    @Override
    public List<Student> findAll() {
        String sql = "select * from student";
        return query(sql, Student.class);
    }
}
