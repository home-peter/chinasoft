package com.peter.day5.domain;

/**
 * Created by zmy on 2018/4/4.
 */
public class Student {
    private Integer id;
    private String name;
    private String password;
    private int age;
    private String description;
    private String sex;

    public Student() {
    }

    public Student(String name, String password, int age, String description, String sex) {
        this.name = name;
        this.password = password;
        this.age = age;
        this.description = description;
        this.sex = sex;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                ", description='" + description + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
