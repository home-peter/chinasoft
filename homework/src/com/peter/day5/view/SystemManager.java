package com.peter.day5.view;

import com.peter.day5.domain.Student;
import com.peter.day5.service.StudentService;
import com.peter.day5.service.impl.StudentServiceImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SystemManager {
    private StudentService studentService = new StudentServiceImpl();
    private Map<Integer, String> menu = new HashMap<>();

    public SystemManager() {
        init();
    }

    private void init() {
        menu.put(1, "查询所有学员信息");
        menu.put(2, "添加学员");
        menu.put(3, "删除学院");
        menu.put(4, "修改学员信息");
        menu.put(5, "退出系统");
    }

    private void printHeader() {
        System.out.println("------****学生管理系统****------");
    }

    private void printFooter() {
        System.out.println("--------------------------");
    }

    private void printMenu() {
        for (Map.Entry<Integer, String> entry : menu.entrySet()) {
            System.out.println(entry.getKey() + "." + entry.getValue());
        }
    }

    private void showAllStudent() {
        System.out.println("编号\t\t" + "姓名\t\t" + "性别\t\t" + "评价");
        for (Student student : studentService.findAll()) {
            System.out.println(student.getId() + "\t\t" + student.getName() + "\t\t" +
                    student.getSex() + "\t\t" + student.getDescription());
        }
        printFooter();
    }

    private void addStudent(Scanner input) {
        System.out.println("请输入学员的姓名");
        String name = input.next();
        System.out.println("请输入学员密码");
        String password = input.next();
        System.out.println("请输入学员年龄");
        int age = input.nextInt();
        System.out.println("请输入学员评价");
        String description = input.next();
        System.out.println("请输入学员性别");
        String sex = input.next();
        // 构造学员对象
        Student student = new Student(name, password, age, description, sex);
        int added = studentService.add(student);
        if (added > 0) {
            System.out.println("增加成功!");
        } else {
            System.out.println("增加失败!");
        }
        printFooter();
    }

    private void deleteStudent(Scanner input) {
        System.out.println("当前操作:根据ID删除学员");
        showAllStudent();
        System.out.println("请输入你要删除学员的ID");
        int id = input.nextInt();
        // 构造学员对象
        Student student = new Student();
        student.setId(id);
        int delete = studentService.delete(student);
        if (delete > 0) {
            System.out.println("删除成功");
        } else {
            System.out.println("删除失败");
        }
        printFooter();
    }

    private void updateStudent(Scanner input) {
        System.out.println("当前操作:根据Id更新学员");
        System.out.println("请输入你要更改学员的Id");
        int id = input.nextInt();
        Student student = studentService.findOne(id);
        System.out.println("请输入修改后的学员姓名:");
        String name = input.next();
        student.setName(name);
        // 执行更新
        int update = studentService.update(student);
        if (update > 0) {
            System.out.println("更新成功");
        } else {
            System.out.println("更新失败");
        }
        printFooter();
    }

    private void exit() {
        System.out.println("退出系统欢迎下次使用!");
    }

    private boolean doChoose(Scanner input) {
        boolean isExit = false;
        System.out.println("请选择相应的操作:");
        int index = input.nextInt();
        switch (index) {
            case 1: {
                showAllStudent();
                break;
            }
            case 2: {
                addStudent(input);
                break;
            }
            case 3: {
                deleteStudent(input);
                break;
            }
            case 4: {
                updateStudent(input);
                break;
            }
            case 5: {
                exit();
                isExit = true;
            }
        }
        return isExit;
    }


    public void run() {
        Scanner input = new Scanner(System.in);
        boolean isExit = false;
        printHeader();
        do {
            printMenu();
            isExit = doChoose(input);
        } while (!isExit);
    }


    public static void main(String[] args) {
        SystemManager manager = new SystemManager();
        manager.run();
    }
}
