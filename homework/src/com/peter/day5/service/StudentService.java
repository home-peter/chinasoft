package com.peter.day5.service;

import com.peter.day5.domain.Student;

import java.util.List;

public interface StudentService {
    int add(Student student);

    int update(Student student);

    int delete(Student student);

    Student findOne(int id);

    List<Student> findAll();
}
