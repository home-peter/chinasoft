package com.peter.day5.service.impl;

import com.peter.day5.dao.StudentDao;
import com.peter.day5.dao.impl.StudentDaoImpl;
import com.peter.day5.domain.Student;
import com.peter.day5.service.StudentService;

import java.util.List;

public class StudentServiceImpl implements StudentService {
    private StudentDao studentDao = new StudentDaoImpl();

    @Override
    public int add(Student student) {
        return studentDao.add(student);
    }

    @Override
    public int update(Student student) {
        return studentDao.update(student);
    }

    @Override
    public int delete(Student student) {
        return studentDao.delete(student);
    }

    @Override
    public Student findOne(int id) {
        return studentDao.findOne(id);
    }

    @Override
    public List<Student> findAll() {
        return studentDao.findAll();
    }
}
