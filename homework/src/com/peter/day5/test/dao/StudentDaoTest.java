package com.peter.day5.test.dao;

import com.peter.day5.dao.StudentDao;
import com.peter.day5.dao.impl.StudentDaoImpl;
import com.peter.day5.domain.Student;

import java.util.List;

public class StudentDaoTest {
    private static StudentDao studentDao = new StudentDaoImpl();
    public static void main(String[] args) {
//        Student student = new Student("peter", "567", 12, "test", "男");
//        Student student1 = studentDao.findOne(1);
//        System.out.println(student1);
        List<Student> studentList = studentDao.findAll();
        for (Student student : studentList) {
            System.out.println(student);
        }
    }
}
