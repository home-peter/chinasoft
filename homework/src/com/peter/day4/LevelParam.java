package com.peter.day4;

/**
 * Created by zmy on 2018/3/26.
 */
public class LevelParam {
    public final static Level[] LEVEL;

    static {
        LEVEL = new Level[]{
                new Level(1, 3, 2, 10, 5),
                new Level(2, 4, 3, 9, 10),
                new Level(3, 5, 4, 8, 15),
                new Level(4, 6, 5, 7, 20),
                new Level(5, 7, 6, 6, 25),
                new Level(6, 8, 7, 5, 30)
        };
    }
}
