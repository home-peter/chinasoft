package com.peter.day4;

import java.util.Scanner;

/**
 * Created by zmy on 2018/3/26.
 */
public class Player {
    // 玩家当前级别号(默认为1级)
    private int levelNo = 1;
    // 玩家当前级别积分
    private int currentScore;
    // 当前级别开始时间
    private long startTime;
    // 当前级别已用时间
    private int elapsedTime;

    public int getLevelNo() {
        return levelNo;
    }

    public void setLevelNo(int levelNo) {
        this.levelNo = levelNo;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(int elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public void play() {
        printHeader();
        Game game = new Game(this);
        Scanner scanner = new Scanner(System.in);
        // 外循环 玩家需要玩的等级
        for (int level = levelNo; level <= LevelParam.LEVEL.length; level++) {
            // 内循环 当前级别需要完成的次数
            for (int strTimes = 0; strTimes < LevelParam.LEVEL[level - 1].getStrTimes(); strTimes++) {
                // 当前级别开始时间
                startTime = System.currentTimeMillis();
                String outStr = game.printSrt();
                String inStr = scanner.next();
                long endTime = System.currentTimeMillis();
                int elapsedTime = (int) ((endTime - startTime) / 1000);
                // 如果大于时间限制
                if (elapsedTime > LevelParam.LEVEL[level - 1].getTimeLimit()) {
                    System.out.println("您输入的太慢,已经超时");
                    System.exit(0);
                }
                // 设置用时
                this.elapsedTime = elapsedTime;
                game.printResult(outStr, inStr);
            }
            // 当前级别完成(级别+1)
            ++levelNo;
            // 清空当前等级积分
            currentScore = 0;
        }

    }


    public void printHeader() {
        System.out.println("游戏开始");
    }

    public static void main(String[] args) {
        Player player = new Player();
        player.play();
    }
}
