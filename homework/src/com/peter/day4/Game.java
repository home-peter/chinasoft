package com.peter.day4;

import java.util.Random;

/**
 * Created by zmy on 2018/3/26.
 */
public class Game {
    private Player player;

    public Game() {
    }

    public Game(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * 输出字符串，返回字符串用于和玩家输入比较
     *
     * @return
     */
    public String printSrt() {
        Level currentLevel = LevelParam.LEVEL[player.getLevelNo() - 1];
        String randomSrt = makeRandomStr(currentLevel.getStrLength());
        System.out.println(randomSrt);
        return randomSrt;
    }

    /**
     * 比较游戏输出out和玩家输入in，根据比较结果输出相应信息
     *
     * @param out
     * @param in
     */
    public void printResult(String out, String in) {
        Level currentLevel = LevelParam.LEVEL[player.getLevelNo() - 1];
        if (out.equals(in)) {
            // 加上积分
            player.setCurrentScore(player.getCurrentScore() + currentLevel.getPerScore());
            System.out.println("输入正确," + "您的积分" + player.getCurrentScore()
                    + "您的级别" + player.getLevelNo() + ",用时" + player.getElapsedTime() + "秒");
        } else {
            System.out.println("输入错误,退出!");
            System.out.println("您最后的成绩为:" + "您的积分" + player.getCurrentScore()
                    + "您的级别" + player.getLevelNo() + ",用时" + player.getElapsedTime() + "秒");
            System.exit(0);
        }
    }

    private static String makeRandomStr(int length) {
        Random random = new Random();
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int r = random.nextInt(26) + 65;
            char c = (char) r;
            buffer.append(c);
        }
        return buffer.toString();
    }

    public static void main(String[] args) {
        System.out.println(makeRandomStr(5));
    }

}
