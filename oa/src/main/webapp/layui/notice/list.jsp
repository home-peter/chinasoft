<%--
  Created by IntelliJ IDEA.
  User: zmy
  Date: 2018/6/28
  Time: 14:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="fixed-table-toolbar">
    <div class="columns pull-left">
        <button class="btn  btn-primary" onclick="add()" type="button">
            <i aria-hidden="true" class="fa fa-plus hidden"></i>添加
        </button>
    </div>

</div>
<table id="table"></table>
<script>
    $('#table').bootstrapTable({
        method: 'GET',
        url: "/noticeAndUser", // 获取表格数据
        dataType: 'json', // 服务器返回的数据类型
        striped: true, // 设置为true会有隔行变色效果
        pagination: true, // 设置为 true 会在表格底部显示分页条
        pageNumber: 1, // 如果设置了分页，首页页码
        pageSize: 10,//如果设置了分页，页面数据条数
        sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
        columns: [
            {
                field: 'id',
                title: '通知编号',
                visible: false
            }, {
                field: 'title',
                title: '名称',
                <%--target="_blank"   href="${ctx}/'+row.id+'/noticeDetails">--%>
                formatter:function (value,row,index) {
                    var noticeTitle= '<a onclick="details(\'' + row.id + '\')"> '+value+'</a>';
                    return noticeTitle;
                }
            }, {
                field: 'user.username',
                title: '发布人'
            }, {
                field: 'status',
                title: '状态',
                formatter:function (value,row,index) {
                    if(value==1){
                        value="已发布";
                    }else{
                        value="未发布"
                    }
                    return value;
                }
            }, {
                field: 'timeRelease',
                title: '发布时间',
                formatter: function (value, row, index) {
                    return changeDateFormat(value)
                }

            }, {
                field: 'options',
                title: '操作',
                formatter: function (value, row, index) {
                    var editButton = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary btn-sm " href="#" title="编辑"  onclick="edit(\'' + row.id + '\')">编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    var deleteButton = '<a class="btn btn-warning btn-sm " href="#" title="删除" onclick="deleted(\'' + row.id + '\')">删除</a>';
                    return editButton + deleteButton;
                }
            }]


    });

    function edit(id) {
        console.log(id);
        layer.open({
            type: 2,
            title: '修改',
            maxin: true,
            area: ['800px', '520px'],
            content: "/"+id+"/noticeUpdate",
            method:"GET"
        })

    }

//    function remove(id) {
//        layer.open({
//            type: 2,
//            title: '删除',
//            maxin: true,
//            area: ['300px', '200px'],
//            content: "/"+id+"/delete"
//        })
//
//    }
    function details(id) {
        layer.open({
            type: 2,
            title: '通告详情',
            maxin: true,
            area: ['800px', '520px'],
            content: "/"+id+"/noticeDetails",
            method:"GET"
        })
    }
    function deleted(id) {
        layer.confirm('确定要删除选中的记录?', {
            btn: ['确定', '取消']
        }, function () {
            $.ajax({
                type: "DELETE",
                url: "/"+id+"/delete",
                success: function (data) {
                    if (data.code == 0) {
                        layer.msg(data.info);
                        reLoad();
                    } else {
                        parent.layer.alert(data.info);
                    }
                }
            })
        })
    }
    function add() {
        layer.open({
            type: 2,
            title: '添加',
            maxin: true,
            area: ['800px', '520px'],
            content: "${ctx}/layui/notice/add.jsp"
        })
    }

   //修改——转换日期格式(时间戳转换为datetime格式)

    function changeDateFormat(cellval) {
        var dateVal = cellval + "";
        if (cellval != null) {
            var date = new Date(parseInt(dateVal.replace("/Date(", "").replace(")/", ""), 10));
            var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
            var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();

            var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
            var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
            var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

            return date.getFullYear() + "-" + month + "-" + currentDate + " " + hours + ":" + minutes + ":" + seconds;
        }
    }
</script>