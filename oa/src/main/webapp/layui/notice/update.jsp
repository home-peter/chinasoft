<%--
  Created by IntelliJ IDEA.
  User: Lfy
  Date: 2018/6/28
  Time: 17:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${ctx}/static/css/bootstrap.min.css">
<link rel="stylesheet" href="${ctx}/static/css/layui.css">
<link rel="stylesheet" href="${ctx}/static/plugins/bootstrap-table/bootstrap-table.css">
<script src="${ctx}/static/plugins/jquery/jquery-3.3.1.js"></script>
<script src="${ctx}/static/plugins/bootstrap/bootstrap.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/static/plugins/layui/layui.js"></script>
<form class="layui-form" action="">
    <input name="uid" type="hidden" value="${user.userId}">
    <input name="id" type="hidden" value="1">
    <div class="layui-form-item">
        <label class="layui-form-label">标题</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input" style="width: 300px" value="${notice.title}">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label" style="width: auto">通告状态</label>
        <div class="layui-input-block">

            <input type="radio" name="mark" value="0" title="一般" <c:if test="${notice.mark == 0}">checked</c:if>/>
            <input type="radio" name="mark" value="1" title="紧急" <c:if test="${notice.mark == 1}">checked</c:if>/>
        </div>
    </div>

    <div class="layui-inline">
        <label class="layui-form-label" style="width: auto">显示开始</label>
        <div class="layui-input-block">
            <input type="text" name="timeBegin" id="timeBegin" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input" value="<fmt:formatDate value="${notice.timeBegin}" type="both"></fmt:formatDate>">
        </div>
    </div>

    <div class="layui-inline">
        <label class="layui-form-label" style="width: auto">显示结束</label>
        <div class="layui-input-block">
            <input type="text" name="timeEnd" id="timeEnd" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input" value="<fmt:formatDate value="${notice.timeEnd}" type="both"></fmt:formatDate>">
        </div>
    </div>

    <div class="layui-inline">
        <label class="layui-form-label" style="width: auto">发布时间</label>
        <div class="layui-input-block">
            <input type="text" name="timeRelease" id="timeRelease" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input" value="<fmt:formatDate value="${notice.timeRelease}" type="both"></fmt:formatDate>">
        </div>
    </div>



    <div class="layui-form-item">
        <label class="layui-form-label" style="width: auto">发布人:&nbsp;&nbsp;&nbsp;&nbsp;${notice.user.username}</label>
    </div>

    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label" style="width: auto">通告内容</label>
        <div class="layui-input-block">
            <textarea name="content" placeholder="请输入内容" class="layui-textarea" >${notice.content}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="release">发布</button>
            <button id="saveBtn" class="layui-btn" lay-submit lay-filter="save">保存</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>



<script>

    $(function () {
        layui.use(['form', 'laydate'], function () {
            var form = layui.form
                , laydate = layui.laydate;
            laydate.render({
                elem: '#timeBegin'
            });
            laydate.render({
                elem: '#timeEnd'
            });
            laydate.render({
                elem: '#timeRelease'
            });


            //监听提交
            form.on('submit(release)', function (data) {
                data.field.status = 1;
                data.field.timeBegin = new Date(data.field.timeBegin).getTime();
                data.field.timeEnd = new Date(data.field.timeEnd).getTime();
                data.field.timeRelease = new Date(data.field.timeRelease).getTime();
                console.log(data.field);
                $.ajax({
                        type:"put",
                        url: "/notice",
                        contentType: "application/json",
                        data:JSON.stringify(data.field) ,
                        success: function (result) {
                            alert(result);
                        }
                    }
                );
                return false;
            });

            form.on('submit(save)', function (data) {
                data.field.status = 0;
                data.field.timeBegin = new Date(data.field.timeBegin).getTime();
                data.field.timeEnd = new Date(data.field.timeEnd).getTime();
                data.field.timeRelease = new Date(data.field.timeRelease).getTime();
                $.ajax({
                        type:"put",
                        url: "/notice",
                        contentType: "application/json",
                        data:JSON.stringify(data.field) ,
                        success: function (result) {
                            alert(result);
                        }
                    }
                );
                return false;
            });

            form.render();
        });
    })


</script>