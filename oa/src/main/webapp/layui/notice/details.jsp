<%--
  Created by IntelliJ IDEA.
  User: zmy
  Date: 2018/6/28
  Time: 14:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" href="${ctx}/static/css/bootstrap.min.css">
<link rel="stylesheet" href="${ctx}/static/css/layui.css">
<link rel="stylesheet" href="${ctx}/static/plugins/bootstrap-table/bootstrap-table.css">
<script src="${ctx}/static/plugins/jquery/jquery-3.3.1.js"></script>
<script src="${ctx}/static/plugins/bootstrap/bootstrap.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/static/plugins/layui/layui.js"></script>


<body >
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-input-inline">
            <div class="layui-card-header" style="width: 100px "  >${notice.title}</div>
            <label class="layui-form-label" style="width: auto  "><h4>发布日期:<fmt:formatDate value="${notice.timeRelease}"></fmt:formatDate></h4></label>
        </div>



    </div>
    <div class="layui-card-body">
        ${notice.content}
    </div>

</div>


</body>
