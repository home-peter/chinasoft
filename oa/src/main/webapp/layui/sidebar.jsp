<%--
  Created by IntelliJ IDEA.
  User: zmy
  Date: 2018/6/28
  Time: 12:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-side-scroll">
    <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
    <ul id="side-nav" class="layui-nav layui-nav-tree" lay-filter="test">
        <li class="layui-nav-item layui-nav-itemed">
            <a href="javascript:;">通告管理</a>
            <dl class="layui-nav-child">
                <shiro:hasPermission name="notice:list">
                    <dd><a id="noticeListBtn" href="${ctx}/layui/notice/list.jsp">通告列表</a></dd>
                </shiro:hasPermission>
            </dl>
        </li>
        <li class="layui-nav-item">
            <a class="" href="javascript:;">行政管理</a>
            <dl class="layui-nav-child">
                <shiro:hasPermission name="Adm:meet">
                    <dd><a href="${ctx}/layui/administration/meet.jsp" id="testLoad">会议室管理</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="Adm:car">
                    <dd><a href="${ctx}/layui/administration/car.jsp">用车管理</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="Adm:chapter">
                    <dd><a href="${ctx}/layui/administration/chapter.jsp">用章管理</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="Adm:book">
                    <dd><a href="${ctx}/layui/administration/book.jsp">图书借阅申请</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="Adm:card">
                    <dd><a href="${ctx}/layui/administration/card.jsp">名片印制流程</a></dd>
                </shiro:hasPermission>
            </dl>
        </li>
        <li class="layui-nav-item">
            <a href="javascript:;">人事管理</a>
            <dl class="layui-nav-child">
                <shiro:hasPermission name="personnel:leave">
                    <dd><a href="${ctx}/layui/leaveSlip/main.jsp">请假申请</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="personnel:leaveList">
                    <dd><a href="${ctx}/layui/leaveSlip/list.jsp">请假列表</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="personnel:business">
                    <dd><a href="${ctx}/layui/travelRequests/main.jsp">出差申请</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="personnel:businessList">
                    <dd><a href="${ctx}/layui/travelRequests/list.jsp">出差列表</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="personnel:income">
                    <dd><a href="${ctx}/layui/incomeCertificate/main.jsp">收入证明流程</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="personnel:incomeList">
                    <dd><a href="${ctx}/layui/incomeCertificate/list.jsp">收入证明流程列表</a></dd>
                </shiro:hasPermission>
            </dl>
        </li>
        <li class="layui-nav-item">
            <a href="javascript:void(0);">系统设置</a>
            <dl class="layui-nav-child">
                <shiro:hasPermission name="system:role">
                    <dd><a href="${ctx}/layui/roleManager/list.jsp">角色管理</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="system:user">
                    <dd><a href="${ctx}/layui/userManager/list.jsp">用户管理</a></dd>
                </shiro:hasPermission>
                <shiro:hasPermission name="system:log">
                    <dd><a href="${ctx}/layui/logManager/list.jsp">操作日志</a></dd>
                </shiro:hasPermission>
            </dl>
        </li>
    </ul>
</div>
