<%--
  Created by IntelliJ IDEA.
  User: zmy
  Date: 2018/6/28
  Time: 12:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-logo">OA在线系统</div>
<ul class="layui-nav layui-layout-right">
    <li class="layui-nav-item"><a id="logout" href="javascript:void(0)">注销</a></li>
</ul>
<script>
    $(function () {
        $("#logout").click(function () {
            $.ajax({
                type: "GET",
                url: "${ctx}/logout",
                async: false,
                success: function (result) {
                    if (result.code == 0) {
                        layer.msg('注销成功', {
                            offset: '15px'
                            , icon: 1
                            , time: 1000
                        }, function () {
                            location.href = '${ctx}/login'; //后台主页
                        });
                    } else {
                        layer.msg(result.info);
                    }
                }
            });
            return false;
        })
    })
</script>