<%--
  Created by IntelliJ IDEA.
  User: 57309
  Date: 2018/6/30
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="fixed-table-toolbar">
    <div class="columns pull-right">
        <button class="btn  btn-primary" onclick="addRole()" type="button">
            <i aria-hidden="true" class="fa fa-plus hidden"></i>添加
        </button>
    </div>
</div>
<table id="table"></table>
<script>
    $('#table').bootstrapTable({
        method: 'GET',
        url: "${ctx}/role/list", // 获取表格数据
        dataType: 'json', // 服务器返回的数据类型
        striped: true, // 设置为true会有隔行变色效果
        pagination: true, // 设置为 true 会在表格底部显示分页条
        pageNumber: 1, // 如果设置了分页，首页页码
        pageSize: 10,//如果设置了分页，页面数据条数
        sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
        columns: [
            {
                field: 'roleId',
                title: '角色编号',
                visible: true
            }, {
                field: 'name',
                title: '名称'
            }, {
                field: 'status',
                title: '状态',
                formatter: function (value, row, index) {
                    var status = row.status;
                    if (status == 0) {
                        return "正常";
                    } else if (status == 1) {
                        return "已删除";
                    }
                }
            }, {
                field: 'options',
                title: '操作',
                formatter: function (value, row, index) {
                    var assignButton = '<a class="btn btn-primary btn-sm" title="修改权限" onclick="assign(\'' + row.roleId + '\')">修改权限</a>';
                    var editButton = '<a class="btn btn-warning btn-sm" title="编辑" onclick="edit(\'' + row.roleId + '\')">编辑角色</a>';
                    var deleteButton = '<a class="btn btn-danger btn-sm" title="删除" onclick="deleted(\'' + row.roleId + '\')">删除</a>';
                    return assignButton + editButton + deleteButton;
                }
            }]
    });

    function reLoad() {
        $('#table').bootstrapTable('refresh');
    }

    function assign(roleId) {
        layer.open({
            type: 2,
            title: '添加权限',
            maxin: true,
            area: ['800px', '520px'],
            content: "${ctx}/roleRight/" + roleId
        })
    }

    function edit(roleId) {
        layer.open({
            type: 2,
            title: '修改角色',
            maxin: true,
            area: ['800px', '520px'],
            content: "${ctx}/role/edit/" + roleId
        })
    }

    function addRole() {
        layer.open({
            type: 2,
            title: '添加',
            maxin: true,
            area: ['800px', '520px'],
            content: "${ctx}/layui/roleManager/add.jsp"
        })
    }

    function deleted(roleId) {
        layer.confirm('确定要删除选中的记录?', {
            btn: ['确定', '取消']
        }, function () {
            $.ajax({
                type: "DELETE",
                url: "${ctx}/role/" + roleId + "/remove",
                success: function (data) {
                    if (data.code == 0) {
                        layer.msg(data.info);
                        reLoad();
                    } else {
                        parent.layer.alert(data.info);
                    }
                }
            })
        })
    }
</script>
