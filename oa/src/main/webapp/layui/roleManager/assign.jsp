<%--
  Created by IntelliJ IDEA.
  User: 57309
  Date: 2018/7/1
  Time: 0:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="${ctx}/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/static/css/layui.css">
</head>
<body>
<form class="layui-form">
    <input type="hidden" id="roleId" name="roleId" value="${role.roleId}">
    <c:forEach var="menuTreeNode" items="${menuTreeNodeList}">
        <div class="layui-form-item">
            <label class="layui-form-label">${menuTreeNode.menu.name}</label>
            <div class="layui-input-block">
                <c:forEach items="${menuTreeNode.childList}" var="subMenu">
                    <c:set var="existed" value="false"/>
                    <c:forEach items="${role.menuSet}" var="assignedMenu">
                        <c:if test="${assignedMenu.menuId eq subMenu.menuId}">
                            <c:set var="existed" value="true"/>
                        </c:if>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${existed}">
                            <input type="checkbox" name="menuId" title="${subMenu.name}" value="${subMenu.menuId}"
                                   checked>
                        </c:when>
                        <c:otherwise>
                            <input type="checkbox" name="menuId" title="${subMenu.name}" value="${subMenu.menuId}">
                        </c:otherwise>
                    </c:choose>
                </c:forEach>

            </div>
        </div>
    </c:forEach>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button id="submitBtn" class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="${ctx}/static/plugins/jquery/jquery-3.3.1.js"></script>
<script src="${ctx}/static/plugins/bootstrap/bootstrap.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/static/plugins/layui/layui.js"></script>
<script>
    $(function () {
        layui.use(['form', 'layer'], function () {
            var form = layui.form;
            var layer = layui.layer;

            form.render();
            // 给按钮添加点击事件
            $("#submitBtn").click(function () {
                var roleId = $("#roleId").val();
                $.ajax({
                    type: 'POST',
                    url: '${ctx}/roleRight/assign/' + roleId,
                    data: $(".layui-form").serialize(),
                    success: function (result) {
                        if (result.code == 0) {
                            parent.layer.msg("赋权成功");
                            parent.reLoad();
                            //先得到当前iframe层的索引
                            var index = parent.layer.getFrameIndex(window.name);
                            //再执行关闭
                            parent.layer.close(index);
                        } else {
                            layer.msg(result.info);
                        }
                    }
                });
                return false;
            });
        });
    })

</script>
</body>
</html>

