<%--
  Created by IntelliJ IDEA.
  User: 57309
  Date: 2018/6/30
  Time: 20:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="${ctx}/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/static/css/layui.css">
</head>
<body>
<form class="layui-form">
    <div class="layui-form-item">
        <label class="layui-form-label">名称</label>
        <div class="layui-input-block">
            <input type="text" name="name" required lay-verify="required" placeholder="请输入角色名称" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="${ctx}/static/plugins/jquery/jquery-3.3.1.js"></script>
<script src="${ctx}/static/plugins/bootstrap/bootstrap.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/static/plugins/layui/layui.js"></script>
<script>
    //Demo
    layui.use('form', function () {
        var form = layui.form;

        //监听提交
        form.on('submit(formDemo)', function (data) {
            $.ajax({
                url: '${ctx}/role/add',
                type: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(data.field),
                success: function (result) {
                    if (result.code == 0) {
                        parent.layer.msg("添加成功");
                        parent.reLoad();
                        //先得到当前iframe层的索引
                        var index = parent.layer.getFrameIndex(window.name);
                        //再执行关闭
                        parent.layer.close(index);
                    } else {
                        layer.msg(result.info);
                    }
                }
            });
            return false;
        });

        form.render();
    });
</script>
</body>
</html>

