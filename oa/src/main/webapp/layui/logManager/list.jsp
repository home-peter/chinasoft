<%--
  Created by IntelliJ IDEA.
  User: zmy
  Date: 2018/7/2
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<table id="table"></table>
<script>
    $('#table').bootstrapTable({
        method: 'GET',
        url: "${ctx}/log/list", // 获取表格数据
        dataType: 'json', // 服务器返回的数据类型
        striped: true, // 设置为true会有隔行变色效果
        pagination: true, // 设置为 true 会在表格底部显示分页条
        pageNumber: 1, // 如果设置了分页，首页页码
        pageSize: 10,//如果设置了分页，页面数据条数
        sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
        columns: [
            {
                field: 'log.logId',
                title: '日志编号',
                visible: true
            }, {
                field: 'user.username',
                title: '用户名'
            },
            {
                field: 'log.note',
                title: '事件'
            },
            {
                field: 'log.gmtCreated',
                title: '操作时间'
            }
        ]
    });

    function reLoad() {
        $('#table').bootstrapTable('refresh');
    }
</script>