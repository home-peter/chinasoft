<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>OA项目</title>
    <link rel="stylesheet" href="${ctx}/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/static/css/layui.css">
    <link rel="stylesheet" href="${ctx}/static/plugins/bootstrap-table/bootstrap-table.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header"></div>

    <div class="layui-side layui-bg-black"></div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;"></div>
    </div>

    <div class="layui-footer"></div>
</div>
<script src="${ctx}/static/plugins/jquery/jquery-3.3.1.js"></script>
<script src="${ctx}/static/plugins/bootstrap/bootstrap.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/static/plugins/layui/layui.js"></script>
<script>
    $(function () {
        init();
        initSideBar();
        loadContent();

        layui.use(['element', 'layer'], function () {
            var element = layui.element;
            var layer = layui.layer;
        });
    });

    function init() {
        loadHeader();
        loadSideBar();
        loadFooter();
    }

    function loadHeader() {
        $.ajax({
            url: "${ctx}/layui/header.jsp",
            async: false,
            success: function (html) {
                $(".layui-header").html(html);
            }
        })
    }

    function loadSideBar() {
        $.ajax({
            url: "${ctx}/layui/sidebar.jsp",
            async: false,
            success: function (html) {
                $(".layui-side").html(html);
            }
        })
    }

    function loadContent() {
        $("#noticeListBtn").click();
        console.log($("#noticeListBtn")[0]);
    }

    function loadFooter() {
        $.ajax({
            url: "${ctx}/layui/footer.jsp",
            async: false,
            success: function (html) {
                $(".layui-footer").html(html);
            }
        })
    }

    function initSideBar() {
        // 获取侧边栏按钮
        var menuList = $("#side-nav li dl a");
        $.each(menuList, function (index, menuBtn) {
            // 给菜单按钮绑定事件
            $(menuBtn).click(function () {
                // 清空内容区内容
                $(".layui-body div").html("");

                // 获取链接路径
                var targetUrl = $(this).attr('href');
                if (!targetUrl) {
                    // 目标路径为空 直接返回 什么也不做
                    return;
                }

                // 请求真实内容
                $.ajax({
                    url: targetUrl,
                    cache: false,
                    async: true,
                    success: function (html) {
                        $(".layui-body div").html(html);
                    }
                });
                return false;
            });
        });
    }

</script>
</body>
</html>
