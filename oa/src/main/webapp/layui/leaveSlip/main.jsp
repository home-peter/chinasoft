<%--
  Created by IntelliJ IDEA.
  User: Fang
  Date: 2018/6/28
  Time: 16:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form class="layui-form layui-form-pane">
    <input name="id" type="hidden" value="${id}">
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;">姓名</label>
        <div class="layui-input-block">
            <input type="text" name="name" id="name" required lay-verify="required" placeholder="请输入姓名"
                   autocomplete="off"
                   class="layui-input" style="width: 400px;float: left;">
        </div>
        <label class="layui-form-label" style="margin-left: 20px;margin-top: -36px;">部门</label>
        <div class="layui-input-inline" style="width: 400px; float: left; margin-top: -36px;">
            <select name="department" id="department" lay-filter="department">
                <option></option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;margin-top: 36px;">开始时间</label>
        <div class="layui-input-block">
            <input type="text" name="starttime" id="starttime" placeholder="开始时间yyyy-mm-dd" autocomplete="off"
                   class="layui-input" style="width: 400px;float: left;margin-top: 36px;">
        </div>
        <label class="layui-form-label" style="margin-left: 20px;">结束时间</label>
        <div class="layui-input-block">
            <input type="text" name="finishtime" id="finishtime" placeholder="结束时间yyyy-mm-dd" autocomplete="off"
                   class="layui-input" style="width: 400px;float: left; ">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;margin-top: 36px;">请假天数</label>
        <div class="layui-input-block">
            <input type="text" name="days" id="days" required lay-verify="required" placeholder="请输入请假天数"
                   autocomplete="off"
                   class="layui-input" style="width: 400px; float: left;margin-top: 36px;">
        </div>
        <label class="layui-form-label" style="margin-left: 20px">请假类型</label>
        <div class="layui-input-inline" style="width: 400px; float: left; ">
            <select name="type" id="type">
                <option value=""></option>
                <option value="事假">事假</option>
                <option value="病假">病假</option>
                <option value="丧假">丧假</option>
                <option value="婚假">婚假</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 100px;margin-top: 36px;">请假事由</label>
        <div class="layui-input-block">
            <input type="text" name="reason" id="reason" required lay-verify="required" placeholder="请输入请假事由"
                   autocomplete="off"
                   class="layui-input" style="width: 400px; float: left; margin-top: 36px">
        </div>
        <label class="layui-form-label" style="margin-left: 20px">审批人</label>
        <div class="layui-input-inline" style="width: 400px; float: left; ">
            <select name="aoName" id="aoName" lay-filter="user">
                <option></option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="add">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script>
//    loadRoleList();

    $(function () {
        layui.use(['form', 'laydate'], function () {
            form = layui.form,
                laydate = layui.laydate;
            laydate.render({
                elem: '#starttime' //指定元素
            });
            laydate.render({
                elem: '#finishtime' //指定元素
            });
            //监听提交
            form.on('submit(add)', function (data) {
                data.field.starttime = new Date(data.field.starttime).getTime();
                data.field.finishtime = new Date(data.field.finishtime).getTime();
                console.log(JSON.stringify(data.field));
                $.ajax({
                    type: "post",
                    url: "${ctx}/LeaveSlip/add",
                    contentType: "application/json",
                    data: JSON.stringify(data.field),
                    success: function (msg) {
                        if (msg.code == 0) {
                            layer.msg("申请成功");

                        } else {
                            layer.msg(result.info);
                        }
                    }
                });
                return false;
            });
            loadDepartmentList();
            loadUserList();
            form.render()
        });
    });

    function loadUserList() {
        // 加载角色列表
        $.ajax({
            type: 'GET',
            url: '${ctx}/user/list',
            async:false,
            success: function (result) {
                console.log(result);
                if (result.code == 0) {
                    var options = "";
                    var userList = result.data;
                    console.log(userList);
                    $.each(userList, function (index, user) {
                        options += "<option value='" + user.username + "'>" + user.username + "</option>";
                    });
                    // 添加到下拉框中
                    $("#aoName").append(options);
                } else {
                    $("#aoName").append("<option>暂无数据</option>");
                }
            }

        });
    }

function loadDepartmentList() {
    // 加载角色列表
    $.ajax({
        type: 'GET',
        url: '${ctx}/department/list',
        async:false,
        success: function (result) {
            console.log(result);
            if (result.code == 0) {
                var options = "";
                var departmentList = result.data;
                console.log(departmentList);
                $.each(departmentList, function (index, department) {
                    options += "<option value='" + department.name + "'>" + department.name + "</option>";
                });
                // 添加到下拉框中
                $("#department").append(options);
            } else {
                $("#department").append("<option>暂无数据</option>");
            }
        }

    });
}
</script>
</body>
</html>