<%--
  Created by IntelliJ IDEA.
  User: Fang
  Date: 2018/7/4
  Time: 9:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<table id="table"></table>
<script>
    $('#table').bootstrapTable({
        method: 'GET',
        url: "${ctx}/LeaveSlip/findAll", // 获取表格数据
        dataType: 'json', // 服务器返回的数据类型
        striped: true, // 设置为true会有隔行变色效果
        pagination: true, // 设置为 true 会在表格底部显示分页条
        pageNumber: 1, // 如果设置了分页，首页页码
        pageSize: 10,//如果设置了分页，页面数据条数
        sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
        columns: [
            {
                field: 'id',
                title: '申请编号',
                visible: false
            }, {
                field: 'name',
                title: '申请人名字',
            },  {
                field: 'department',
                title: '所在部门'
            },{
                field: 'starttime',
                title: '申请开始时间'
            }, {
                field: 'finishtime',
                title: '申请结束时间'
            }, {
                field: 'days',
                title: '请假天数'
            }, {
                field: 'type',
                title: '请假类型'
            }, {
                field: 'reason',
                title: '申请事由'
            }, {
                field: 'aoName',
                title: '审批人姓名'
            }, {
                field: 'options',
                title: '操作',
                formatter: function (value, row, index) {
                    var deleteButton = '<a class="btn btn-warning btn-sm " href="#" title="删除" onclick="remove(\'' + row.id + '\')"><i class="glyphicon glyphicon-remove">删除</i></a>';
                    return deleteButton;
                }
            }]


    });
    function remove(id) {
        layer.confirm('确定要删除选中的记录?', {
            btn: ['确定', '取消']
        }, function () {
            $.ajax({
                type: "DELETE",
                url: "${ctx}/LeaveSlip/" + id + "/delete",
                success: function (data) {
                    if (data.code == 0) {
                        layer.msg(data.info);
                        reLoad();
                    } else {
                        parent.layer.alert(data.info);
                    }
                }
            })
        })

    }
    //修改——转换日期格式(时间戳转换为datetime格式)

    function changeDateFormat(cellval) {
        var dateVal = cellval + "";
        if (cellval != null) {
            var date = new Date(parseInt(dateVal.replace("/Date(", "").replace(")/", ""), 10));
            var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
            var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();

            var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
            var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
            var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

            return date.getFullYear() + "-" + month + "-" + currentDate + " " + hours + ":" + minutes + ":" + seconds;
        }
    }
</script>
