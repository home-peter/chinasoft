<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>登录在线OA</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <link rel="stylesheet" href="${ctx}/static/css/layui.css" media="all">
    <link rel="stylesheet" href="${ctx}/static/css/layuiadmin/admin.css" media="all">
    <link rel="stylesheet" href="${ctx}/static/css/layuiadmin/login.css" media="all">
</head>
<body>

<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">

    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>Online OA</h2>
            <p>在线OA系统</p>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body">
            <form id="loginForm" class="layui-form" action="">
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-username"
                           for="LAY-user-login-username"></label>
                    <input type="text" name="username" id="LAY-user-login-username" lay-verify="required"
                           placeholder="用户名"
                           class="layui-input">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-password"
                           for="LAY-user-login-password"></label>
                    <input type="password" name="password" id="LAY-user-login-password" lay-verify="required"
                           placeholder="密码" class="layui-input">
                </div>

                <div class="layui-form-item" style="margin-bottom: 20px;">
                    <input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
                    <a href="#" class="layadmin-user-jump-change layadmin-link"
                       style="margin-top: 7px;">忘记密码？</a>
                </div>
                <div class="layui-form-item">
                    <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="login">登 入</button>
                </div>
            </form>
        </div>
    </div>

    <div class="layui-trans layadmin-user-login-footer">

        <p>© 2018 <a href="#" target="_blank">onlineOA.com</a></p>
    </div>
</div>
<script src="${ctx}/static/plugins/jquery/jquery-3.3.1.js"></script>
<script src="${ctx}/static/plugins/layui/layui.js"></script>
<script>
    $(function () {
        layui.use(['form', 'layer'], function () {
            var form = layui.form;
            var layer = layui.layer;

            //监听提交
            form.on('submit(login)', function (data) {
                console.log(data);
                $.ajax({
                    type: 'POST',
                    url: '${ctx}/login',
                    data: $("#loginForm").serialize(),
                    success: function (result) {
                        if (result.code == 0) {
                            layer.msg('登入成功', {
                                offset: '15px'
                                , icon: 1
                                , time: 1000
                            }, function () {
                                location.href = '${ctx}/index'; //后台主页
                            });
                        } else {
                            layer.msg(result.info);
                        }
                    }
                });
                return false;
            });
        });
    });

</script>
</body>
</html>