<%--
  Created by IntelliJ IDEA.
  User: 57309
  Date: 2018/7/1
  Time: 21:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="${ctx}/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ctx}/static/css/layui.css">
</head>
<body>
<form class="layui-form" id="editUserForm">
    <input id="userId" type="hidden" name="userId" value="${user.userId}">
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-block">
            <input type="text" name="password" required lay-verify="required" placeholder="请输入密码" autocomplete="off"
                   class="layui-input" value="${user.password}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">真实姓名</label>
        <div class="layui-input-block">
            <input type="text" name="realName" required lay-verify="required" placeholder="请输入用户真实姓名" autocomplete="off"
                   class="layui-input" value="${user.realName}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">所在部门</label>
        <div class="layui-input-block">
            <select id="departmentSelect" name="departmentId" lay-filter="department">
                <option>请选择部门</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-block">
            <c:choose>
                <c:when test="${user.gender eq '男'}">
                    <input type="radio" name="gender" value="男" title="男" checked>
                </c:when>
                <c:otherwise>
                    <input type="radio" name="gender" value="男" title="男">
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${user.gender eq '女'}">
                    <input type="radio" name="gender" value="女" title="女" checked>
                </c:when>
                <c:otherwise>
                    <input type="radio" name="gender" value="女" title="女">
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">职称</label>
        <div class="layui-input-block">
            <select id="jobSelect" name="jobId" lay-filter="job">
                <option>请选择职称</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">角色</label>
        <div class="layui-input-block">
            <select id="role" name="roleId" lay-filter="role">
                <option>请选择角色</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <div class="layui-input-block">
            <c:choose>
                <c:when test="${user.status eq 0}">
                    <input type="radio" name="status" value="0" title="正常" checked>
                </c:when>
                <c:otherwise>
                    <input type="radio" name="status" value="0" title="正常">
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${user.status eq 2}">
                    <input type="radio" name="status" value="2" title="冻结" checked>
                </c:when>
                <c:otherwise>
                    <input type="radio" name="status" value="2" title="冻结">
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="${ctx}/static/plugins/jquery/jquery-3.3.1.js"></script>
<script src="${ctx}/static/plugins/bootstrap/bootstrap.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="${ctx}/static/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/static/plugins/layui/layui.js"></script>
<script>
    // 加载部门列表
    loadDepartmentList();

    // todo加载角色列表
    loadRoleList();

    layui.use('form', function () {
        var form = layui.form;

        // 监听部门选择
        form.on('select(department)', function (data) {
            // 获取部门编号
            var departmentId = data.value;
            console.log('${ctx}/' + departmentId + '/jobList');
            // 根据部门编号异步加载职位
            $.ajax({
                type: 'GET',
                url: '${ctx}/' + departmentId + '/jobList',
                success: function (result) {
                    if (result.code == 0) {
                        $("#jobSelect option").remove();
                        var jobList = result.data;
                        if (jobList.length > 0) {
                            var options = "";
                            $.each(jobList, function (index, job) {
                                if (job.jobId == ${user.job.jobId}) {
                                    options += "<option selected value='" + job.jobId + "'>" + job.name + "</option>";
                                } else {
                                    options += "<option value='" + job.jobId + "'>" + job.name + "</option>";
                                }
                            });
                            console.log(options);
                            // 添加到下拉框中
                            $("#jobSelect").append(options);
                        } else {
                            $("#jobSelect").append("<option>暂无职称</option>");
                        }
                        form.render();
                    }
                }
            });
        });

        //监听提交
        form.on('submit(formDemo)', function (data) {
            console.log(JSON.stringify(data.field));
            var userId = $("#userId").val();
            $.ajax({
                url: '${ctx}/user/edit/' + userId,
                type: 'POST',
                data: JSON.stringify(data.field),
                contentType: 'application/json',
                success: function (result) {
                    if (result.code == 0) {
                        parent.layer.msg("添加成功");
                        parent.reLoad();
                        //先得到当前iframe层的索引
                        var index = parent.layer.getFrameIndex(window.name);
                        //再执行关闭
                        parent.layer.close(index);
                    } else {
                        layer.msg(result.info);
                    }
                }
            });
            return false;
        });

        form.render();
    });

    function loadDepartmentList() {
        // 加载部门列表
        $.ajax({
            type: 'GET',
            url: '${ctx}/department/list',
            success: function (result) {
                if (result.code == 0) {
                    var options = "";
                    var departmentList = result.data;
                    $.each(departmentList, function (index, department) {
                        if (department.departmentId == ${user.job.department.departmentId}) {
                            options += "<option selected value='" + department.departmentId + "'>" + department.name + "</option>";
                        } else {
                            options += "<option value='" + department.departmentId + "'>" + department.name + "</option>";
                        }
                    });
                    // 添加到下拉框中
                    $("#departmentSelect").append(options);
                }
            }
        });
    }

    function loadRoleList() {
        // 加载角色列表
        $.ajax({
            type: 'GET',
            url: '${ctx}/role/list',
            success: function (result) {
                if (result.code == 0) {
                    var options = "";
                    var roleList = result.data;
                    $.each(roleList, function (index, role) {
                        options += "<option value='" + role.roleId + "'>" + role.name + "</option>";
                    });
                    // 添加到下拉框中
                    $("#role").append(options);
                } else {
                    $("#role").append("<option>服务器异常,暂无数据,请稍后再试</option>");
                }
            }
        });
    }
</script>
</body>
</html>
