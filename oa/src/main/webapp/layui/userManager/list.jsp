<%--
  Created by IntelliJ IDEA.
  User: 57309
  Date: 2018/6/30
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="fixed-table-toolbar">
    <div class="columns pull-right">
        <button class="btn  btn-primary" onclick="addUser()" type="button">
            <i aria-hidden="true" class="fa fa-plus hidden"></i>添加
        </button>
    </div>
</div>
<table id="table"></table>
<script>
    $('#table').bootstrapTable({
        method: 'GET',
        url: "${ctx}/user/list", // 获取表格数据
        dataType: 'json', // 服务器返回的数据类型
        striped: true, // 设置为true会有隔行变色效果
        pagination: true, // 设置为 true 会在表格底部显示分页条
        pageNumber: 1, // 如果设置了分页，首页页码
        pageSize: 10,//如果设置了分页，页面数据条数
        sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
        columns: [
            {
                field: 'userId',
                title: '用户编号',
                visible: true
            }, {
                field: 'username',
                title: '用户名'
            },
            {
                field: 'password',
                title: '密码'
            },
            {
                field: 'realName',
                title: '真名'
            },
            {
                field: 'gender',
                title: '男'
            },
            {
                field: 'age',
                title: '年龄',
                visible: false
            },
            {
                field: 'phone',
                title: '手机',
                visible: false
            }, {
                field: 'status',
                title: '状态',
                formatter: function (value, row, index) {
                    var status = row.status;
                    if (status == 0) {
                        return "正常";
                    } else if (status == 1) {
                        return "已删除";
                    } else if (status == 2) {
                        return "已锁定";
                    }
                }
            }, {
                field: 'options',
                title: '操作',
                formatter: function (value, row, index) {
                    var editButton = '<a class="btn btn-warning btn-sm" title="编辑" onclick="edit(\'' + row.userId + '\')">修改用户</a>';
                    var deleteButton = '<a class="btn btn-danger btn-sm" title="删除用户" onclick="deleted(\'' + row.userId + '\')">删除用户</a>';
                    return editButton + deleteButton;
                }
            }]
    });

    function reLoad() {
        $('#table').bootstrapTable('refresh');
    }

    function edit(userId) {
        layer.open({
            type: 2,
            title: '修改用户',
            maxin: true,
            area: ['800px', '600px'],
            content: "${ctx}/user/edit/" + userId
        })
    }

    function addUser() {
        layer.open({
            type: 2,
            title: '添加',
            maxin: true,
            area: ['800px', '600px'],
            content: "${ctx}/layui/userManager/add.jsp"
        })
    }

    function deleted(userId) {
        layer.confirm('确定要删除选中的记录?', {
            btn: ['确定', '取消']
        }, function () {
            $.ajax({
                type: "DELETE",
                url: "${ctx}/user/" + userId + "/remove",
                success: function (data) {
                    if (data.code == 0) {
                        layer.msg(data.info);
                        reLoad();
                    } else {
                        parent.layer.alert(data.info);
                    }
                }
            })
        })
    }
</script>
