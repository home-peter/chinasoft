<%--
  Created by IntelliJ IDEA.
  User: zh
  Date: 2018/6/29
  Time: 9:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form class="layui-form layui-form-pane" action="">
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-block">
            <input type="text" name="name" required  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">申请时间</label>
        <div class="layui-input-block">
            <input type="text" name="time" required  lay-verify="required" placeholder="11/14/2009" autocomplete="off" class="layui-input" id="test1">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">部门</label>
        <div class="layui-input-block">
            <input type="text" name="department" required  lay-verify="required" placeholder="请输入部门" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用章类型</label>
        <div class="layui-input-block">
            <select name="type" lay-verify="required">
                <option value=""></option>
                <option value="公司用章">公司用章</option>
                <option value="财务用章">财务用章</option>
                <option value="人事用章">人事用章</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">请假事由</label>
        <div class="layui-input-block">
            <textarea name="account" placeholder="请输入请假事由" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">审批人</label>
        <div class="layui-input-block">
            <select name="approval" lay-verify="required">
                <option value=""></option>
                <option value="王五">王五</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item" style="margin-left: -110px">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    $(function () {
        //Demo
        layui.use('form', function () {
            var form = layui.form;

            //监听提交
            form.on('submit(formDemo)', function (data) {
//                layer.msg(JSON.stringify(data.field));
                data.field.time = new Date(data.field.time).getTime();
                $.ajax({
                    type:"POST", //四种提交类型：get、post、put、delete
                    url:"${ctx}/addChapterUse",
                    contentType: 'application/json',
                    data:JSON.stringify(data.field),
                    success:function(i){
                        if(i>0){
                            alert("申请成功")
                        }
                    }
                });
                return false;
            });
            form.render();
        });
    });
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#test1' //指定元素
        });
    });
</script>
</body>
</html>
