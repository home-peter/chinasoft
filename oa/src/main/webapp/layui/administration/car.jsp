<%--
  Created by IntelliJ IDEA.
  User: zh
  Date: 2018/6/28
  Time: 17:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="layui-tab">
    <ul class="layui-tab-title">
        <li class="layui-this">用车预定</li>
        <li>车辆信息管理</li>
    </ul>
    <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <form class="layui-form layui-form-pane" action="">
                <div class="layui-form-item">
                    <label class="layui-form-label">车辆编号</label>
                    <div class="layui-input-block">
                        <input type="text" name="carId" required lay-verify="required" placeholder="请输入车辆编号"
                               autocomplete="off" class="layui-input" style="width: 400px; float: left">
                    </div>
                    <label class="layui-form-label" style="margin-top: -36px;margin-left: 20px">用车人</label>
                    <div class="layui-input-block">
                        <input type="text" name="carMan" required lay-verify="required" placeholder="请输入用车人"
                               autocomplete="off" class="layui-input"
                               style="width: 400px; float: left; margin-top: -36px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">司机</label>
                    <div class="layui-input-block">
                        <input type="text" name="driver" required lay-verify="required" placeholder="请输入司机"
                               autocomplete="off" class="layui-input" style="width: 400px; float: left">
                    </div>
                    <label class="layui-form-label" style="margin-top: -36px;margin-left: 20px">随行人员</label>
                    <div class="layui-input-block">
                        <input type="text" name="entourage" required lay-verify="required" placeholder="请输入随行人员"
                               autocomplete="off" class="layui-input"
                               style="width: 400px; float: left; margin-top: -36px">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-form-item">
                        <label class="layui-form-label">起始时间</label>
                        <div class="layui-input-block">
                            <input type="text" name="beginTime" required lay-verify="required" placeholder="07/10/2007"
                                   autocomplete="off" class="layui-input" style="width: 400px; float: left" id="test1">
                        </div>
                        <label class="layui-form-label" style="margin-top: -36px;margin-left: 20px">结束时间</label>
                        <div class="layui-input-block">
                            <input type="text" name="overTime" required lay-verify="required" placeholder="07/25/2007"
                                   autocomplete="off" class="layui-input"
                                   style="width: 400px; float: left; margin-top: -36px" id="test2">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">用车部门</label>
                    <div class="layui-input-block">
                        <input type="text" name="department" required lay-verify="required" placeholder="请输入用车部门"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">目的地</label>
                    <div class="layui-input-block">
                        <input type="text" name="destination" required lay-verify="required" placeholder="请输入目的地"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">申请里程（公里）</label>
                    <div class="layui-input-block">
                        <input type="text" name="distance" required lay-verify="required" placeholder="请输入申请里程（公里）"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">用车事由</label>
                    <div class="layui-input-block">
                        <textarea name="account" placeholder="请输入内容" class="layui-textarea"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">审批人</label>
                    <div class="layui-input-block">
                        <select name="approval" lay-verify="required">
                            <option value=""></option>
                            <option value="王五">王五</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item" style="margin-left: -110px">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="layui-tab-item">
            <div class="fixed-table-toolbar">
                <div class="columns pull-left">
                    <button class="btn  btn-primary"  type="button" id="btn1">
                        <i aria-hidden="true" class="fa fa-plus hidden"></i>添加
                    </button>
                </div>
            </div>
            <table id="table"></table>
        </div>
    </div>
</div>

<script>
    function reLoad() {
        $('#table').bootstrapTable('refresh');
    }

    $(function () {
        //Demo
        layui.use('form', function () {
            var form = layui.form;

            //监听提交
            form.on('submit(formDemo)', function (data) {
                console.log(data.field)
                data.field.beginTime = new Date(data.field.beginTime).getTime();
                data.field.overTime = new Date(data.field.overTime).getTime();
                $.ajax({
                    type: 'PUT', //四种提交类型：get、post、put、delete
                    url: "${ctx}/addApplicationCar",
                    contentType: 'application/json',
                    data: JSON.stringify(data.field),
                    success: function (result) {
                        if (result.code == 0) {
                            parent.layer.msg("预定成功");
                            parent.reLoad();
                            //先得到当前iframe层的索引
                            var index = parent.layer.getFrameIndex(window.name);
                            //再执行关闭
                            parent.layer.close(index);
                        } else {
                            layer.msg(result.info);
                        }
                    }
                });
                return false;
            });
            form.render();
        });
    });
    $('#table').bootstrapTable({
        method: 'GET',
        url: '${ctx}/getAllCar', // 获取表格数据
        dataType: 'json', // 服务器返回的数据类型
        striped: true, // 设置为true会有隔行变色效果
        pagination: true, // 设置为 true 会在表格底部显示分页条
        pageNumber: 1, // 如果设置了分页，首页页码
        pageSize: 10,//如果设置了分页，页面数据条数
        sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
        columns: [
            {
                field: 'id',
                title: '序列号'
            },
            {
                field: 'carNumber',
                title: '车牌号'
            }, {
                field: 'type',
                title: '车型'
            }, {
                field: 'carId',
                title: '编号'
            }, {
                field: 'remarks',
                title: '备注'
            }, {
                field: 'options',
                title: '操作',
                formatter: function (value, row, index) {
                    var editButton = '<a class="btn btn-primary btn-sm " href="#" title="编辑" onclick="edit(\'' + row.id + '\')">修改</a>';
                    var deleteButton = '<a class="btn btn-warning btn-sm " href="#" title="删除" onclick="deleted(\'' + row.id + '\')">删除</a>';
                    return editButton + deleteButton;
                }
            }],
    });

    function edit(id) {
        layer.open({
            type: 2,
            title: '修改',
            maxin: true,
            area: ['800px', '520px'],
            content: "${ctx}/getCarById/"+id,
            anim: 1
        })
    }
    function deleted(id) {
        layer.confirm('确定要删除选中的记录?', {
            btn: ['确定', '取消']
        }, function () {
            $.ajax({
                type: "DELETE",
                url: "${ctx}/delCar/" + id,
                success: function (data) {
                    if (data.code == 0) {
                        parent.layer.msg("删除成功");
                        parent.reLoad();
                        //先得到当前iframe层的索引
                        var index = parent.layer.getFrameIndex(window.name);
                        //再执行关闭
                        parent.layer.close(index);
                    } else {
                        layer.msg(result.info);
                    }
                }
            })
        })
    }
    $('#btn1').on('click', function () {
        layer.open({
            type: 2,
            title: '添加',
            maxin: true,
            area: ['800px', '520px'],
            content: "${ctx}/layui/administration/addCar.jsp",
            anim: 1
        })
    });
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#test1' //指定元素
        });
        laydate.render({
            elem: '#test2' //指定元素
        });
    });

</script>
</body>
</html>
