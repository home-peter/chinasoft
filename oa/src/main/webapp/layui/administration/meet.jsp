<%--
  Created by IntelliJ IDEA.
  User: zh
  Date: 2018/6/28
  Time: 16:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="layui-tab">
    <ul class="layui-tab-title">
        <li class="layui-this">会议室预定</li>
        <li>会议室预定管理</li>
        <li>会议室管理</li>
    </ul>
    <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">

            <form class="layui-form layui-form-pane" action="">
                <div class="layui-form-item">
                    <label class="layui-form-label">预定时间</label>
                    <div class="layui-input-block">
                        <input id="oTime" type="text" name="meetTime" required lay-verify="required" placeholder="请输入预定时间"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item" style="margin-left: -110px">
                    <div class="layui-input-block">
                        <button id="submitBtn" class="layui-btn" lay-submit lay-filter="formDemo">查询</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            可预订会议室查询结果：<br>
            <table id="table1"></table>
            </form>

            <form class="layui-form layui-form-pane" action="">
                <div class="layui-form-item">
                    <label class="layui-form-label">预定人</label>
                    <div class="layui-input-block">
                        <input type="text" name="reservations" required lay-verify="required" placeholder="请输入预定人"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">会议室名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" required lay-verify="required" placeholder="请输入会议室名称"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">会议类型</label>
                    <div class="layui-input-block">
                        <select name="type" lay-verify="required">
                            <option value=""></option>
                            <option value="会议">会议</option>
                            <option value="培训">培训</option>
                            <option value="面试">面试</option>
                            <option value="会客">会客</option>
                            <option value="其他">其他</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">记录人</label>
                    <div class="layui-input-block">
                        <input type="text" name="recorder" required lay-verify="required" placeholder="请输入记录人"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item" style="margin-left: -110px">
                    <div class="layui-input-block">
                        <button  class="layui-btn" lay-submit lay-filter="formDemo1">预定</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="layui-tab-item">
            <form class="layui-form layui-form-pane" action="">
                <div class="layui-form-item">
                    <label id="orderTime" class="layui-form-label">预定时间</label>
                    <div class="layui-input-block">
                        <input type="text" name="meetTime" required lay-verify="required" placeholder="请输入预定时间"
                               autocomplete="off" class="layui-input" id="test2">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">预定人</label>
                    <div class="layui-input-block">
                        <input type="text" name="reservations" required lay-verify="required" placeholder="请输入预定人"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">会议室名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" required lay-verify="required" placeholder="请输入会议室名称"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">会议室类型</label>
                    <div class="layui-input-block">
                        <select name="type" lay-verify="required">
                            <option value=""></option>
                            <option value="会议">会议</option>
                            <option value="培训">培训</option>
                            <option value="面试">面试</option>
                            <option value="会客">会客</option>
                            <option value="其他">其他</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item" style="margin-left: -110px">
                    <div class="layui-input-block">
                        <button id="submitBtn1" class="layui-btn" lay-submit lay-filter="formDemo2">查询</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
                查询结果：<br>
                <table id="table2">
                </table>
            </form>
        </div>
        <div class="layui-tab-item">
            <div class="fixed-table-toolbar">
                <div class="columns pull-left">
                    <button class="btn  btn-primary"  type="button" id="btn9">
                        <i aria-hidden="true" class="fa fa-plus hidden"></i>添加
                    </button>
                </div>
                <div class="columns pull-right">
                    <button class="btn btn-success" onclick="reLoad()">查询</button>
                </div>

                <div class="columns pull-right col-md-2 nopadding">
                    <input class="form-control" id="searchName" placeholder="姓名" type="text">
                </div>
            </div>
            <table id="table"></table>
        </div>
    </div>
</div>

<script>
    $(function () {
        layui.use('form', function () {
            var form = layui.form;
            //监听提交
            form.on('submit(formDemo)', function (data) {
                data.field.meetTime = new Date(data.field.meetTime).getTime();
                $.ajax({
                    type: "POST", //四种提交类型：get、post、put、delete
                    url: "${ctx}/getMeetByMeetTime",
                    contentType: 'application/json',
                    async:false,
                    data: JSON.stringify(data.field),
                    success: function (result) {
                        if (result.code == 0){
                            $('#table1').bootstrapTable({
                                striped: true, // 设置为true会有隔行变色效果
                                pagination: true, // 设置为 true 会在表格底部显示分页条
                                pageNumber: 1, // 如果设置了分页，首页页码
                                pageSize: 10,//如果设置了分页，页面数据条数
                                sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
                                columns: [
                                    {
                                        field: 'id',
                                        title: '序号'
                                    }, {
                                        field: 'name',
                                        title: '会议室名称'
                                    }, {
                                        field: 'describe',
                                        title: '会议室描述'
                                    }],
                                data:result.data
                            });
                            console.log(result.data);
                        }
                    }
                });
                return false;
            });

            form.on('submit(formDemo1)', function (data){
                var oTime = new Date($("#oTime").val()).getTime();
                data.field.meetTime = oTime;
                $.ajax({
                    type:'PUT',
                    url: "${ctx}/addMeetingRoom",
                    contentType: 'application/json',
                    data:JSON.stringify(data.field),
                    success: function (result) {
                        if (result.code == 0) {
                            parent.layer.msg("预定成功");
                            parent.reLoad();
                            //先得到当前iframe层的索引
                            var index = parent.layer.getFrameIndex(window.name);
                            //再执行关闭
                            parent.layer.close(index);
                        } else {
                            layer.msg(result.info);
                        }
                    }
                });
                return false;
            });
            form.on('submit(formDemo2)', function (data) {
                data.field.meetTime = new Date(data.field.meetTime).getTime();
                console.log(data.field);
                $.ajax({
                    type: "POST", //四种提交类型：get、post、put、delete
                    url: "${ctx}/getMeetingRoom1",
                    contentType: 'application/json',
                    async:false,
                    data: JSON.stringify(data.field),
                    success: function (result) {
                        if (result.code == 0){
                            $('#table2').bootstrapTable({
                                striped: true, // 设置为true会有隔行变色效果
                                pagination: true, // 设置为 true 会在表格底部显示分页条
                                pageNumber: 1, // 如果设置了分页，首页页码
                                pageSize: 10,//如果设置了分页，页面数据条数
                                sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
                                columns: [
                                    {
                                        field: 'id',
                                        title: '序号'
                                    }, {
                                        field: 'name',
                                        title: '会议室名称'
                                    }, {
                                        field: 'meetTime',
                                        title: '会议时间'
                                    }, {
                                        field: 'type',
                                        title: '会议类型'
                                    }, {
                                        field: 'recorder',
                                        title: '记录人'
                                    }, {
                                        field: 'reservations',
                                        title: '预定人'
                                    },{
                                        field: 'time',
                                        title: '预定时间'
                                    }],
                                data:[result.data]
                            });
                            console.log(result.data);
                        }
                    }
                });

                return false;
            });
            form.render();
        });

        $('#table').bootstrapTable({
            method: 'GET',
            url: '${ctx}/getAllMeet', // 获取表格数据
            dataType: 'json', // 服务器返回的数据类型
            striped: true, // 设置为true会有隔行变色效果
            pagination: true, // 设置为 true 会在表格底部显示分页条
            pageNumber: 1, // 如果设置了分页，首页页码
            pageSize: 10,//如果设置了分页，页面数据条数
            sidePagination: 'client',//设置在哪里进行分页，可选值为 'client' 或者 'server'。
            columns: [
                {
                    field: 'id',
                    title: '序号'
                }, {
                    field: 'name',
                    title: '会议室名称'
                }, {
                    field: 'describe',
                    title: '会议室描述'
                }, {
                    field: 'options',
                    title: '操作',
                    formatter: function (value, row, index) {
                        var editButton = '<a class="btn btn-primary btn-sm " href="#" title="编辑" onclick="edit(\'' + row.id + '\')">修改</a>';
                        var deleteButton = '<a class="btn btn-warning btn-sm " href="#" title="删除" onclick="remove(\'' + row.id + '\')">删除</a>';
                        return editButton + deleteButton;
                    }
                }],
        });




        $('#btn9').on('click', function () {
            layer.open({
                type: 2,
                title: '添加',
                maxin: true,
                area: ['800px', '520px'],
                content: "${ctx}/layui/administration/addMeet.jsp",
                anim: 1
            })
        });
    });

    function reLoad() {
        $('#table').bootstrapTable('refresh');
    }

    function edit(id) {
        layer.open({
            type: 2,
            title: '修改',
            maxin: true,
            area: ['800px', '520px'],
            content: "${ctx}/getMeetById/" + id,
            anim: 1
        })
    }
    function remove(id) {
        layer.confirm('确定要删除选中的记录?', {
            btn: ['确定', '取消']
        }, function () {
            $.ajax({
                type: "DELETE",
                url: "${ctx}/delMeet/" + id,
                success: function (data) {
                    if (data.code == 0) {
                        parent.layer.msg("删除成功");
                        parent.reLoad();
                        //先得到当前iframe层的索引
                        var index = parent.layer.getFrameIndex(window.name);
                        //再执行关闭
                        parent.layer.close(index);
                    } else {
                        layer.msg(result.info);
                    }
                }
            })
        })
    }
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#oTime' //指定元素
        });
        laydate.render({
            elem: '#test2' //指定元素
        });
    });
</script>
</body>
</html>
