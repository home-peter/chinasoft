<%--
  Created by IntelliJ IDEA.
  User: zh
  Date: 2018/6/29
  Time: 9:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="layui-tab-item layui-show">
    <form id="visitingCard" class="layui-form layui-form-pane" action="" >
        <div class="layui-form-item">
            <label class="layui-form-label">申请人</label>
            <div class="layui-input-block">
                <input type="text" name="name" required  lay-verify="required" placeholder="请输入申请人" autocomplete="off" class="layui-input" style="width: 400px; float: left">
            </div>
            <label class="layui-form-label" style="margin-top: -36px;margin-left: 20px">申请时间</label>
            <div class="layui-input-block">
                <input type="text" name="time" required  lay-verify="required" placeholder="11/14/2009" autocomplete="off" class="layui-input" style="width: 400px; float: left; margin-top: -36px" id="test1">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">部门</label>
            <div class="layui-input-block">
                <input type="text" name="department" required  lay-verify="required" placeholder="请输入部门" autocomplete="off" class="layui-input" style="width: 400px; float: left">
            </div>
            <label class="layui-form-label" style="margin-top: -36px;margin-left: 20px">职位</label>
            <div class="layui-input-block">
                <input type="text" name="post" required  lay-verify="required" placeholder="请输入职位" autocomplete="off" class="layui-input" style="width: 400px; float: left; margin-top: -36px">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-form-item">
                <label class="layui-form-label">手机号</label>
                <div class="layui-input-block">
                    <input type="text" name="telephone" required  lay-verify="required" placeholder="请输入手机号" autocomplete="off" class="layui-input" style="width: 400px; float: left">
                </div>
                <label class="layui-form-label" style="margin-top: -36px;margin-left: 20px">座机号</label>
                <div class="layui-input-block">
                    <input type="text" name="seatNumber" required  lay-verify="required" placeholder="请输入座机号" autocomplete="off" class="layui-input" style="width: 400px; float: left; margin-top: -36px">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">数量</label>
            <div class="layui-input-block">
                <input type="text" name="number" required  lay-verify="required" placeholder="请输入数量" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <textarea name="remarks" placeholder="请输入备注" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">审批人</label>
            <div class="layui-input-block">
                <select name="approval" lay-verify="required">
                    <option value=""></option>
                    <option value="王五">王五</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item" style="margin-left: -110px">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>
<script>
    //注意：选项卡 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;

        //…
    });
    $(function () {
        //Demo
        layui.use('form', function () {
            var form = layui.form;

            //监听提交
            form.on('submit(formDemo)', function (data) {
//                layer.msg(JSON.stringify(data.field));
                data.field.time = new Date(data.field.time).getTime();
                $.ajax({
                    type:"POST", //四种提交类型：get、post、put、delete
                    url:"${ctx}/addVisitingCard",
                    contentType: 'application/json',
                    data:JSON.stringify(data.field),
                    success:function(result){
                        alert("申请成功");
                    }
                });
                return false;
            });
            form.render();
        });
    });
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#test1' //指定元素
        });
    });
</script>
</body>
</html>
