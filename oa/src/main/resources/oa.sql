/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.21-log : Database - oa
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`oa` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `oa`;

/*Table structure for table `application_car` */

DROP TABLE IF EXISTS `application_car`;

CREATE TABLE `application_car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carId` int(11) NOT NULL,
  `carMan` varchar(50) NOT NULL,
  `entourage` varchar(50) NOT NULL,
  `driver` varchar(50) NOT NULL,
  `beginTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `overTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `destination` varchar(50) NOT NULL,
  `distance` int(11) NOT NULL,
  `account` varchar(255) NOT NULL,
  `department` varchar(50) NOT NULL,
  `approval` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `application_car` */

insert  into `application_car`(`id`,`carId`,`carMan`,`entourage`,`driver`,`beginTime`,`overTime`,`destination`,`distance`,`account`,`department`,`approval`) values (1,1,'1','1','1','2018-06-26 15:33:27','2018-06-26 15:33:27','1',1,'1','1','1'),(2,111,'111','111','111','2017-01-01 08:00:00','2017-01-01 08:00:00','11',11,'11','11','王五'),(3,2,'2','2','2','2018-07-14 08:00:00','2018-07-18 08:00:00','2',2,'2','2','王五');

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `department` varchar(50) NOT NULL,
  `bookId` int(11) NOT NULL,
  `bookName` varchar(50) NOT NULL,
  `approval` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `book` */

insert  into `book`(`id`,`name`,`time`,`department`,`bookId`,`bookName`,`approval`) values (1,'1','2018-06-26 15:54:43','1',1,'为了你我愿意热爱整个世界','1'),(2,'2','2018-01-01 08:00:00','22',22,'222','王五'),(3,'2','2019-07-19 08:00:00','2',2,'2','王五');

/*Table structure for table `car` */

DROP TABLE IF EXISTS `car`;

CREATE TABLE `car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carId` int(11) NOT NULL,
  `carNumber` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `car` */

insert  into `car`(`id`,`carId`,`carNumber`,`type`,`remarks`) values (7,23121,'京A1221','金杯小货车','小货车'),(8,123123,'京p1233','现代轿车','！！！'),(9,23234,'京p34223','五菱荣光','111');

/*Table structure for table `chapter_use` */

DROP TABLE IF EXISTS `chapter_use`;

CREATE TABLE `chapter_use` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `department` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `account` varchar(255) NOT NULL,
  `approval` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `chapter_use` */

insert  into `chapter_use`(`id`,`name`,`time`,`department`,`type`,`account`,`approval`) values (1,'1','2018-06-26 15:43:58','1','1','1','1'),(2,'1','2018-01-01 08:00:00','11','公司用章','111','王五'),(3,'2','2018-07-13 08:00:00','2','财务用章','2','王五');

/*Table structure for table `incomecertificate` */

DROP TABLE IF EXISTS `incomecertificate`;

CREATE TABLE `incomecertificate` (
  `ID` int(10) NOT NULL AUTO_INCREMENT COMMENT '收入证明流程序号',
  `NAME` varchar(20) NOT NULL COMMENT '申请人名字',
  `TIME` datetime NOT NULL COMMENT '申请时间',
  `DEPARTMENT` varchar(20) NOT NULL COMMENT '部门',
  `POSITION` varchar(20) NOT NULL COMMENT '职位',
  `REASON` varchar(50) NOT NULL COMMENT '申请事由',
  `AONAME` varchar(20) NOT NULL COMMENT '审批人姓名',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `incomecertificate` */

insert  into `incomecertificate`(`ID`,`NAME`,`TIME`,`DEPARTMENT`,`POSITION`,`REASON`,`AONAME`) values (2,'邹豪','2018-07-05 08:00:00','质检部','员工','还方挺强5000元','zmy');

/*Table structure for table `leaveslip` */

DROP TABLE IF EXISTS `leaveslip`;

CREATE TABLE `leaveslip` (
  `ID` int(10) NOT NULL AUTO_INCREMENT COMMENT '请假序号',
  `NAME` varchar(20) NOT NULL COMMENT '申请人姓名',
  `DEPARTMENT` varchar(20) NOT NULL COMMENT '部门',
  `STARTTIME` datetime NOT NULL COMMENT '假期开始时间',
  `FINISHTIME` datetime NOT NULL COMMENT '请假结束时间',
  `DAYS` int(10) NOT NULL COMMENT '请假天数',
  `TYPE` varchar(10) NOT NULL COMMENT '请假类型',
  `REASON` varchar(50) NOT NULL DEFAULT '' COMMENT '请假理由',
  `AONAME` varchar(20) NOT NULL DEFAULT '' COMMENT '审批人',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `leaveslip` */

insert  into `leaveslip`(`ID`,`NAME`,`DEPARTMENT`,`STARTTIME`,`FINISHTIME`,`DAYS`,`TYPE`,`REASON`,`AONAME`) values (1,'柳飞洋','研发部','2018-07-04 08:00:00','2018-07-19 08:00:00',15,'事假','产子','zmy'),(4,'方挺强','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','考试','zh'),(5,'周杰伦','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','唱歌','zh'),(6,'李荣浩','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','演唱会','ftq'),(7,'薛之谦','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','演综艺','ftq'),(8,'黄渤','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','演综艺','ftq'),(9,'黄磊','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','演综艺','ftq'),(10,'罗志祥','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','演综艺','ftq'),(11,'张艺兴','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','演综艺','ftq'),(12,'韦德','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00',1,'事假','球赛','zmy'),(13,'贝尔','研发部','2018-07-04 08:00:00','2018-07-11 08:00:00',7,'病假','乱吃东西吃坏肚子','zmy'),(15,'方挺强','研发部','2018-07-04 08:00:00','2018-07-12 08:00:00',432,'病假','病假','ftq');

/*Table structure for table `meet` */

DROP TABLE IF EXISTS `meet`;

CREATE TABLE `meet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `describe` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `meet` */

insert  into `meet`(`id`,`name`,`describe`) values (9,'C212','C座三楼多媒体会议室'),(10,'B321','B座三楼多媒体会议室'),(11,' C222','C座三楼多媒体会议室');

/*Table structure for table `meetingroom` */

DROP TABLE IF EXISTS `meetingroom`;

CREATE TABLE `meetingroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `reservations` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `recorder` varchar(20) NOT NULL,
  `meetTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `meetingroom` */

insert  into `meetingroom`(`id`,`name`,`reservations`,`type`,`recorder`,`meetTime`,`time`) values (1,'1','1','1','1','2018-07-02 00:00:00','2018-06-25 17:29:30'),(2,'1','1','1','1','2018-01-12 00:00:00','2018-06-25 17:30:03'),(3,'C212','zh','会议','zmy','2018-07-05 08:00:00','2018-07-04 13:52:53'),(4,'B321','邹豪','面试','zmy','2018-07-05 08:00:00','2018-07-04 14:28:16');

/*Table structure for table `notice` */

DROP TABLE IF EXISTS `notice`;

CREATE TABLE `notice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '通告编号',
  `title` varchar(255) DEFAULT NULL COMMENT '通告标题',
  `content` text COMMENT '通告内容',
  `mark` int(11) DEFAULT NULL COMMENT '标题标注，紧急，一般',
  `time_begin` timestamp NULL DEFAULT NULL COMMENT '通告显示开始时间，null为无时间限制',
  `time_end` timestamp NULL DEFAULT NULL COMMENT '通告显示结束时间，null为无时间限制',
  `status` int(11) DEFAULT NULL COMMENT '发布状态，0草稿，1已发布，2已过期',
  `time_release` timestamp NULL DEFAULT NULL COMMENT '通告发布时间',
  `uid` bigint(20) DEFAULT NULL COMMENT '发布人编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `notice` */

insert  into `notice`(`id`,`title`,`content`,`mark`,`time_begin`,`time_end`,`status`,`time_release`,`uid`) values (1,'规范办公xxx','全体办公室员工：\n\n       为加强办公时间管控、提高办公效率、规范办公做风，现对相关事项做如下说明：\n\n一、早晨就餐情况\n\n       公司早晨正常上班办公时间是8：00，近期发现8：30分左右在办公就餐，严重影响办公环境和他人的工作情绪，为此请全体办公室的员工在8：00之前就餐完毕；今后如有发现做通报批评处理。\n\n二、下午打扫卫生时间\n\n       公司办公室正常的下班时间是17：00，为提高相关的事物处理效率，打扫卫生规定时间为16：50以后。\n\n      以上通知，请全体员工认真执行。\n                                                                                                                  人力资源部\n\n                                                                                                                 ',1,'2018-07-04 08:00:00','2018-07-04 08:00:00',1,'2018-07-04 08:00:00',7),(2,'晚会通知','今晚8点，庆功晚会',0,'2018-06-24 15:00:43','2018-06-25 15:00:43',1,'2018-06-26 15:00:43',7),(3,'重要通知','晚上领导来查，注意。',0,'2018-07-10 08:00:00','2018-07-10 08:00:00',1,'2018-07-12 08:00:00',7),(5,'明天放假了','明天放假',0,'2018-07-06 08:00:00','2018-07-07 08:00:00',1,'2018-07-06 08:00:00',7);

/*Table structure for table `sys_department` */

DROP TABLE IF EXISTS `sys_department`;

CREATE TABLE `sys_department` (
  `department_id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '部门编号',
  `name` varchar(255) DEFAULT NULL COMMENT '部门名',
  `status` int(11) DEFAULT NULL COMMENT '部门状态',
  `gmt_created` timestamp NULL DEFAULT NULL COMMENT '数据产生时间',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '数据修改时间',
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `sys_department` */

insert  into `sys_department`(`department_id`,`name`,`status`,`gmt_created`,`gmt_modified`) values (2,'研发部',0,'2018-07-02 15:33:28',NULL),(3,'质检部',0,'2018-07-02 15:33:40',NULL),(4,'产品设计部门',0,'2018-07-02 15:33:59',NULL);

/*Table structure for table `sys_job` */

DROP TABLE IF EXISTS `sys_job`;

CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '职位编号',
  `department_id` bigint(20) DEFAULT NULL COMMENT '所属部门编号',
  `name` varchar(255) DEFAULT NULL COMMENT '职位名',
  `status` int(11) DEFAULT NULL COMMENT '职位状态',
  `gmt_created` timestamp NULL DEFAULT NULL COMMENT '数据产生时间',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '数据修改时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `sys_job` */

insert  into `sys_job`(`job_id`,`department_id`,`name`,`status`,`gmt_created`,`gmt_modified`) values (2,2,'研发员',0,'2018-07-02 15:34:51',NULL),(3,3,'质检员',0,'2018-07-02 15:35:04',NULL),(4,4,'产品设计员',0,'2018-07-02 15:35:27',NULL);

/*Table structure for table `sys_log` */

DROP TABLE IF EXISTS `sys_log`;

CREATE TABLE `sys_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户编号',
  `note` varchar(1024) DEFAULT NULL COMMENT '用户操作',
  `gmt_created` timestamp NULL DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

/*Data for the table `sys_log` */

insert  into `sys_log`(`log_id`,`user_id`,`note`,`gmt_created`) values (25,8,'登录系统','2018-07-03 15:48:40'),(26,7,'登录系统','2018-07-03 16:01:06'),(27,7,'登录系统','2018-07-03 16:05:15'),(28,7,'登录系统','2018-07-04 13:24:50'),(29,7,'登录系统','2018-07-04 13:25:12'),(30,7,'添加请假申请','2018-07-04 13:26:45'),(31,7,'添加请假申请','2018-07-04 13:27:13'),(32,7,'添加出差申请','2018-07-04 13:28:23'),(33,7,'添加出差申请','2018-07-04 13:28:47'),(34,7,'删除出差申请','2018-07-04 13:28:52'),(35,7,'删除请假申请','2018-07-04 13:28:57'),(36,7,'添加收入流程证明申请','2018-07-04 13:29:57'),(37,7,'给角色赋权','2018-07-04 13:29:58'),(38,7,'删除收入流程证明申请','2018-07-04 13:30:03'),(39,7,'登录系统','2018-07-04 13:30:29'),(40,7,'给角色赋权','2018-07-04 13:31:41'),(41,7,'给角色赋权','2018-07-04 13:31:53'),(42,7,'给角色赋权','2018-07-04 13:32:02'),(43,7,'给角色赋权','2018-07-04 13:32:13'),(44,7,'添加请假申请','2018-07-04 13:33:04'),(45,7,'删除请假申请','2018-07-04 13:33:10'),(46,7,'给角色赋权','2018-07-04 13:33:30'),(47,7,'给角色赋权','2018-07-04 13:33:42'),(48,9,'登录系统','2018-07-04 13:34:01'),(49,7,'登录系统','2018-07-04 13:34:18'),(50,7,'添加请假申请','2018-07-04 13:34:27'),(51,7,'登录系统','2018-07-04 13:34:30'),(52,7,'添加请假申请','2018-07-04 13:34:55'),(53,8,'登录系统','2018-07-04 13:35:01'),(54,7,'添加请假申请','2018-07-04 13:35:08'),(55,7,'添加请假申请','2018-07-04 13:35:31'),(56,7,'添加请假申请','2018-07-04 13:35:39'),(57,7,'添加请假申请','2018-07-04 13:35:44'),(58,7,'添加请假申请','2018-07-04 13:35:49'),(59,7,'添加请假申请','2018-07-04 13:35:58'),(60,7,'添加请假申请','2018-07-04 13:36:28'),(61,7,'添加请假申请','2018-07-04 13:37:25'),(62,8,'登录系统','2018-07-04 13:41:54'),(63,7,'新增通告','2018-07-04 13:51:51'),(64,8,'删除一条用车记录','2018-07-04 13:53:33'),(65,8,'删除一条用车记录','2018-07-04 13:53:34'),(66,8,'删除一条用车记录','2018-07-04 13:53:35'),(67,7,'添加请假申请','2018-07-04 13:53:47'),(68,7,'删除请假申请','2018-07-04 13:53:55'),(69,7,'新增通告','2018-07-04 13:54:04'),(70,7,'添加出差申请','2018-07-04 13:54:22'),(71,7,'删除出差申请','2018-07-04 13:54:27'),(72,7,'添加收入流程证明申请','2018-07-04 13:54:46'),(73,7,'删除收入流程证明申请','2018-07-04 13:54:52'),(74,7,'更新通告','2018-07-04 13:55:00'),(75,8,'执行用车管理','2018-07-04 13:55:24'),(76,8,'执行用车管理','2018-07-04 13:55:47'),(77,8,'执行用车管理','2018-07-04 13:56:27'),(78,7,'登录系统','2018-07-04 13:59:00'),(79,8,'登录系统','2018-07-04 13:59:39'),(80,7,'登录系统','2018-07-04 14:00:59'),(81,7,'登录系统','2018-07-04 14:02:27'),(82,8,'登录系统','2018-07-04 14:05:21'),(83,7,'登录系统','2018-07-04 14:12:48'),(84,7,'登录系统','2018-07-04 14:23:20'),(85,7,'添加角色','2018-07-04 14:24:23'),(86,7,'给角色赋权','2018-07-04 14:24:36'),(87,7,'添加用户','2018-07-04 14:25:25'),(88,10,'登录系统','2018-07-04 14:25:44'),(89,7,'登录系统','2018-07-04 14:26:24'),(90,7,'添加请假申请','2018-07-04 14:30:55'),(91,7,'登录系统','2018-07-04 15:35:21'),(92,8,'登录系统','2018-07-04 17:01:58'),(93,7,'登录系统','2018-07-04 17:02:32'),(94,8,'执行借书','2018-07-04 17:02:49'),(95,7,'登录系统','2018-07-04 17:45:27'),(96,7,'新增通告','2018-07-04 17:46:25'),(97,7,'更新通告','2018-07-04 17:47:03'),(98,7,'添加角色','2018-07-04 17:49:02'),(99,7,'给角色赋权','2018-07-04 17:49:11'),(100,7,'添加用户','2018-07-04 17:49:32'),(101,11,'登录系统','2018-07-04 17:49:55');

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '权限编号',
  `name` varchar(255) DEFAULT NULL COMMENT '权限名',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父权限编号',
  `url` varchar(255) DEFAULT NULL COMMENT '权限路径',
  `permission` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `gmt_created` timestamp NULL DEFAULT NULL COMMENT '插入时间',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`menu_id`,`name`,`parent_id`,`url`,`permission`,`order_num`,`status`,`gmt_created`,`gmt_modified`) values (3,'通告管理',0,NULL,'notice',NULL,0,'2018-07-02 15:36:48',NULL),(4,'通告列表',3,NULL,'notice:list',NULL,0,'2018-07-02 15:37:02',NULL),(6,'行政管理',0,NULL,'Adm',NULL,0,'2018-07-02 15:37:29',NULL),(7,'会议室管理',6,NULL,'Adm:meet',NULL,0,'2018-07-02 15:37:54',NULL),(8,'用车管理',6,NULL,'Adm:car',NULL,0,'2018-07-02 15:38:11',NULL),(9,'用章管理',6,NULL,'Adm:chapter',NULL,0,'2018-07-02 15:38:29',NULL),(10,'图书借阅管理',6,NULL,'Adm:book',NULL,0,'2018-07-02 15:38:46',NULL),(11,'名片印制流程',6,NULL,'Adm:card',NULL,0,'2018-07-02 15:39:13',NULL),(12,'人事管理',0,NULL,'personnel',NULL,0,'2018-07-02 15:39:36',NULL),(13,'请假管理',12,NULL,'personnel:leave',NULL,0,'2018-07-02 15:39:52',NULL),(14,'出差管理',12,NULL,'personnel:business',NULL,0,'2018-07-02 15:40:02',NULL),(15,'收入证明管理',12,NULL,'personnel:income',NULL,0,'2018-07-02 15:40:21',NULL),(16,'系统设置',0,NULL,'system',NULL,0,'2018-07-02 15:40:40',NULL),(17,'角色管理',16,NULL,'system:role',NULL,0,'2018-07-02 15:40:56',NULL),(18,'操作日志',16,NULL,'system:log',NULL,0,'2018-07-02 15:41:09',NULL),(19,'用户管理',16,NULL,'system:user',NULL,0,'2018-07-03 09:23:17',NULL);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `gmt_created` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

insert  into `sys_role`(`role_id`,`name`,`status`,`gmt_created`,`gmt_modified`) values (7,'系统管理员',0,'2018-07-02 15:36:02',NULL),(8,'普通用户',0,'2018-07-03 09:25:59',NULL),(9,'超级管理员',0,'2018-07-04 14:24:23',NULL),(10,'HR',0,'2018-07-04 17:49:02',NULL);

/*Table structure for table `sys_role_menu` */

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `role_menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色-权限编号',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色编号',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '权限编号',
  `gmt_created` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`role_menu_id`,`role_id`,`menu_id`,`gmt_created`) values (27,8,4,'2018-07-03 09:26:48'),(96,7,4,'2018-07-04 13:33:41'),(97,7,7,'2018-07-04 13:33:41'),(98,7,8,'2018-07-04 13:33:42'),(99,7,9,'2018-07-04 13:33:42'),(100,7,10,'2018-07-04 13:33:42'),(101,7,11,'2018-07-04 13:33:42'),(102,7,13,'2018-07-04 13:33:42'),(103,7,14,'2018-07-04 13:33:42'),(104,7,15,'2018-07-04 13:33:42'),(105,7,17,'2018-07-04 13:33:42'),(106,7,18,'2018-07-04 13:33:42'),(107,7,19,'2018-07-04 13:33:42'),(108,9,4,'2018-07-04 14:24:36'),(109,9,7,'2018-07-04 14:24:36'),(110,9,8,'2018-07-04 14:24:36'),(111,9,9,'2018-07-04 14:24:36'),(112,9,10,'2018-07-04 14:24:36'),(113,9,11,'2018-07-04 14:24:36'),(114,10,13,'2018-07-04 17:49:11'),(115,10,14,'2018-07-04 17:49:11'),(116,10,15,'2018-07-04 17:49:11');

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '用户密码',
  `real_name` varchar(255) DEFAULT NULL COMMENT '用户真实姓名',
  `gender` varchar(10) DEFAULT NULL COMMENT '用户性别',
  `age` int(11) DEFAULT NULL COMMENT '用户年龄',
  `phone` varchar(11) DEFAULT NULL COMMENT '用户联系方式',
  `job_id` bigint(20) DEFAULT NULL COMMENT '用户职业',
  `status` int(11) DEFAULT NULL COMMENT '用户状态',
  `gmt_created` timestamp NULL DEFAULT NULL COMMENT '数据产生时间',
  `gmt_modified` timestamp NULL DEFAULT NULL COMMENT '数据修改时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`user_id`,`username`,`password`,`real_name`,`gender`,`age`,`phone`,`job_id`,`status`,`gmt_created`,`gmt_modified`) values (7,'zmy','123456','张明远','男',22,'18100177470',2,0,'2018-07-02 15:44:58',NULL),(8,'zh','123456','邹豪','女',NULL,NULL,3,0,'2018-07-02 17:28:39',NULL),(9,'ftq','123456','方挺强','男',NULL,NULL,4,0,'2018-07-03 09:27:32',NULL),(10,'mmm','123456','张三','男',NULL,NULL,4,0,'2018-07-04 14:25:25',NULL),(11,'xxx','123456','xxx','男',NULL,NULL,2,0,'2018-07-04 17:49:32',NULL);

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户-角色编号',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户编号',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色编号',
  `gmt_created` timestamp NULL DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`user_role_id`,`user_id`,`role_id`,`gmt_created`) values (11,7,7,'2018-07-02 15:45:23'),(12,8,7,'2018-07-02 17:28:39'),(13,9,8,'2018-07-03 09:27:32'),(14,10,9,'2018-07-04 14:25:25'),(15,11,10,'2018-07-04 17:49:32');

/*Table structure for table `travelrequests` */

DROP TABLE IF EXISTS `travelrequests`;

CREATE TABLE `travelrequests` (
  `ID` int(10) NOT NULL AUTO_INCREMENT COMMENT '申请出差编号',
  `NAME` varchar(20) NOT NULL COMMENT '出差人名字',
  `DEPARTMENT` varchar(20) NOT NULL COMMENT '部门',
  `STARTTIME` datetime NOT NULL COMMENT '出差开始时间',
  `FINISHTIME` datetime NOT NULL COMMENT '出差结束时间',
  `PLACE` varchar(50) NOT NULL COMMENT '出差地点',
  `TOOL` varchar(10) NOT NULL COMMENT '出差工具',
  `REASON` varchar(50) NOT NULL COMMENT '出差理由',
  `TASK` varchar(50) NOT NULL COMMENT '出差任务',
  `MONEY` int(10) NOT NULL COMMENT '借款金额',
  `AONAME` varchar(20) NOT NULL COMMENT '审批人',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `travelrequests` */

insert  into `travelrequests`(`ID`,`NAME`,`DEPARTMENT`,`STARTTIME`,`FINISHTIME`,`PLACE`,`TOOL`,`REASON`,`TASK`,`MONEY`,`AONAME`) values (1,'方挺强','研发部','2018-07-04 08:00:00','2018-07-05 08:00:00','杭州','飞机','球赛','赢球',1000,'zmy');

/*Table structure for table `visiting_card` */

DROP TABLE IF EXISTS `visiting_card`;

CREATE TABLE `visiting_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `post` varchar(50) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `seatNumber` varchar(50) NOT NULL,
  `number` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `approval` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `visiting_card` */

insert  into `visiting_card`(`id`,`name`,`department`,`time`,`post`,`telephone`,`seatNumber`,`number`,`remarks`,`approval`) values (1,'1','1','2018-06-26 16:06:15','1','1','1',1,'1','1'),(2,'2','2','2018-01-01 08:00:00','2','2','2',2,'2','0'),(3,'2','2','2018-07-21 08:00:00','2','2','2',2,'2','王五');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
