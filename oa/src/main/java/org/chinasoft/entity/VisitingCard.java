package org.chinasoft.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by zh on 2018/6/25.
 */
public class VisitingCard {
    //名片印制序列
    private  int id;
    //申请人
    private  String name;
    //申请人部门
    private  String department;
    //申请时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private  Date time;
    //申请职位
    private  String post;
    //手机号
    private  String telephone;
    //座机号
    private  String seatNumber;
    //数量
    private  String number;
    //备注
    private  String remarks;
    //审批人
    private  String approval;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    @Override
    public String toString() {
        return "VisitingCard{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", time=" + time +
                ", post='" + post + '\'' +
                ", telephone='" + telephone + '\'' +
                ", seatNumber='" + seatNumber + '\'' +
                ", number='" + number + '\'' +
                ", remarks='" + remarks + '\'' +
                ", approval='" + approval + '\'' +
                '}';
    }
}
