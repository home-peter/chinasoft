package org.chinasoft.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by Fang on 2018/6/25.
 */
public class LeaveSlip {
    private int id;//请假序号
    private String name;//申请人姓名
    private String department;//部门
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date starttime;//请假开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date finishtime;//请假结束时间
    private int days;//请假天数
    private String type;//请假类型
    private String reason;//请假事由
    private String aoName;//审批人

    public LeaveSlip(){}

    public LeaveSlip(int id, String name, String department, Date starttime, Date finishtime, int days, String type, String reason, String aoName) {
        this.id = id;
        this.name = name;
        this.department = department;
        this.starttime = starttime;
        this.finishtime = finishtime;
        this.days = days;
        this.type = type;
        this.reason = reason;
        this.aoName = aoName;
    }

    public LeaveSlip(String name, String department, Date starttime, Date finishtime, int days, String type, String reason, String aoName) {
        this.name = name;
        this.department = department;
        this.starttime = starttime;
        this.finishtime = finishtime;
        this.days = days;
        this.type = type;
        this.reason = reason;
        this.aoName = aoName;
    }

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getDepartment() {return department;}

    public void setDepartment(String department) {this.department = department;}

    public Date getStarttime() {return starttime;}

    public void setStarttime(Date starttime) {this.starttime = starttime;}

    public Date getFinishtime() {return finishtime;}

    public void setFinishtime(Date finishtime) {this.finishtime = finishtime;}

    public int getDays() {return days;}

    public void setDays(int days) {this.days = days;}

    public String getType() {return type;}

    public void setType(String type) {this.type = type;}

    public String getReason() {return reason;}

    public void setReason(String reason) {this.reason = reason;}

    public String getAoName() {return aoName;}

    public void setAoName(String aoName) {this.aoName = aoName;}

    @Override
    public String toString() {
        return "LeaveSlip{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", starttime=" + starttime +
                ", finishtime=" + finishtime +
                ", days=" + days +
                ", type='" + type + '\'' +
                ", reason='" + reason + '\'' +
                ", aoName='" + aoName + '\'' +
                '}';
    }
}

