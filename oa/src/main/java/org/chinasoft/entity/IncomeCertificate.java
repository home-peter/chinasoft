package org.chinasoft.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by Fang on 2018/6/25.
 */
public class IncomeCertificate {
    private int id;//收入证明流程序号
    private String name;//申请人姓名
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;//申请时间
    private String department;//部门
    private String position;//职位
    private String reason;//申请事由
    private String aoName;//审批人姓名

    public IncomeCertificate(){}

    public IncomeCertificate(int id, String name, Date time, String department, String position, String reason, String aoName) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.department = department;
        this.position = position;
        this.reason = reason;
        this.aoName = aoName;
    }

    public IncomeCertificate(String name, Date time, String department, String position, String reason, String aoName) {
        this.name = name;
        this.time = time;
        this.department = department;
        this.position = position;
        this.reason = reason;
        this.aoName = aoName;
    }

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public Date getTime() {return time;}

    public void setTime(Date time) {this.time = time;}

    public String getDepartment() {return department;}

    public void setDepartment(String department) {this.department = department;}

    public String getPosition() {return position;}

    public void setPosition(String position) {this.position = position;}

    public String getReason() {return reason;}

    public void setReason(String reason) {this.reason = reason;}

    public String getAoName() {return aoName;}

    public void setAoName(String aoName) {
        this.aoName = aoName;
    }
}
