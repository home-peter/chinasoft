package org.chinasoft.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by zh on 2018/6/25.
 */
public class ChapterUse {
    //用章申请序列
    private int id;
    //姓名
    private String name;
    //申请时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;
    //部门
    private String department;
    //用章类型
    private String type;
    //请假事由
    private String account;
    //审批人
    private String approval;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    @Override
    public String toString() {
        return "ChapterUse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", time=" + time +
                ", department='" + department + '\'' +
                ", type='" + type + '\'' +
                ", account='" + account + '\'' +
                ", approval='" + approval + '\'' +
                '}';
    }
}
