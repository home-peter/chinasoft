package org.chinasoft.entity;

import java.util.List;

/**
 * Created by lfy on 2018/6/25.
 */
public class PageBean<T> {
    /**
     * 现在的页码
     */
    private int currentPage;
    /**
     * 一页几条记录
     */
    private int pageSize;
    /**
     * 总页数
     */
    private int totalPage;
    /**
     * 分页查询出的列表
     */
    private List<T> lists;

    public List<T> getLists() {
        return lists;
    }

    public void setLists(List<T> lists) {
        this.lists = lists;
    }


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
}
