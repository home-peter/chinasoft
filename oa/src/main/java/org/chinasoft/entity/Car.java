package org.chinasoft.entity;

/**
 * Created by zh on 2018/6/25.
 */
public class Car {
    //车辆序列
    private int id;
    //车辆编号
    private int carId;
    //车牌号
    private String carNumber;
    //车型
    private String type;
    //备注
    private String remarks;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String   toString() {
        return "Car{" +
                "id=" + id +
                ", carId=" + carId +
                ", carNumber='" + carNumber + '\'' +
                ", type='" + type + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
