package org.chinasoft.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by zh on 2018/6/25.
 */
public class ApplicationCar {
    //用车申请编号
    private int id;
    //车辆编号
    private int carId;
    //用车人
    private String carMan;
    //随行人员
    private String entourage;
    //司机
    private String driver;
    //目的地
    private String destination;
    //申请里程（公里）
    private int distance;
    //用车时有
    private String account;
    //用车部门
    private String department;
    //审批人
    private String approval;
    //开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date beginTime;
    //结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date overTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarMan() {
        return carMan;
    }

    public void setCarMan(String carMan) {
        this.carMan = carMan;
    }

    public String getEntourage() {
        return entourage;
    }

    public void setEntourage(String entourage) {
        this.entourage = entourage;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getOverTime() {
        return overTime;
    }

    public void setOverTime(Date overTime) {
        this.overTime = overTime;
    }

    @Override
    public String toString() {
        return "ApplicationCar{" +
                "id=" + id +
                ", carId=" + carId +
                ", carMan='" + carMan + '\'' +
                ", entourage='" + entourage + '\'' +
                ", driver='" + driver + '\'' +
                ", destination='" + destination + '\'' +
                ", distance=" + distance +
                ", account='" + account + '\'' +
                ", department='" + department + '\'' +
                ", approval='" + approval + '\'' +
                ", beginTime=" + beginTime +
                ", overTime=" + overTime +
                '}';
    }
}
