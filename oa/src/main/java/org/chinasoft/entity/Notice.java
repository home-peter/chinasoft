package org.chinasoft.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by lfy on 2018/6/25.
 */
public class Notice {
    /**
     * 通告编号
     */
    private long id;
    /**
     * 通告标题
     */
    private String title;
    /**
     * 通告内容
     */
    private String content;
    /**
     *标题标注，0一般，1紧急
     */
    private int mark;
    /**
     *通告显示开始时间，null为无时间限制
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date timeBegin;
    /**
     *通告显示结束时间，null为无时间限制
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date timeEnd;
    /**
     *发布状态，0草稿，1已发布，2已过期
     */
    private int status;
    /**
     *通告发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date timeRelease;
    /**
     *发布人编号
     */
    private long uid;
    /**
     * 发布人
     */
    private User user;

    public Notice() {
    }



    public Notice(String title, String content, int mark, Date timeBegin, Date timeEnd, int status, Date timeRelease, long uid) {
        this.title = title;
        this.content = content;
        this.mark = mark;
        this.timeBegin = timeBegin;
        this.timeEnd = timeEnd;
        this.status = status;
        this.timeRelease = timeRelease;
        this.uid = uid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public Date getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(Date timeBegin) {
        this.timeBegin = timeBegin;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getTimeRelease() {
        return timeRelease;
    }

    public void setTimeRelease(Date timeRelease) {
        this.timeRelease = timeRelease;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    @Override
    public String toString() {
        return "Notice{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", mark=" + mark +
                ", timeBegin=" + timeBegin +
                ", timeEnd=" + timeEnd +
                ", status=" + status +
                ", timeRelease=" + timeRelease +
                ", uid=" + uid +
                '}';
    }
}
