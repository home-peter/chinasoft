package org.chinasoft.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by zh on 2018/6/25.
 */
public class MeetingRoom {
    //预约ID
    private int id;
    //会议室名称
    private String name;
    //预约人
    private String reservations;
    //会议室类型
    private String type;
    //记录人
    private String recorder;
    //会议预定时间
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date meetTime;
    //预定时间
    private String time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReservations() {
        return reservations;
    }

    public void setReservations(String reservations) {
        this.reservations = reservations;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRecorder() {
        return recorder;
    }

    public void setRecorder(String recorder) {
        this.recorder = recorder;
    }

    public Date getMeetTime() {
        return meetTime;
    }

    public void setMeetTime(Date meetTime) {
        this.meetTime = meetTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "MeetingRoom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", reservations='" + reservations + '\'' +
                ", type='" + type + '\'' +
                ", recorder='" + recorder + '\'' +
                ", meetTime=" + meetTime +
                ", time='" + time + '\'' +
                '}';
    }
}
