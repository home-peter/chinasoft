package org.chinasoft.entity;

/**
 * Created by zh on 2018/6/25.
 */
public class Meet {
    //会议室序列
    private int id;
    //会议室名称
    private String name;
    //会议室描述
    private String describe;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @Override
    public String toString() {
        return "Meet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }

    public Meet(String name, String describe) {
        this.name = name;
        this.describe = describe;
    }

    public Meet() {
    }
}
