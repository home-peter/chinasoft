package org.chinasoft.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by Fang on 2018/6/25.
 */
public class TravelRequests {
    private int id;//申请出差编号
    private String name;//出差人姓名
    private String department;//部门
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date starttime;//出差开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date finishtime;//出差结束时间
    private String place;//出差地点
    private String tool;//出差工具
    private String reason;//出差理由
    private String task;//出差任务
    private int money;//借款金额
    private String aoName;//审批人

    public TravelRequests(){}

    public TravelRequests(int id, String name, String department, Date starttime, Date finishtime, String place, String tool, String reason, String task, int money, String aoName) {
        this.id = id;
        this.name = name;
        this.department = department;
        this.starttime = starttime;
        this.finishtime = finishtime;
        this.place = place;
        this.tool = tool;
        this.reason = reason;
        this.task = task;
        this.money = money;
        this.aoName = aoName;
    }

    public TravelRequests(String name, String department, Date starttime, Date finishtime, String place, String tool, String reason, String task, int money, String aoName) {
        this.name = name;
        this.department = department;
        this.starttime = starttime;
        this.finishtime = finishtime;
        this.place = place;
        this.tool = tool;
        this.reason = reason;
        this.task = task;
        this.money = money;
        this.aoName = aoName;
    }

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getDepartment() {return department;}

    public void setDepartment(String department) {this.department = department;}

    public Date getStarttime() {return starttime;}

    public void setStarttime(Date starttime) {this.starttime = starttime;}

    public Date getFinishtime() {return finishtime;}

    public void setFinishtime(Date finishtime) {this.finishtime = finishtime;}

    public String getPlace() {return place;}

    public void setPlace(String place) {this.place = place;}

    public String getTool() {return tool;}

    public void setTool(String tool) {this.tool = tool;}

    public String getReason() {return reason;}

    public void setReason(String reason) {this.reason = reason;}

    public String getTask() {return task;}

    public void setTask(String task) {this.task = task;}

    public int getMoney() {return money;}

    public void setMoney(int money) {this.money = money;}

    public String getAoName() {
        return aoName;
    }

    public void setAoName(String aoName) {
        this.aoName = aoName;
    }

    @Override
    public String toString() {
        return "TravelRequests{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", starttime=" + starttime +
                ", finishtime=" + finishtime +
                ", place='" + place + '\'' +
                ", tool='" + tool + '\'' +
                ", reason='" + reason + '\'' +
                ", task='" + task + '\'' +
                ", money=" + money +
                ", aoName='" + aoName + '\'' +
                '}';
    }
}
