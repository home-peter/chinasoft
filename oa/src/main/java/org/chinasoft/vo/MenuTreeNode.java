package org.chinasoft.vo;

import org.chinasoft.entity.Menu;

import java.util.List;

public class MenuTreeNode {
    private Menu menu;
    private List<Menu> childList;

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<Menu> getChildList() {
        return childList;
    }

    public void setChildList(List<Menu> childList) {
        this.childList = childList;
    }
}
