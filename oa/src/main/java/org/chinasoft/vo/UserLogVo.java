package org.chinasoft.vo;

import org.chinasoft.entity.Log;
import org.chinasoft.entity.User;

public class UserLogVo {
    private User user;
    private Log log;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }
}
