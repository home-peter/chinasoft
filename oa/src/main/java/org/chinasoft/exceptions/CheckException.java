package org.chinasoft.exceptions;

public class CheckException extends RuntimeException {
    public CheckException(String message) {
        super(message);
    }

    public CheckException(Throwable cause) {
        super(cause);
    }
}
