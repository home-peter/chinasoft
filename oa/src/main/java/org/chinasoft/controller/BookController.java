package org.chinasoft.controller;

import org.chinasoft.aspect.Log;
import org.chinasoft.entity.Book;
import org.chinasoft.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zh on 2018/6/27.
 */
@Controller
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "addBook", method = RequestMethod.POST)
    @ResponseBody
    @Log(note = "执行借书")
    public int addBook(@RequestBody Book book) {
        int i = bookService.addBook(book);
        return i;
    }
}
