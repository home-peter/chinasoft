package org.chinasoft.controller;

import org.chinasoft.entity.ChapterUse;
import org.chinasoft.service.ChapterUseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zh on 2018/6/27.
 */
@Controller
public class ChapterUseController {

    @Autowired
    private ChapterUseService chapterUseService;

    @RequestMapping(value = "/addChapterUse",method = RequestMethod.POST)
    @ResponseBody
    public int addChapterUse(@RequestBody  ChapterUse chapterUse){
        int i = chapterUseService.addChapterUse(chapterUse);
        return i;
    }
}
