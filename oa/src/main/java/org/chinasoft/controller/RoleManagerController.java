package org.chinasoft.controller;

import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.aspect.Log;
import org.chinasoft.entity.Menu;
import org.chinasoft.entity.Role;
import org.chinasoft.service.MenuService;
import org.chinasoft.service.RoleService;
import org.chinasoft.utils.ResultBean;
import org.chinasoft.vo.MenuTreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class RoleManagerController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;

    @GetMapping("/role/list")
    @ExceptionHandler
    @ResponseBody
    public ResultBean<List<Role>> getRoleList() {
        List<Role> roleList = new ArrayList<>(roleService.getAll())
                .stream()
                .sorted(Comparator.comparing(Role::getRoleId))
                .collect(Collectors.toList());

        return new ResultBean<>(roleList);
    }

    @PutMapping("/role/add")
    @ExceptionHandler
    @ResponseBody
    @Log(note = "添加角色")
    public ResultBean addRole(@RequestBody Role role) {
        return new ResultBean<>(roleService.save(role));
    }

    @GetMapping("/role/{roleId}")
    @ExceptionHandler
    @ResponseBody
    public ResultBean<Role> getRoleByRoleId(@PathVariable("roleId") Long roleId) {
        return new ResultBean<>(roleService.getByRoleId(roleId));
    }

    @GetMapping("/menu/list")
    @ExceptionHandler
    @ResponseBody
    public ResultBean<List<Menu>> getMenuList() {
        return new ResultBean<>(menuService.getAll());
    }

    @GetMapping("/roleRight/{roleId}")
    public String toAssignPage(@PathVariable("roleId") Long roleId, Model model) {
        Role role = roleService.getByRoleId(roleId);
        List<Menu> menuList = menuService.getAll();

        List<MenuTreeNode> menuTreeNodeList = menuService.buildTreeByAllMenuList(menuList);

        model.addAttribute("role", role);
        model.addAttribute("menuTreeNodeList", menuTreeNodeList);
        return "roleManager/assign";
    }

    @GetMapping("/role/edit/{roleId}")
    public String toEditPage(@PathVariable("roleId") Long roleId, Model model) {
        Role role = roleService.getByRoleId(roleId);
        model.addAttribute("role", role);
        return "roleManager/edit";
    }

    @PostMapping("/roleRight/assign/{roleId}")
    @ExceptionHandler
    @ResponseBody
    @Log(note = "给角色赋权")
    public ResultBean<Role> assignRoleRight(@PathVariable("roleId") Long roleId,
                                            @RequestParam(value = "menuId", required = false) Long[] menuIds) {
        Role currentRole = new Role();
        currentRole.setRoleId(roleId);
        List<Menu> menuList = new ArrayList<>();
        if (menuIds != null) {
            for (Long menuId : menuIds) {
                Menu menu = new Menu();
                menu.setMenuId(menuId);
                menuList.add(menu);
            }
        }
        Role role = roleService.assignAccess(currentRole, menuList);
        return new ResultBean<>(role);
    }

    @PostMapping("/role/update")
    @ExceptionHandler
    @ResponseBody
    @Log(note = "更新角色")
    public ResultBean updateRole(@RequestBody Role role) {
        roleService.update(role);
        return new ResultBean().success();
    }

    @DeleteMapping("/role/{roleId}/remove")
    @ExceptionHandler
    @ResponseBody
    @Log(note = "删除角色")
    public ResultBean deleteRole(@PathVariable("roleId") Long roleId) {
        roleService.removeByRoleId(roleId);
        return new ResultBean().success();
    }
}
