package org.chinasoft.controller;

import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.aspect.Log;
import org.chinasoft.entity.Department;
import org.chinasoft.entity.TravelRequests;
import org.chinasoft.service.TravelRequestsService;
import org.chinasoft.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Fang on 2018/6/26.
 */
@Controller
@RequestMapping("TravelRequests")
public class TravelRequestsController {
    @Autowired
    TravelRequestsService travelRequestsService;
    @RequestMapping("{id}/getById")
    public String getById(@PathVariable("id")int id, HttpServletRequest request){
        TravelRequests travelRequests=travelRequestsService.getById(id);
        request.setAttribute("travelRequests",travelRequests);
        return "";
    }
    @RequestMapping(value = "/add",method=RequestMethod.POST)
    @ResponseBody
    @ExceptionHandler
    @Log(note = "添加出差申请")
    public ResultBean add(@RequestBody TravelRequests travelRequests){
        travelRequestsService.add(travelRequests);
        return new ResultBean().success();
    }
    @RequestMapping("update")
    @Log(note = "修改出差申请")
    public String update(TravelRequests travelRequests) {
        travelRequestsService.update(travelRequests);
        return "";
    }
    @RequestMapping(value = "{id}/delete")
    @ResponseBody
    @Log(note = "删除出差申请")
    public ResultBean delete(@PathVariable("id")int id) {
        travelRequestsService.delete(id);
        return new ResultBean().success();
    }
    @RequestMapping("findAll")
    @ResponseBody
    @ExceptionHandler
    public ResultBean<List<TravelRequests>> getTravelRequestsList(){
        return new ResultBean<>(travelRequestsService.findAll());
    }
}
