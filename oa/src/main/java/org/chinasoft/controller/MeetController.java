package org.chinasoft.controller;

import org.chinasoft.entity.Meet;
import org.chinasoft.entity.MeetingRoom;
import org.chinasoft.service.MeetService;
import org.chinasoft.service.MeetingRoomService;
import org.chinasoft.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by zh on 2018/6/27.
 */
@Controller
public class MeetController {
    @Autowired
    private MeetService meetService;

    @Autowired
    private MeetingRoomService meetingRoomService;


    @RequestMapping(value = "/addMeet", method = RequestMethod.PUT)
    @ResponseBody
    @ExceptionHandler
    public ResultBean addMeet(@RequestBody Meet meet) {
        return new ResultBean<>(meetService.addMeet(meet));
    }

    @RequestMapping(value = "/delMeet/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @ExceptionHandler
    public ResultBean delMeet(@PathVariable int id) {
        meetService.delMeet(id);
        return new ResultBean().success();
    }

    @RequestMapping(value = "/updateMeet", method = RequestMethod.POST)
    @ResponseBody
    @ExceptionHandler
    public ResultBean updateMeet(@RequestBody Meet meet) {
        return new ResultBean<>(meetService.updateMeet(meet));
    }

    @RequestMapping(value = "/getAllMeet", method = RequestMethod.GET)
    @ResponseBody
    public List<Meet> getAllMeet() {
        return meetService.getAll();
    }

    @RequestMapping(value = "/getMeetByMeetTime")
    @ResponseBody
    public ResultBean<List<Meet>> getMeetByMeetTime(@RequestBody MeetingRoom meetingRoom) {
        List<Meet> list = meetService.getMeetByMeetTime(meetingRoom.getMeetTime());
        return new ResultBean<>(list);
    }

    @RequestMapping(value = "/addMeetingRoom",method = RequestMethod.PUT)
    @ResponseBody
    @ExceptionHandler
    public ResultBean addMeetingRoom(@RequestBody MeetingRoom meetingRoom) {
        return new ResultBean<>(meetingRoomService.addMeetingRoom(meetingRoom));
    }

    @RequestMapping("/getMeetById/{id}")
    public String getMeetById(@PathVariable int id, Model model) {
        Meet meet = meetService.getMeetById(id);
        model.addAttribute("meet", meet);
        return "administration/updateMeet";
    }

    @RequestMapping(value = "/getMeetingRoom1",method = RequestMethod.POST)
    @ResponseBody
    @ExceptionHandler
    public ResultBean<MeetingRoom> getMeetingRoom(@RequestBody MeetingRoom meetingRoom){
        MeetingRoom meetingRoom1 = meetingRoomService.getMeetingRoom(meetingRoom);
        System.out.println(meetingRoom1);
        return new ResultBean<>(meetingRoom1);
    }
}
