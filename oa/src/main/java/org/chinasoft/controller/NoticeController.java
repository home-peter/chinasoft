package org.chinasoft.controller;


import org.chinasoft.aspect.Log;
import org.chinasoft.entity.Notice;
import org.chinasoft.entity.PageBean;
import org.chinasoft.service.NoticeService;
import org.chinasoft.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by lfy on 2018/6/26.
 */
@Controller
public class NoticeController {
   @Autowired
    NoticeService noticeService;
   @RequestMapping(value = "{id}/noticeDetails",method = RequestMethod.GET)
     public String findNoticeById(@PathVariable("id") long id,HttpServletRequest request){
       Notice notice=noticeService.findNoticeById(id);
       request.setAttribute("notice",notice);
       return "notice/details";
    }
    @RequestMapping(value = "{id}/noticeUpdate",method = RequestMethod.GET)
      public String findNoticeAndUpdate(@PathVariable("id") long id,HttpServletRequest request){
        Notice notice=noticeService.findNoticeById(id);
        request.setAttribute("notice",notice);
        return "notice/update";
    }
   @RequestMapping(value = "notice",method = RequestMethod.GET)
   @ResponseBody
    public List<Notice> findAll(){
         return noticeService.findAll();
   }

   @RequestMapping(value = "noticeAndUser",method = RequestMethod.GET)
   @ResponseBody
   public List<Notice> findAllNoticeAndUser(){
        return noticeService.findAllNoticeAndUser();
   }

   @RequestMapping(value = "{id}/delete",method = RequestMethod.DELETE)
   @ResponseBody
   @Log(note = "删除通告")
    public ResultBean delNoticeById(@PathVariable("id") long id){
        noticeService.delNoticeById(id);
        return new ResultBean().success();

   }
   @RequestMapping(value = "notice",method = RequestMethod.POST)
   @ResponseBody
   @Log(note = "新增通告")
    public ResultBean<Notice> addNotice(@RequestBody Notice notice){
        if (noticeService.addNotice(notice)>0)
        return new ResultBean<Notice>().success();
        return new ResultBean<Notice>().failed();
   }
   @RequestMapping(value = "notice",method = RequestMethod.PUT)
   @ResponseBody
   @Log(note = "更新通告")
    public ResultBean<Notice> updateNotice(@RequestBody Notice notice){
        if (noticeService.updateNotice(notice)>0)
        return new ResultBean<Notice>().success();
        return new ResultBean<Notice>().failed();
   }
   @RequestMapping("{currentPage}/{pageSize}/{flag}/findByPage")
   @ResponseBody
    public String findByPage(@PathVariable("currentPage") int currentPage,@PathVariable("pageSize") int pageSize,@PathVariable("flag") int flag,HttpServletRequest request){
        PageBean<Notice> pageBean=noticeService.findByPage(currentPage,pageSize,flag);
        request.setAttribute("pageBean",pageBean);
        return "";

   }



}
