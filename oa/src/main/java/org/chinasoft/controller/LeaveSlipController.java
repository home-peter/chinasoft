package org.chinasoft.controller;

import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.aspect.Log;
import org.chinasoft.entity.LeaveSlip;
import org.chinasoft.entity.TravelRequests;
import org.chinasoft.service.LeaveSlipService;
import org.chinasoft.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Fang on 2018/6/26.
 */
@Controller
@RequestMapping("LeaveSlip")
public class LeaveSlipController {
    @Autowired
    LeaveSlipService leaveSlipService;

    @RequestMapping("{id}/getById")
    public String getById(@PathVariable("id") int id, HttpServletRequest request) {
        LeaveSlip leaveSlip = leaveSlipService.getById(id);
        request.setAttribute("leaveSlip", leaveSlip);
        return "";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    @Log(note = "添加请假申请")
    public ResultBean add(@RequestBody LeaveSlip leaveSlip) {
        leaveSlipService.add(leaveSlip);
        return new ResultBean().success();
    }

    @RequestMapping("{leaveSlip}/update")
    @Log(note = "修改请假申请")
    public String upadte(@PathVariable("leaveSlip") LeaveSlip leaveSlip, HttpServletRequest request) {
        leaveSlipService.update(leaveSlip);
        return "";
    }

    @RequestMapping(value = "{id}/delete")
    @ResponseBody
    @Log(note = "删除请假申请")
    public ResultBean delete(@PathVariable("id") int id) {
       leaveSlipService.delete(id);
        return new ResultBean().success();
    }

    @RequestMapping("findAll")
    @ResponseBody
    @ExceptionHandler
    public ResultBean<List<LeaveSlip>> getLeaveSlipList(){
        return new ResultBean<>(leaveSlipService.findAll());
    }
}
