package org.chinasoft.controller;

import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.entity.Department;
import org.chinasoft.entity.Job;
import org.chinasoft.service.DepartmentService;
import org.chinasoft.service.JobService;
import org.chinasoft.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private JobService jobService;

    @GetMapping("/department/{departmentId}")
    @ResponseBody
    @ExceptionHandler
    public ResultBean<Department> getDepartmentById(@PathVariable("departmentId") Long departmentId) {
        return new ResultBean<>(departmentService.getByDepartmentId(departmentId));
    }

    @GetMapping("/department/list")
    @ResponseBody
    @ExceptionHandler
    public ResultBean<List<Department>> getDepartmentList() {
        return new ResultBean<>(departmentService.getAll());
    }

    @GetMapping("/{departmentId}/jobList")
    @ResponseBody
    @ExceptionHandler
    public ResultBean<List<Job>> getJobListByDepartmentId(@PathVariable("departmentId") Long departmentId) {
        return new ResultBean<>(jobService.getByDepartmentId(departmentId));
    }
}
