package org.chinasoft.controller;

import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.entity.VisitingCard;
import org.chinasoft.service.VisitingCardService;
import org.chinasoft.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zh on 2018/6/27.
 */
@Controller
public class VisitingCardController {

    @Autowired
    private VisitingCardService visitingCardService;

    @RequestMapping(value = "/addVisitingCard",method=RequestMethod.POST)
    @ExceptionHandler
    @ResponseBody
    public ResultBean addVisitingCard(@RequestBody VisitingCard visitingCard){
        System.out.println(visitingCard);
        visitingCardService.addVisitingCard(visitingCard);
        return new ResultBean().success();
    }
    @ExceptionHandler
    @ResponseBody
    @RequestMapping("/helloWorld")
    public ResultBean helloWorld(){
        return new ResultBean().success();
    }
}
