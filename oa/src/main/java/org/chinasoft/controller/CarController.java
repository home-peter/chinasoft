package org.chinasoft.controller;


import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.aspect.Log;
import org.chinasoft.entity.ApplicationCar;
import org.chinasoft.entity.Car;
import org.chinasoft.service.ApplicationCarService;
import org.chinasoft.service.CarService;
import org.chinasoft.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by zh on 2018/6/27.
 */
@Controller
public class CarController {

    @Autowired
    private CarService carService;

    @Autowired
    private ApplicationCarService applicationCarService;

    @RequestMapping("/addCar")
    @ResponseBody
    @ExceptionHandler
    @Log(note = "执行用车管理")
    public ResultBean addCar(@RequestBody Car car) {
        return new ResultBean<>(carService.addCar(car));
    }

    @RequestMapping(value = "/delCar/{id}", method = RequestMethod.DELETE)
    @ExceptionHandler
    @Log(note = "删除一条用车记录")
    @ResponseBody
    public ResultBean delCar(@PathVariable("id") int id){
        carService.delCar(id);
        return new ResultBean().success();
    }

    @RequestMapping("/updateCar")
    @ResponseBody
    @ExceptionHandler
    @Log(note = "修改用车记录")
    public ResultBean updateCar(@RequestBody Car car) {
        return new ResultBean<>(carService.updateCar(car));
    }

    @RequestMapping("/getCarById/{id}")
    public String getCarById(@PathVariable int id, Model model) {
        Car car = carService.getCarById(id);
        model.addAttribute("car", car);
        return "administration/updateCar";
    }

    @RequestMapping(value = "/getAllCar", method = RequestMethod.GET)
    @ResponseBody
    public List<Car> getAllCar() {
        return carService.getAll();
    }

    @RequestMapping(value = "/addApplicationCar", method = RequestMethod.PUT)
    @ResponseBody
    @ExceptionHandler
    public ResultBean addApplicationCar(@RequestBody ApplicationCar applicationCar) {
        return new ResultBean<>(applicationCarService.addApplicationCar(applicationCar));
    }
}
