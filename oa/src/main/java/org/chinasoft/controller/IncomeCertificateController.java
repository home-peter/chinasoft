package org.chinasoft.controller;

import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.aspect.Log;
import org.chinasoft.entity.IncomeCertificate;
import org.chinasoft.entity.LeaveSlip;
import org.chinasoft.service.IncomeCertificateService;
import org.chinasoft.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Created by Fang on 2018/6/26.
 */
@Controller
@RequestMapping("/IncomeCertificate")
public class IncomeCertificateController {
    @Autowired
    IncomeCertificateService incomeCertificateService;

    @RequestMapping("{id}/getById")
    public String getById(@PathVariable("id") int id, HttpServletRequest request) {
        IncomeCertificate incomeCertificate = incomeCertificateService.getById(id);
        request.setAttribute("incomeCertificate", incomeCertificate);
        return "";
    }

    @RequestMapping(value = "/add",method=RequestMethod.POST)
    @ResponseBody
    @Log(note = "添加收入流程证明申请")
    public ResultBean add(@RequestBody IncomeCertificate incomeCertificate) {
        incomeCertificateService.add(incomeCertificate);
        return new ResultBean().success();
    }

    @RequestMapping(value = "{id}/update",method = RequestMethod.GET)
    @ResponseBody
    @ExceptionHandler
    @Log(note = "修改收入流程证明申请")
    public ResultBean update(@RequestBody IncomeCertificate incomeCertificate) {
        incomeCertificateService.update(incomeCertificate);
        return new ResultBean().success();
    }

    @RequestMapping(value = "{id}/delete")
    @ResponseBody
    @Log(note = "删除收入流程证明申请")
    public ResultBean delete(@PathVariable("id")int id) {
        incomeCertificateService.delete(id);
            return new ResultBean().success();
    }

    @RequestMapping("findAll")
    @ResponseBody
    @ExceptionHandler
    public ResultBean<List<IncomeCertificate>> getIncomeCertificateList(){
        return new ResultBean<>(incomeCertificateService.findAll());
    }
}
