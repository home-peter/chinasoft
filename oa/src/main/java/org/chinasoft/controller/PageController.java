package org.chinasoft.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {
    @GetMapping("/login")
    public String toLoginPage() {
        return "/login";
    }

    @GetMapping("/index")
    public String toMainPage() {
        return "/index";
    }
}
