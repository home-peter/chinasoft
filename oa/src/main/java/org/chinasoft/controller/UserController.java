package org.chinasoft.controller;

import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.aspect.Log;
import org.chinasoft.entity.Job;
import org.chinasoft.entity.Role;
import org.chinasoft.entity.User;
import org.chinasoft.service.UserService;
import org.chinasoft.utils.ResultBean;
import org.chinasoft.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    @ResponseBody
    @ExceptionHandler
    public ResultBean<User> getUser(@PathVariable Long id) {
        return new ResultBean<>(userService.findByUserId(id));
    }

    @GetMapping("/user/list")
    @ResponseBody
    @ExceptionHandler
    public ResultBean<List<User>> getUserList() {
        return new ResultBean<>(userService.findAllUser());
    }

    @PutMapping("/user/save")
    @ResponseBody
    @ExceptionHandler
    @Log(note = "添加用户")
    public ResultBean register(@RequestBody UserDTO userDTO) {
        Job job = new Job();
        job.setJobId(userDTO.getJobId());
        Set<Role> roleSet = new HashSet<>();
        Role role = new Role();
        role.setRoleId(userDTO.getRoleId());
        roleSet.add(role);

        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setRealName(userDTO.getRealName());
        user.setGender(userDTO.getGender());
        user.setStatus(userDTO.getStatus());
        user.setJob(job);
        user.setRoleSet(roleSet);

        userService.save(user);
        return new ResultBean().success();
    }

    @PostMapping("/user/edit/{userId}")
    @ResponseBody
    @ExceptionHandler
    @Log(note = "编辑用户")
    public ResultBean editUser(@PathVariable("userId") Long userId, @RequestBody UserDTO userDTO) {
        User user = userService.findByUserId(userId);

        Job job = new Job();
        job.setJobId(userDTO.getJobId());
        Set<Role> roleSet = new HashSet<>();
        Role role = new Role();
        role.setRoleId(userDTO.getRoleId());
        roleSet.add(role);

        user.setPassword(userDTO.getPassword());
        user.setRealName(userDTO.getRealName());
        user.setGender(userDTO.getGender());
        user.setStatus(userDTO.getStatus());
        user.setJob(job);
        user.setRoleSet(roleSet);

        userService.update(user);
        return new ResultBean().success();
    }

    @DeleteMapping("/user/{userId}/remove")
    @ResponseBody
    @ExceptionHandler
    @Log(note = "删除用户")
    public ResultBean deleteUser(@PathVariable("userId") Long userId) {
        User user = userService.findByUserId(userId);
        userService.remove(user);
        return new ResultBean().success();
    }

    @GetMapping("/user/edit/{userId}")
    public String toEditPage(@PathVariable("userId") Long userId, Model model) {
        User user = userService.findByUserId(userId);
        model.addAttribute("user", user);

        return "userManager/edit";
    }
}
