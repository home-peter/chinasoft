package org.chinasoft.controller;

import org.chinasoft.aspect.ExceptionHandler;
import org.chinasoft.service.LogService;
import org.chinasoft.utils.ResultBean;
import org.chinasoft.vo.UserLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class LogController {
    @Autowired
    private LogService logService;

    @GetMapping("/log/list")
    @ExceptionHandler
    @ResponseBody
    public ResultBean<List<UserLogVo>> getAllLog() {
        return new ResultBean<>(logService.getAll());
    }
}
