package org.chinasoft.service;

import org.chinasoft.entity.ChapterUse;

/**
 * Created by zh on 2018/6/26.
 */
public interface ChapterUseService {
    /**
     * 保存用章记录
     * @param chapterUse
     * @return
     */
    int addChapterUse(ChapterUse chapterUse);
}
