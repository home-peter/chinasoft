package org.chinasoft.service;

import org.chinasoft.entity.Log;
import org.chinasoft.vo.UserLogVo;

import java.util.List;

public interface LogService {
    Integer save(Log log);

    List<UserLogVo> getAll();

}
