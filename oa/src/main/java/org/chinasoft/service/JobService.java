package org.chinasoft.service;

import org.chinasoft.entity.Job;

import java.util.List;

public interface JobService {
    /**
     * 根据职业编号查询
     *
     * @param jobId
     * @return
     */
    Job getByJobId(Long jobId);

    /**
     * 根据部门编号查询该部门下职位
     *
     * @param departmentId
     * @return
     */
    List<Job> getByDepartmentId(Long departmentId);
}
