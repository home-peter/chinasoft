package org.chinasoft.service.impl;

import org.chinasoft.dao.MeetDao;
import org.chinasoft.entity.Meet;
import org.chinasoft.service.MeetService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by zh on 2018/6/25.
 */
@Service(value = "meetService")
public class MeetServiceImpl implements MeetService {
    @Resource
    private MeetDao meetDao;

    public int addMeet(Meet meet) {
        int i = meetDao.addMeet(meet);
        return i;
    }

    public int delMeet(int id) {
        int i = meetDao.delMeet(id);
        return i;
    }

    public int updateMeet(Meet meet) {
        int i = meetDao.updateMeet(meet);
        return i;
    }

    public List<Meet> getAll() {
        List<Meet> list = meetDao.getAll();
        return list;
    }

    @Override
    public List<Meet> getMeetByMeetTime(Date meetTime) {
        List<Meet> list = meetDao.getCarByMeetTime(meetTime);
        return list;
    }

    @Override
    public Meet getMeetById(int id) {
        Meet meet = meetDao.getMeetById(id);
        return meet;
    }
}
