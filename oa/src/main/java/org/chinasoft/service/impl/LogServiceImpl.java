package org.chinasoft.service.impl;

import org.chinasoft.dao.LogMapper;
import org.chinasoft.dao.UserMapper;
import org.chinasoft.entity.Log;
import org.chinasoft.entity.User;
import org.chinasoft.service.LogService;
import org.chinasoft.vo.UserLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogMapper logMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public Integer save(Log log) {
        return logMapper.save(log);
    }

    @Override
    public List<UserLogVo> getAll() {
        List<UserLogVo> userLogVoList = new ArrayList<>();
        for (Log log : logMapper.getAll()) {
            UserLogVo userLogVo = new UserLogVo();
            User user = userMapper.getByUserId(log.getUserId());
            userLogVo.setLog(log);
            userLogVo.setUser(user);

            userLogVoList.add(userLogVo);
        }
        return userLogVoList;
    }
}
