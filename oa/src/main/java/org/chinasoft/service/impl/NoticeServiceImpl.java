package org.chinasoft.service.impl;

import org.chinasoft.dao.NoticeMapper;
import org.chinasoft.entity.Notice;
import org.chinasoft.entity.PageBean;
import org.chinasoft.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * Created by lfy on 2018/6/25.
 */
@Service("NoticeService")
@Transactional
public class NoticeServiceImpl implements NoticeService{
    @Autowired
    private NoticeMapper noticeMapper;

    public int addNotice(Notice notice) {
        return noticeMapper.addNotice(notice);
    }

    public int delNoticeById(Long id) {
        return noticeMapper.delNoticeById(id);
    }

    public int updateNotice(Notice notice) {
        return noticeMapper.updateNotice(notice);
    }

    public Notice findNoticeById(Long id) {
        return noticeMapper.findNoticeById(id);
    }

    public List<Notice> findAll() {
        return noticeMapper.findAll();
    }

    public PageBean<Notice> findByPage(int currentPage,int pageSize,int flag) {
        HashMap<String,Object> map=new HashMap<String, Object>();
        PageBean<Notice> pageBean=new PageBean<Notice>();
        //封装当前页码和每页通告条数
        pageBean.setCurrentPage(currentPage);
        pageBean.setPageSize(pageSize);
        //计算从第几条开始查询
        int start=(currentPage-1)*pageSize;
        map.put("start",start);
        map.put("pageSize",pageSize);
        //是否需要增加条件查询 已发布未过期的，若flag=1则要增加条件
        map.put("flag",flag);
        //封装总页数
        double count=noticeMapper.getCount();
        Double totalPage=Math.ceil(count/pageSize);
        pageBean.setTotalPage(totalPage.intValue());
        //查询
        List<Notice> noticeList =noticeMapper.findByPage(map);
        //封装查询结果
        pageBean.setLists(noticeList);
        return pageBean;
    }

    @Override
    public List<Notice> findAllNoticeAndUser() {

        return noticeMapper.findAllNoticeAndUser();
    }
}
