package org.chinasoft.service.impl;

import org.chinasoft.dao.MeetingRoomDao;
import org.chinasoft.entity.MeetingRoom;
import org.chinasoft.service.MeetingRoomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zh on 2018/6/25.
 */
@Service(value = "meetingRoomService")
public class MeetingRoomServiceImpl implements MeetingRoomService {
    @Resource
    private MeetingRoomDao meetingRoomDao;

    public int addMeetingRoom(MeetingRoom meetingRoom) {
        int i = meetingRoomDao.addMeetingRoom(meetingRoom);
        return i;
    }

    @Override
    public MeetingRoom getMeetingRoom(MeetingRoom meetingRoom) {
        MeetingRoom meetingRoom1 = meetingRoomDao.getMeetingRoom(meetingRoom);
        return meetingRoom1;
    }
}
