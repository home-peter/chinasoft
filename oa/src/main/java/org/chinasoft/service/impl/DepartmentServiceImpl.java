package org.chinasoft.service.impl;

import org.chinasoft.dao.DepartmentMapper;
import org.chinasoft.entity.Department;
import org.chinasoft.enums.SystemStatus;
import org.chinasoft.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public Department getByDepartmentId(Long departmentId) {

        return departmentMapper.getByDepartmentId(departmentId);
    }

    @Override
    public List<Department> getAll() {
        return departmentMapper.getAll()
                .stream()
                .filter(department -> SystemStatus.normal.getCode().equals(department.getStatus()))
                .collect(Collectors.toList());
    }
}
