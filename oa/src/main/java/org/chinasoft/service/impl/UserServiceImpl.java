package org.chinasoft.service.impl;

import org.chinasoft.dao.UserMapper;
import org.chinasoft.dao.UserRoleMapper;
import org.chinasoft.entity.Role;
import org.chinasoft.entity.User;
import org.chinasoft.enums.SystemStatus;
import org.chinasoft.service.UserService;
import org.chinasoft.utils.CheckUtil;
import org.chinasoft.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Transactional
    public User save(User user) {
        CheckUtil.check(user != null, "user is null");
        CheckUtil.check(StringUtil.isNotEmpty(user.getUsername()), "用户名不能为空");
        CheckUtil.check(userMapper.countByUsername(user.getUsername()) == 0, "用户名已存在");
        CheckUtil.check(StringUtil.isNotEmpty(user.getPassword()), "密码不能为空");
        CheckUtil.check(StringUtil.isNotEmpty(user.getRealName()), "真实姓名不能为空");
        CheckUtil.check(StringUtil.isNotEmpty(user.getGender()), "性别不能为空");
        CheckUtil.check(user.getJob().getJobId() != null, "职称不能为空");
        CheckUtil.check(user.getRoleSet() != null, "角色不能为空");
        CheckUtil.check(user.getStatus() != null, "用户状态不能为空");


        // 数据校验无误添加到数据库
        userMapper.save(user);

        // 去重
        userRoleMapper.removeByUserId(user.getUserId());
        // 添加用户角色关系
        for (Role role : user.getRoleSet()) {
            // 添加关系
            userRoleMapper.save(user.getUserId(), role.getRoleId());
        }
        return user;
    }

    @Override
    @Transactional
    public User update(User user) {
        CheckUtil.check(user != null && user.getUserId() != null, "user is null");
        CheckUtil.check(StringUtil.isNotEmpty(user.getUsername()), "用户名不能为空");
        CheckUtil.check(StringUtil.isNotEmpty(user.getPassword()), "密码不能为空");
        CheckUtil.check(StringUtil.isNotEmpty(user.getRealName()), "真实姓名不能为空");
        CheckUtil.check(StringUtil.isNotEmpty(user.getGender()), "性别不能为空");
        CheckUtil.check(user.getJob().getJobId() != null, "职称不能为空");
        CheckUtil.check(user.getRoleSet() != null, "角色不能为空");
        CheckUtil.check(user.getStatus() != null, "用户状态不能为空");

        // 数据校验无误则更新
        userMapper.update(user);

        // 去重
        userRoleMapper.removeByUserId(user.getUserId());
        // 添加用户角色关系
        for (Role role : user.getRoleSet()) {
            // 添加关系
            userRoleMapper.save(user.getUserId(), role.getRoleId());
        }

        return user;
    }

    @Override
    @Transactional
    public void remove(User user) {
        CheckUtil.check(user != null, "不能删除为空的用户");
        user.setStatus(SystemStatus.removed.getCode());
        userMapper.update(user);
    }

    @Override
    public User findByUsername(String username) {
        CheckUtil.check(StringUtil.isNotEmpty(username), "username can't be empty");
        return userMapper.getByUsername(username);
    }

    @Override
    public User findByUserId(Long userId) {
        return userMapper.getByUserId(userId);
    }

    @Override
    public List<User> findAllUser() {
        return userMapper.getAll()
                .stream()
                .filter(user -> SystemStatus.normal.getCode().equals(user.getStatus()))
                .collect(Collectors.toList());
    }
}
