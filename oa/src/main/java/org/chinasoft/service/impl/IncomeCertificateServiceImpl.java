package org.chinasoft.service.impl;

import org.chinasoft.dao.IncomeCertificateMapper;
import org.chinasoft.entity.IncomeCertificate;
import org.chinasoft.service.IncomeCertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Fang on 2018/6/25.
 */
@Service("IncomeCertificateService")
public class IncomeCertificateServiceImpl implements IncomeCertificateService {
    @Autowired
    private IncomeCertificateMapper incomeCertificateMapper;

    public int add(IncomeCertificate incomeCertificate) {
        return incomeCertificateMapper.add(incomeCertificate);
    }
    public int delete(int id) {
        return incomeCertificateMapper.delete(id);
    }
    public int update(IncomeCertificate incomeCertificate) {
        return incomeCertificateMapper.update(incomeCertificate);
    }
    public IncomeCertificate getById(int id) {
        return incomeCertificateMapper.getById(id);
    }
    public List<IncomeCertificate> findAll() {
        return incomeCertificateMapper.findAll();
    }
}
