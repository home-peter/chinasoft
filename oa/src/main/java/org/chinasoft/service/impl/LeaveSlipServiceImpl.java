package org.chinasoft.service.impl;

import org.chinasoft.dao.LeaveSlipMapper;
import org.chinasoft.entity.LeaveSlip;
import org.chinasoft.service.LeaveSlipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Fang on 2018/6/26.
 */
@Service("LeaveSlipService")
public class LeaveSlipServiceImpl implements LeaveSlipService {
    @Autowired
    private LeaveSlipMapper leaveSlipMapper;
    public int add(LeaveSlip leaveSlip) {
        return leaveSlipMapper.add(leaveSlip);
    }
    public int delete(int id) {
        return leaveSlipMapper.delete(id);
    }
    public int update(LeaveSlip leaveSlip) {
        return leaveSlipMapper.update(leaveSlip);
    }
    public LeaveSlip getById(int id) {
        return leaveSlipMapper.getById(id);
    }
    public List<LeaveSlip> findAll() {
        return leaveSlipMapper.findAll();
    }

}
