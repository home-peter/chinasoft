package org.chinasoft.service.impl;

import org.chinasoft.dao.BookDao;
import org.chinasoft.entity.Book;
import org.chinasoft.service.BookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zh on 2018/6/26.
 */
@Service(value = "bookService")
public class BookServiceImpl implements BookService {
    @Resource
    private BookDao bookDao;

    public int addBook(Book book) {
        int i = bookDao.addBook(book);
        return i;
    }
}
