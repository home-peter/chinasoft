package org.chinasoft.service.impl;

import org.chinasoft.dao.VisitingCardDao;
import org.chinasoft.entity.VisitingCard;
import org.chinasoft.service.VisitingCardService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zh on 2018/6/26.
 */
@Service(value = "visitingCardService")
public class VisitingCardServiceImpl implements VisitingCardService {

    @Resource
    private VisitingCardDao visitingCardDao;

    public int addVisitingCard(VisitingCard visitingCard) {
        int i = visitingCardDao.addVisitingCard(visitingCard);
        return i;
    }
}
