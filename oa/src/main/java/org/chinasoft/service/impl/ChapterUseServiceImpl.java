package org.chinasoft.service.impl;

import org.chinasoft.dao.ChapterUseDao;
import org.chinasoft.entity.ChapterUse;
import org.chinasoft.service.ChapterUseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zh on 2018/6/26.
 */
@Service(value = "chapterUseService")
public class ChapterUseServiceImpl implements ChapterUseService {
    @Resource
    private ChapterUseDao chapterUseDao;

    public int addChapterUse(ChapterUse chapterUse) {
        int i = chapterUseDao.addChapterUse(chapterUse);
        return i;
    }
}
