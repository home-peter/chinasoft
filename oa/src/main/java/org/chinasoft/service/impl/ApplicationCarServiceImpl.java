package org.chinasoft.service.impl;

import org.chinasoft.dao.ApplicationCarDao;
import org.chinasoft.entity.ApplicationCar;
import org.chinasoft.service.ApplicationCarService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zh on 2018/6/26.
 */
@Service(value = "applicationCarService")
public class ApplicationCarServiceImpl implements ApplicationCarService {
    @Resource
    private ApplicationCarDao applicationCarDao;

    public int addApplicationCar(ApplicationCar applicationCar) {
        int i = applicationCarDao.addApplicationCar(applicationCar);
        return i;
    }
}
