package org.chinasoft.service.impl;

import org.chinasoft.dao.JobMapper;
import org.chinasoft.entity.Job;
import org.chinasoft.enums.SystemStatus;
import org.chinasoft.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JobServiceImpl implements JobService {
    @Autowired
    private JobMapper jobMapper;

    @Override
    public Job getByJobId(Long jobId) {
        return jobMapper.getByJobId(jobId);
    }

    @Override
    public List<Job> getByDepartmentId(Long departmentId) {
        return jobMapper.getByDepartmentId(departmentId)
                .stream()
                .filter(job -> SystemStatus.normal.getCode().equals(job.getStatus()))
                .collect(Collectors.toList());
    }
}
