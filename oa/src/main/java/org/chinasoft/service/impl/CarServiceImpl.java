package org.chinasoft.service.impl;

import org.chinasoft.dao.CarDao;
import org.chinasoft.entity.Car;
import org.chinasoft.service.CarService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by zh on 2018/6/26.
 */
@Service(value = "carService")
public class CarServiceImpl implements CarService {
    @Resource
    private CarDao carDao;

    public int addCar(Car car) {
        int i = carDao.addCar(car);
        return i;
    }

    public int delCar(int id) {
        int i = carDao.delCar(id);
        return i;
    }

    public int updateCar(Car car) {
        int i = carDao.updateCar(car);
        return i;
    }

    public List<Car> getAll() {
        List<Car> list= carDao.getAll();
        return list;
    }

    @Override
    public Car getCarById(int id) {
        Car car = carDao.getCarById(id);
        return car;
    }
}
