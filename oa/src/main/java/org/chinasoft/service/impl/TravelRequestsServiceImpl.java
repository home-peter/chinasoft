package org.chinasoft.service.impl;

import org.chinasoft.dao.TravelRequestsMapper;
import org.chinasoft.entity.TravelRequests;
import org.chinasoft.service.TravelRequestsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Fang on 2018/6/26.
 */
@Service("TravelRequestsService")
public class TravelRequestsServiceImpl implements TravelRequestsService {
    @Autowired
    private TravelRequestsMapper travelRequestsMapper;
    public int add(TravelRequests travelRequests) {
        return travelRequestsMapper.add(travelRequests);
    }

    public int delete(int id) {
        return travelRequestsMapper.delete(id);
    }

    public int update(TravelRequests travelRequests) {
        return travelRequestsMapper.update(travelRequests);
    }

    public TravelRequests getById(int id) {
        return travelRequestsMapper.getById(id);
    }

    public List<TravelRequests> findAll() {
        return travelRequestsMapper.findAll();
    }
}
