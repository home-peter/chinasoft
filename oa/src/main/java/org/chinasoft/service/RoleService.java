package org.chinasoft.service;

import org.chinasoft.entity.Menu;
import org.chinasoft.entity.Role;

import java.util.List;
import java.util.Set;

public interface RoleService {
    /**
     * 通过角色编号查询
     *
     * @param roleId
     * @return
     */
    Role getByRoleId(Long roleId);

    /**
     * 查询所有角色
     *
     * @return
     */
    Set<Role> getAll();

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    Role save(Role role);

    /**
     * 给角色赋权
     *
     * @param menuList
     * @return
     */
    Role assignAccess(Role currentRole, List<Menu> menuList);

    /**
     * 通过角色编号删除角色
     *
     * @param roleId
     */
    void removeByRoleId(Long roleId);

    /**
     * 更新角色
     *
     * @param role
     */
    void update(Role role);
}
