package org.chinasoft.service;

import org.chinasoft.entity.Menu;
import org.chinasoft.vo.MenuTreeNode;

import java.util.List;
import java.util.Set;

public interface MenuService {
    /**
     * 查询所有菜单
     *
     * @return
     */
    List<Menu> getAll();

    /**
     * 通过角色编号查询菜单集合
     *
     * @param roleId
     * @return
     */
    Set<Menu> getByRoleId(Long roleId);

    /**
     * 生成菜单树
     *
     * @param menuList
     * @return
     */
    List<MenuTreeNode> buildTreeByAllMenuList(List<Menu> menuList);
}
