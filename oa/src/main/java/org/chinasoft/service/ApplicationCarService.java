package org.chinasoft.service;

import org.chinasoft.entity.ApplicationCar;

/**
 * Created by zh on 2018/6/26.
 */
public interface ApplicationCarService {
    /**
     * 增加用车记录
     * @param applicationCar
     * @return
     */
    int addApplicationCar(ApplicationCar applicationCar);

}
