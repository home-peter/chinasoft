package org.chinasoft.service;

import org.chinasoft.entity.IncomeCertificate;

import java.util.List;

/**
 * Created by Fang on 2018/6/25.
 */
public interface IncomeCertificateService {
    /**
     * 添加
     * @param incomeCertificate 收入证明实例
     * @return int
     */
     int add(IncomeCertificate incomeCertificate);
    /**
     * 删除
     * @param id 收入证明实例id
     * @return int
     */
     int delete(int id);
    /**
     * 修改
     * @param incomeCertificate 收入证明实例
     * @return int
     */
     int update(IncomeCertificate incomeCertificate);
    /**
     * 通过id查找
     * @param id 收入证明实例id
     * @return incomeCertificate
     */
     IncomeCertificate getById(int id);

    /**
     * 查找所有实例
     * @return  List<IncomeCertificate>
     */
     List<IncomeCertificate> findAll();
}
