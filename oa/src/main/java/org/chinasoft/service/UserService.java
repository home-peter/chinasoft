package org.chinasoft.service;

import org.chinasoft.entity.User;

import java.util.List;

public interface UserService {
    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    User save(User user);

    /**
     * 更新用户
     *
     * @param user
     * @return
     */
    User update(User user);

    /**
     * 删除用户
     *
     * @param user
     */
    void remove(User user);

    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return
     */
    User findByUsername(String username);

    /**
     * 通过用户编号查找用户
     *
     * @param userId
     * @return
     */
    User findByUserId(Long userId);

    /**
     * 查询所有用户
     *
     * @return
     */
    List<User> findAllUser();
}
