package org.chinasoft.service;

import org.chinasoft.entity.MeetingRoom;

/**
 * Created by zh on 2018/6/25.
 */
public interface MeetingRoomService {
    int addMeetingRoom(MeetingRoom meetingRoom);
    MeetingRoom getMeetingRoom(MeetingRoom meetingRoom);
}
