package org.chinasoft.service;

import org.chinasoft.entity.TravelRequests;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Fang on 2018/6/26.
 */
public interface TravelRequestsService {
     /**
      * 添加
      * @param travelRequests 出差申请实例
      * @return int
      */
     int add(TravelRequests travelRequests);
     /**
      * 删除
      * @param id 出差申请实例id
      * @return int
      */
     int delete(int id);
     /**
      * 修改
      * @param travelRequests 出差申请实例
      * @return int
      */
     int update(TravelRequests travelRequests);
     /**
      * 通过id查找
      * @param id 出差申请实例id
      * @return TravelRequests
      */
     TravelRequests getById(int id);
     /**
      * 查找所有实例
      * @return  List<TravelRequests>
      */
     List<TravelRequests> findAll();
}
