package org.chinasoft.service;

import org.chinasoft.entity.Book;

/**
 * Created by zh on 2018/6/26.
 */
public interface BookService {
    /**
     * 保存借书记录
     * @param book
     * @return
     */
    int addBook(Book book);
}
