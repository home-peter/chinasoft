package org.chinasoft.service;

import org.chinasoft.entity.Notice;
import org.chinasoft.entity.PageBean;
import org.chinasoft.entity.User;

import java.util.List;

/**
 * Created by lfy on 2018/6/25.
 */

public interface NoticeService {
    /**
     * 新增一个通告
     * @param notice 通告实例
     * @return int
     */
    public int addNotice(Notice notice);

    /**
     * 删除一个通告
     * @param id 通告编号
     * @return int
     */
    public int delNoticeById(Long id);

    /**
     * 修改一个通告
     * @param notice 通告实例
     * @return int
     */
    public int updateNotice(Notice notice);

    /**
     * 查找一个通告
     * @param id 通告编号
     * @return Notice notice 所查找的通告
     */
    public Notice findNoticeById(Long id);

    /**
     * 查找所有通告
     * @return List<Notice>
     */
    public List<Notice> findAll();

    /**
     * 分页查询
     * @param currentPage 当前页码
     * @param pageSize    每页的通告条数
     * @param flag       是否需要增加条件查询已发布未过期的通告 flag为1则增加该条件，0或其他不增加
     * @return  PageBean<Notice> 含有所查询List<Notice>的 PageBean
     */
    public PageBean<Notice> findByPage(int currentPage,int pageSize,int flag);

    /**
     * 查找通告和其发布者
     * @return List<Notice>
     */
    public List<Notice> findAllNoticeAndUser();
}
