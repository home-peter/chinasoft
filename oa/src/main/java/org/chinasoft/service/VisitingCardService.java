package org.chinasoft.service;

import org.chinasoft.entity.VisitingCard;

/**
 * Created by zh on 2018/6/26.
 */
public interface VisitingCardService {
    /**
     * 保存制作名片申请
     * @param visitingCard
     * @return
     */
    int addVisitingCard(VisitingCard visitingCard);
}
