package org.chinasoft.service;

import org.chinasoft.entity.Department;

import java.util.List;

public interface DepartmentService {
    /**
     * 通过部门编号获取部门
     *
     * @param departmentId
     * @return
     */
    Department getByDepartmentId(Long departmentId);

    /**
     * 获取所有部门
     *
     * @return
     */
    List<Department> getAll();
}
