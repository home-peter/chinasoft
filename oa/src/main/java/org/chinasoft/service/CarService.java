package org.chinasoft.service;

import org.chinasoft.entity.Car;
import org.springframework.dao.DataAccessException;

import java.util.Date;
import java.util.List;

/**
 * Created by zh on 2018/6/26.
 */
public interface CarService {
    /**
     * 添加车辆
     * @param car
     * @return
     */
    int addCar(Car car);

    /**
     * 删除车辆
     * @param id 车辆ID
     * @return
     */
    int delCar(int id);

    /**
     * 更新车辆信息
     * @param car
     * @return
     */
    int updateCar(Car car);

    /**
     * 得到全部车辆信息
     * @return
     */
    List<Car> getAll();
    Car getCarById(int id);
}
