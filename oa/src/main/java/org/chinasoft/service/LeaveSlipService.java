package org.chinasoft.service;

import org.chinasoft.entity.LeaveSlip;

import java.util.List;

/**
 * Created by Fang on 2018/6/26.
 */
public interface LeaveSlipService {
     /**
      * 添加
      * @param leaveSlip 请假申请实例
      * @return int
      */
     int add(LeaveSlip leaveSlip);
     /**
      * 删除
      * @param id 请假申请实例id
      * @return int
      */
     int delete(int id);
     /**
      * 修改
      * @param leaveSlip 请假申请实例
      * @return int
      */
     int update(LeaveSlip leaveSlip);
     /**
      * 通过id查找
      * @param id 请假申请实例id
      * @return LeaveSlip
      */
     LeaveSlip getById(int id);
     /**
      * 查找所有实例
      * @return  List<LeaveSlip>
      */
     List<LeaveSlip> findAll();
}
