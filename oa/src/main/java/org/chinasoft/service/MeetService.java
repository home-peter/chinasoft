package org.chinasoft.service;

import org.chinasoft.entity.Meet;

import java.util.Date;
import java.util.List;

/**
 * Created by zh on 2018/6/25.
 */
public interface MeetService {
    int addMeet(Meet meet);

    int delMeet(int id);

    int updateMeet(Meet meet);

    List<Meet> getAll();

    List<Meet> getMeetByMeetTime(Date meetTime);

    Meet getMeetById(int id);
}