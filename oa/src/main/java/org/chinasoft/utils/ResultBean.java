package org.chinasoft.utils;

public class ResultBean<T> {
    public static final int SUCCESS = 0;
    public static final int FAIL = 1;
    public static final int UNAUTH = 403;

    private int code;
    private String info;
    private T data;

    public ResultBean() {
    }

    public ResultBean(T data) {
        this.code = SUCCESS;
        this.info = "success";
        this.data = data;
    }

    public ResultBean(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public ResultBean(Throwable e) {
        this.code = FAIL;
        this.info = e.getLocalizedMessage();
    }

    public ResultBean success() {
        this.code = SUCCESS;
        this.info = "success";
        return this;
    }

    public ResultBean unAuth() {
        this.code = UNAUTH;
        this.info = "un auth";
        return this;
    }

    public ResultBean failed() {
        this.code = FAIL;
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResultBean{" +
                "code=" + code +
                ", info='" + info + '\'' +
                ", data=" + data +
                '}';
    }
}
