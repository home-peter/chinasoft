package org.chinasoft.enums;

public enum SystemStatus {
    normal(0), removed(1), userLocked(2);

    private Integer code;

    SystemStatus(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
