package org.chinasoft.shiro;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

// 密码匹配器
public class CredentialsMatcher extends SimpleCredentialsMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken uToken = (UsernamePasswordToken) token;
        // 获取用户输入的密码
        String inPassword = new String(uToken.getPassword());
        // 获取得数据库中的密码
        String dbPassword = (String) info.getCredentials();
        // 进行密码的比较
        return this.equals(inPassword, dbPassword);
    }
}
