package org.chinasoft.dao;

import org.chinasoft.entity.IncomeCertificate;

import java.util.List;

/**
 * Created by Fang on 2018/6/25.
 */
public interface IncomeCertificateMapper {
     int add(IncomeCertificate incomeCertificate);
     int delete(int id);
     int update(IncomeCertificate incomeCertificate);
     IncomeCertificate getById(int id);
     List<IncomeCertificate> findAll();

}
