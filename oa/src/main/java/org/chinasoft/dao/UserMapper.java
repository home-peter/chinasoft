package org.chinasoft.dao;

import org.chinasoft.entity.User;

import java.util.List;

public interface UserMapper {
    /**
     * 通过用户编号查询
     *
     * @param userId 用户编号
     * @return 用户实体
     */
    User getByUserId(Long userId);

    /**
     * 查询所有用户
     *
     * @return
     */
    List<User> getAll();

    /**
     * 通过用户名查找用户
     *
     * @param username 用户名
     * @return
     */
    User getByUsername(String username);

    /**
     * 通过用户名模糊查询
     *
     * @param username
     * @return
     */
    List<User> queryByUsername(String username);

    /**
     * 通过真实姓名查询
     *
     * @param realName
     * @return
     */
    List<User> queryByRealName(String realName);

    /**
     * 保存用户
     *
     * @return
     */
    Integer save(User user);

    /**
     * 通过用户名查找用户个数
     *
     * @param username
     * @return
     */
    Integer countByUsername(String username);

    /**
     * 更新用户
     *
     * @param user
     * @return
     */
    Integer update(User user);
}
