package org.chinasoft.dao;

import org.chinasoft.entity.Department;

import java.util.List;

public interface DepartmentMapper {
    /**
     * 通过部门编号获取部门
     *
     * @param departmentId
     * @return
     */
    Department getByDepartmentId(Long departmentId);

    List<Department> getAll();
}
