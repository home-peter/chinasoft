package org.chinasoft.dao;

import org.chinasoft.entity.TravelRequests;

import java.util.List;

/**
 * Created by Fang on 2018/6/25.
 */
public interface TravelRequestsMapper {
     int add(TravelRequests travelRequests);
     int delete(int id);
     int update(TravelRequests travelRequests);
     TravelRequests getById(int id);
     List<TravelRequests> findAll();

}
