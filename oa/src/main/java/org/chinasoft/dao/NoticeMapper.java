package org.chinasoft.dao;

import org.chinasoft.entity.Notice;

import java.util.HashMap;
import java.util.List;

/**
 * Created by lfy on 2018/6/25.
 */
public interface NoticeMapper {
     /**
      * 新增一个通告
      * @param notice 通告实例
      * @return int
      */
     public int addNotice(Notice notice);
     /**
      * 删除一个通告
      * @param id 通告编号
      * @return int
      */
     public int delNoticeById(Long id);
     /**
      * 修改一个通告
      * @param notice 通告实例
      * @return int
      */
     public int updateNotice(Notice notice);
     /**
      * 查找一个通告
      * @param id 通告编号
      * @return Notice notice 所查找的通告
      */
     public Notice findNoticeById(Long id);
     /**
      * 查找所有通告
      * @return List<Notice>
      */
     public List<Notice> findAll();

     /**
      * 分页查找通告
      * @param map
      * @return List<Notice>
      */
     public List<Notice> findByPage(HashMap<String,Object> map);

     /**
      * 的到通告总条数
      * @return int 通告条数
      */
     public int getCount();

     public List<Notice> findAllNoticeAndUser();
}
