package org.chinasoft.dao;

import org.apache.ibatis.annotations.Param;
import org.chinasoft.entity.Menu;
import org.chinasoft.entity.RoleMenu;

import java.util.Set;

public interface RoleMenuMapper {
    /**
     * 通过主键查询
     *
     * @param roleMenuId
     * @return
     */
    RoleMenu getByRoleMenuId(Long roleMenuId);

    /**
     * 通过角色编号查询菜单集合
     *
     * @param roleId
     * @return
     */
    Set<Menu> getMenuSetByRoleId(Long roleId);

    /**
     * 添加角色-权限
     *
     * @param roleId
     * @param menuId
     * @return
     */
    Integer save(@Param("roleId") Long roleId, @Param("menuId") Long menuId);

    /**
     * 通过角色编号删除 角色-权限关系
     *
     * @param roleId
     * @return
     */
    Integer removeByRoleId(Long roleId);
}
