package org.chinasoft.dao;

import org.chinasoft.entity.Book;

/**
 * Created by zh on 2018/6/26.
 */
public interface BookDao {
    int addBook(Book book);
}
