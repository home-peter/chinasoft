package org.chinasoft.dao;

import org.chinasoft.entity.ChapterUse;

/**
 * Created by zh on 2018/6/26.
 */
public interface ChapterUseDao {
    int addChapterUse(ChapterUse chapterUse);
}
