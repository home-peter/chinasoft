package org.chinasoft.dao;

import org.apache.ibatis.annotations.Param;
import org.chinasoft.entity.Role;
import org.chinasoft.entity.User;
import org.chinasoft.entity.UserRole;

import java.util.Set;

public interface UserRoleMapper {
    /**
     * 通过用户-角色编号查询
     *
     * @param userRoleId
     * @return
     */
    UserRole getByUserRoleId(Long userRoleId);

    /**
     * 通过用户编号查询拥有的角色
     *
     * @param userId
     * @return
     */
    Set<Role> getRoleSetByUserId(Long userId);

    /**
     * 通过角色编号查询该角色下的所以用户
     *
     * @param roleId
     * @return
     */
    Set<User> getUserSetByRoleId(Long roleId);

    /**
     * 添加用户-角色关系
     *
     * @param userId
     * @param roleId
     * @return
     */
    Integer save(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 通过用户编号删除用户-角色关系
     *
     * @param userId
     * @return
     */
    Integer removeByUserId(Long userId);
}
