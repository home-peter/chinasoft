package org.chinasoft.dao;

import org.chinasoft.entity.MeetingRoom;

/**
 * Created by zh on 2018/6/25.
 */
public interface MeetingRoomDao {
    int addMeetingRoom(MeetingRoom meetingRoom);
    MeetingRoom getMeetingRoom(MeetingRoom meetingRoom);
}
