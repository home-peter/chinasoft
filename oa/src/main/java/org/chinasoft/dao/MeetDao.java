package org.chinasoft.dao;

import org.chinasoft.entity.Meet;

import java.util.Date;
import java.util.List;

/**
 * Created by zh on 2018/6/25.
 */
public interface MeetDao {
    int addMeet(Meet meet);
    int delMeet(int id);
    int updateMeet(Meet meet);
    Meet getMeetById(int id);
    List<Meet> getAll();
    List<Meet> getCarByMeetTime(Date meetTime);
}
