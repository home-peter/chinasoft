package org.chinasoft.dao;

import org.chinasoft.entity.ApplicationCar;

/**
 * Created by zh on 2018/6/26.
 */
public interface ApplicationCarDao {
    int addApplicationCar(ApplicationCar applicationCar);
}
