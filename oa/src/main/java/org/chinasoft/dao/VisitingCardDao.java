package org.chinasoft.dao;

import org.chinasoft.entity.VisitingCard;

/**
 * Created by zh on 2018/6/26.
 */
public interface VisitingCardDao {
    int addVisitingCard(VisitingCard visitingCard);
}
