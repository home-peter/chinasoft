package org.chinasoft.dao;

import org.chinasoft.entity.Role;

import java.util.Set;

public interface RoleMapper {
    /**
     * 通过角色编号查询
     *
     * @param roleId
     * @return
     */
    Role getByRoleId(Long roleId);

    /**
     * 保存角色
     *
     * @param role
     * @return
     */
    Integer save(Role role);

    /**
     * 更新角色
     *
     * @param role
     * @return
     */
    Integer update(Role role);

    /**
     * 查询所有角色
     *
     * @return
     */
    Set<Role> getAll();
}
