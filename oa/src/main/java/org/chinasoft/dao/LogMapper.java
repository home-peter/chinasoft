package org.chinasoft.dao;

import org.chinasoft.entity.Log;

import java.util.List;

public interface LogMapper {
    Integer save(Log log);

    List<Log> getAll();
}
