package org.chinasoft.dao;

import org.chinasoft.entity.LeaveSlip;

import java.util.List;

/**
 * Created by Fang on 2018/6/25.
 */
public interface LeaveSlipMapper {
     int add(LeaveSlip leaveSlip);
     int delete(int id);
     int update(LeaveSlip leaveSlip);
     LeaveSlip getById(int id);
     List<LeaveSlip> findAll();

}
