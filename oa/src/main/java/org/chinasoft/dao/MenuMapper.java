package org.chinasoft.dao;

import org.chinasoft.entity.Menu;

import java.util.List;

public interface MenuMapper {
    Menu getByMenuId(Long menuId);

    List<Menu> getAll();
}
