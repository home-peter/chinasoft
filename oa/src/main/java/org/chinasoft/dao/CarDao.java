package org.chinasoft.dao;

import org.chinasoft.entity.Car;

import java.util.Date;
import java.util.List;

/**
 * Created by zh on 2018/6/26.
 */
public interface CarDao {
    int addCar(Car car);
    int delCar(int id);
    int updateCar(Car car);
    Car getCarById(int id);
    List<Car> getAll();

}
