import org.chinasoft.entity.VisitingCard;
import org.chinasoft.service.VisitingCardService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * Created by zh on 2018/6/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("applicationContext.xml")
public class VisitingCardServiceTest {
    @Autowired
    private VisitingCardService visitingCardService;

    @Test
    public  void addVisitingCard(){
        VisitingCard visitingCard = new VisitingCard();
        visitingCard.setApproval("1");
        visitingCard.setDepartment("1");
        visitingCard.setName("1");
        visitingCard.setNumber("1");
        visitingCard.setPost("1");
        visitingCard.setRemarks("1");
        visitingCard.setSeatNumber("1");
        visitingCard.setTelephone("1");
        visitingCard.setTime(new Date());
        int i = visitingCardService.addVisitingCard(visitingCard);
        System.out.println(i);
    }
}
