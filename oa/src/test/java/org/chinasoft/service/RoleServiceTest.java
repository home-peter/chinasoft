package org.chinasoft.service;

import org.chinasoft.entity.Menu;
import org.chinasoft.entity.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class RoleServiceTest {
    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;

    @Test
    public void save() {
        Role role = new Role();
        role.setName("部门领导");
        Role save = roleService.save(role);
    }

    @Test
    public void assignAccess() {
        Role role = roleService.getByRoleId(6L);
        List<Menu> menuList = menuService.getAll();
        List<Menu> menus = menuList.subList(0, 1);
        roleService.assignAccess(role, menus);
    }
    @Test
    public void removeByRoleId(){
        roleService.removeByRoleId(1L);
    }
}