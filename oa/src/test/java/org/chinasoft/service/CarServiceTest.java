package org.chinasoft.service;

import org.chinasoft.entity.Car;
import org.chinasoft.service.CarService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by zh on 2018/6/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class CarServiceTest {
    @Autowired
    private CarService carService;

    @Test
    public void addCar(){
        Car car = new Car();
        car.setCarNumber("1");
        car.setCarId(1);
        car.setRemarks("1");
        car.setType("1");
        int i = carService.addCar(car);
        System.out.println(i);
    }

    @Test
    public void delCar(){
        int i = carService.delCar(1);
        System.out.println(i);
    }

    @Test
    public void updateCar(){
        Car car = new Car();
        car.setCarNumber("2");
        car.setCarId(2);
        car.setRemarks("3");
        car.setType("2");
        int i = carService.updateCar(car);
        System.out.println(i);
    }

    @Test
    public void getAll(){
        List<Car> list = carService.getAll();
        for(Car car : list){
            System.out.println(car);
        }
    }
}
