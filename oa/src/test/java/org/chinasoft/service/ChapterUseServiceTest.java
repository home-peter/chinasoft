import org.chinasoft.entity.ChapterUse;
import org.chinasoft.service.ChapterUseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * Created by zh on 2018/6/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("applicationContext.xml")
public class ChapterUseServiceTest {
    @Autowired
    private ChapterUseService chapterUseService;

    @Test
    public void addChapterUse(){
        ChapterUse chapterUse = new ChapterUse();
        chapterUse.setAccount("1");
        chapterUse.setApproval("1");
        chapterUse.setDepartment("1");
        chapterUse.setName("1");
        chapterUse.setTime(new Date());
        chapterUse.setType("1");
        int i = chapterUseService.addChapterUse(chapterUse);
        System.out.println(i);
    }
}
