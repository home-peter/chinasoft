package org.chinasoft.service;

import org.chinasoft.entity.IncomeCertificate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Fang on 2018/6/26.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class IncomeCertificateServiceTest {
    @Autowired
    IncomeCertificateService incomeCertificateService;
    @Test
    public void add() throws Exception {
        IncomeCertificate incomeCertificate =new IncomeCertificate("1",new Date(),"1","1","1","1");
        incomeCertificateService.add(incomeCertificate);
    }

    @Test
    public void delete() throws Exception {
        incomeCertificateService.delete(3);
    }

    @Test
    public void update() throws Exception {
        IncomeCertificate incomeCertificate =new IncomeCertificate(4,"1",new Date(),"1","1","1","1");
        incomeCertificateService.update(incomeCertificate);
    }

    @Test
    public void getById() throws Exception {
        IncomeCertificate incomeCertificate = incomeCertificateService.getById(1);
    }

    @Test
    public void findAll() throws Exception {
        incomeCertificateService.findAll();
    }

}