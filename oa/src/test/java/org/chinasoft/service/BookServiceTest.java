import org.chinasoft.entity.Book;
import org.chinasoft.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * Created by zh on 2018/6/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("applicationContext.xml")
public class BookServiceTest {
    @Autowired
    private BookService bookService;

    @Test
    public void addBook(){
        Book book = new Book();
        book.setApproval("1");
        book.setBookId(1);
        book.setBookName("为了你我愿意热爱整个世界");
        book.setDepartment("1");
        book.setName("1");
        book.setTime(new Date());
        int i = bookService.addBook(book);
        System.out.println(i);
    }
}
