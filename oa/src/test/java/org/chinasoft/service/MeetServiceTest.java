package org.chinasoft.service;

import org.chinasoft.entity.Meet;
import org.chinasoft.service.impl.MeetServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * Created by zh on 2018/6/25.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MeetServiceTest {
    @Autowired
    private MeetServiceImpl meetService;

    @Test
    public void addMeet(){
        Meet meet =new Meet();
        meet.setName("1112");
        meet.setDescribe("111");
        int i = meetService.addMeet(meet);
        System.out.println(i);
    }

    @Test
    public void delMeet(){
        int i = meetService.delMeet(1);
        System.out.println(i);
    }

    @Test
    public void updateMeet(){
        Meet meet = new Meet();
        meet.setId(2);
        meet.setName("zh");
        meet.setDescribe("zh");
        int i = meetService.updateMeet(meet);
        System.out.println(i);
    }

    @Test
    public void getAll(){
        List<Meet> list = meetService.getAll();
        for (Meet meet : list){
            System.out.println(meet);
        }
    }
    @Test
    public void getMeetByMeetTime(){
        List<Meet> list = meetService.getMeetByMeetTime(new Date());
        System.out.println(list);
    }
}
