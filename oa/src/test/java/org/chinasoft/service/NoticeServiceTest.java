package org.chinasoft.service;

import org.chinasoft.entity.Notice;
import org.chinasoft.entity.PageBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * Created by lfy on 2018/6/26.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class NoticeServiceTest {
    @Autowired
    NoticeService noticeService;
    @Test
    public void addNotice() throws Exception {
        Notice notice=new Notice("333","3333",0,new Date(),new Date(),1,new Date(),1);
        System.out.println(noticeService.addNotice(notice));
    }
    @Test
    public void delNotice() throws Exception {
        System.out.println(noticeService.delNoticeById(6L));
    }

    @Test
    public void updateNotice() throws Exception {
        Notice notice=new Notice("G333","G3333",1,new Date(),new Date(),2,new Date(),2);
        notice.setId(5L);
        System.out.println(noticeService.updateNotice(notice));
    }

    @Test
    public void findNotice() throws Exception {
        Notice n= noticeService.findNoticeById(1L);
        System.out.println(n);
    }

    @Test
    public void findAll() throws Exception {
        List<Notice> noticeList= noticeService.findAll();
        for (Notice notice : noticeList) {
            System.out.println(notice);
        }
    }

    @Test
    public void findByPage() throws Exception {
         int currentPage = 1;
         int pageSize=2;
         int flag=1;
        PageBean<Notice> pageBean= noticeService.findByPage(currentPage,pageSize,flag);
        List<Notice> noticeList=pageBean.getLists();
        for (Notice notice:noticeList) {
            System.out.println(notice);
        }
    }


}