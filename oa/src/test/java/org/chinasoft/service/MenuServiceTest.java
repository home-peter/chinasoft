package org.chinasoft.service;

import org.chinasoft.entity.Menu;
import org.chinasoft.vo.MenuTreeNode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MenuServiceTest {
    @Autowired
    private MenuService menuService;

    @Test
    public void getAll() {
    }

    @Test
    public void getByRoleId() {
        Set<Menu> menuSet = menuService.getByRoleId(1L);
    }

    @Test
    public void buildTreeByAllMenuList() {
        List<Menu> menuList = menuService.getAll();
        List<MenuTreeNode> menuTreeNodeList = menuService.buildTreeByAllMenuList(menuList);
    }
}