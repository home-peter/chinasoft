package org.chinasoft.service;

import org.chinasoft.entity.TravelRequests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Fang on 2018/6/26.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TravelRequestsServiceTest {
    @Autowired
    TravelRequestsService travelRequestsService;
    @Test
    public void add() throws Exception {
        TravelRequests travelRequests =new TravelRequests("1","1",new Date(),new Date(),"1","1","1","1",1,"1");
        travelRequestsService.add(travelRequests);
    }

    @Test
    public void delete() throws Exception {
        travelRequestsService.delete(3);
    }

    @Test
    public void update() throws Exception {
        TravelRequests travelRequests =new TravelRequests(2,"1","1",new Date(),new Date(),"1","1","1","1",1,"1");
        travelRequestsService.update(travelRequests);
    }

    @Test
    public void getById() throws Exception {
        TravelRequests travelRequests = travelRequestsService.getById(1);
        System.out.println(travelRequests);
    }

    @Test
    public void findAll() throws Exception {
        travelRequestsService.findAll();
    }

}