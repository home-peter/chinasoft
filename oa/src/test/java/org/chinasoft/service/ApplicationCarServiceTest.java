import org.chinasoft.entity.ApplicationCar;
import org.chinasoft.service.ApplicationCarService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * Created by zh on 2018/6/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("applicationContext.xml")
public class ApplicationCarServiceTest {
    @Autowired
    private ApplicationCarService applicationCarService;

    @Test
    public void addApplicationCar(){
        ApplicationCar applicationCar = new ApplicationCar();
        applicationCar.setAccount("1");
        applicationCar.setApproval("1");
        applicationCar.setBeginTime(new Date());
        applicationCar.setCarId(1);
        applicationCar.setCarMan("1");
        applicationCar.setDepartment("1");
        applicationCar.setDestination("1");
        applicationCar.setEntourage("1");
        applicationCar.setOverTime(new Date());
        applicationCar.setDriver("1");
        applicationCar.setDistance(1);
        int i = applicationCarService.addApplicationCar(applicationCar);
        System.out.println(i);
    }
}
