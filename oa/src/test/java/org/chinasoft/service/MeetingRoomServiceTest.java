package org.chinasoft.service;

import org.chinasoft.entity.MeetingRoom;
import org.chinasoft.service.impl.MeetingRoomServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zh on 2018/6/25.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MeetingRoomServiceTest {
   @Autowired
    private MeetingRoomServiceImpl meetService;

   @Test
    public void addMeetingRoom(){
       MeetingRoom meetingRoom = new MeetingRoom();
       meetingRoom.setName("1");
       meetingRoom.setReservations("1");
       meetingRoom.setRecorder("1");
       meetingRoom.setType("1");
       int i = meetService.addMeetingRoom(meetingRoom);
       System.out.println(i);
   }

   @Test
   public void getMeetingRoom(){
       MeetingRoom meetingRoom = new MeetingRoom();
       //meetingRoom.setMeetTime();
       meetingRoom.setType("1");
       meetingRoom.setName("1");
       meetingRoom.setReservations("1");
      MeetingRoom meetingRoom1 = meetService.getMeetingRoom(meetingRoom);
      System.out.println(meetingRoom1);
   }
   @Test
    public void getTime() throws ParseException {
       //Date currentTime = new Date("2018-01-01");
       //System.out.println(currentTime);
       SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
       Date parse = formatter.parse("2018-01-01");

       System.out.println(parse);

   }
}
