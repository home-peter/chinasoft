package org.chinasoft.service;

import org.chinasoft.entity.LeaveSlip;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Fang on 2018/6/26.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class LeaveSlipServiceTest {
    @Autowired
    LeaveSlipService   leaveSlipService;
    @Test
    public void add() throws Exception {
        LeaveSlip leaveSlip =new LeaveSlip("1","1",new Date(),new Date(),1,"1","1","1");
        leaveSlipService.add(leaveSlip);
    }

    @Test
    public void delete() throws Exception {
        leaveSlipService.delete(3);
    }

    @Test
    public void update() throws Exception {
        LeaveSlip leaveSlip =new LeaveSlip(2,"1","1",new Date(),new Date(),1,"1","1","1");
        leaveSlipService.update(leaveSlip);
    }

    @Test
    public void getById() throws Exception {
        LeaveSlip leaveSlip = leaveSlipService.getById(1);
        System.out.println(leaveSlip);
    }

    @Test
    public void findAll() throws Exception {
        leaveSlipService.findAll();
    }

}