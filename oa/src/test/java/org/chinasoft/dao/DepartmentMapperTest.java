package org.chinasoft.dao;

import org.chinasoft.entity.Department;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class DepartmentMapperTest {
    @Autowired
    private DepartmentMapper departmentMapper;

    @Test
    public void getByDepartmentId() {
        Department department = departmentMapper.getByDepartmentId(1L);
    }

    @Test
    public void getAll() {
        List<Department> departmentList = departmentMapper.getAll();
    }
}