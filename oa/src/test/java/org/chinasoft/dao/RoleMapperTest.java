package org.chinasoft.dao;

import org.chinasoft.entity.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class RoleMapperTest {
    @Autowired
    private RoleMapper roleMapper;

    @Test
    public void getByRoleId() {
        Role role = roleMapper.getByRoleId(1L);
    }

    @Test
    public void save() {
        Role role = new Role();
        role.setName("系统管理员");

        roleMapper.save(role);
    }

    @Test
    public void update() {
        Role role = roleMapper.getByRoleId(3L);
        role.setName("人事专员");
        roleMapper.update(role);
    }

    @Test
    public void getAll() {
        Set<Role> roleSet = roleMapper.getAll();
    }
}