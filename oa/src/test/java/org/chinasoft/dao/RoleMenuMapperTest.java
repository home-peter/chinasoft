package org.chinasoft.dao;

import org.chinasoft.entity.Menu;
import org.chinasoft.entity.RoleMenu;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class RoleMenuMapperTest {
    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Test
    public void getByRoleMenuId() {
        RoleMenu roleMenu = roleMenuMapper.getByRoleMenuId(1L);
    }

    @Test
    public void getMenuSetByRoleId() {
        Set<Menu> menuSet = roleMenuMapper.getMenuSetByRoleId(1L);
    }

    @Test
    public void save() {
        roleMenuMapper.save(1L, 2L);
    }

    @Test
    public void removeByRoleId() {
        roleMenuMapper.removeByRoleId(3L);
    }
}