package org.chinasoft.dao;

import org.chinasoft.entity.Menu;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MenuMapperTest {
    @Autowired
    private MenuMapper menuMapper;

    @Test
    public void getByMenuId() {
        Menu menu = menuMapper.getByMenuId(1L);
    }

    @Test
    public void getAll() {
        List<Menu> menuList = menuMapper.getAll();
    }
}