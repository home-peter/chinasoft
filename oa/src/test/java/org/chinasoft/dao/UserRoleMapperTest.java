package org.chinasoft.dao;

import org.chinasoft.entity.Role;
import org.chinasoft.entity.User;
import org.chinasoft.entity.UserRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UserRoleMapperTest {
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Test
    public void getByUserRoleId() {
        UserRole userRole = userRoleMapper.getByUserRoleId(1L);
    }

    @Test
    public void getUserRoleByUserId() {
        Set<Role> roles = userRoleMapper.getRoleSetByUserId(1L);
        for (Role role : roles) {
            System.out.println(role.getName());
        }
    }

    @Test
    public void getUserRoleByRoleId() {
        Set<User> userRoles = userRoleMapper.getUserSetByRoleId(1L);
        for (User user : userRoles) {
            System.out.println(user.getRealName());
        }
    }

    @Test
    public void save() {
        userRoleMapper.save(1L, 3L);
    }
}