package org.chinasoft.dao;

import org.chinasoft.entity.LeaveSlip;
import org.chinasoft.entity.TravelRequests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * Created by Fang on 2018/6/25.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class LeaveSlipMapperTest {
    @Autowired
    private LeaveSlipMapper leaveSlipMapper;

    @Test
    public void getById() {
        LeaveSlip leaveSlip = leaveSlipMapper.getById(1);
        System.out.println(leaveSlip);
    }

    @Test
    public void add(){
        LeaveSlip leaveSlip =new LeaveSlip("1","1",new Date(),new Date(),1,"1","1","1");
        leaveSlipMapper.add(leaveSlip);
    }

    @Test
    public void delete(){
        leaveSlipMapper.delete(2);
    }

    @Test
    public void update(){
        LeaveSlip leaveSlip =new LeaveSlip(2,"1","1",new Date(),new Date(),1,"1","1","1");
        leaveSlipMapper.update(leaveSlip);
    }

    @Test
    public void findAll(){
        leaveSlipMapper.findAll();
    }
}
