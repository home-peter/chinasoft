package org.chinasoft.dao;

import org.chinasoft.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class UserMapperTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void getByUserId() {
        User user = userMapper.getByUserId(1L);
    }

    @Test
    public void save() {
        User user = new User();
        user.setUsername("abc");
        Integer save = userMapper.save(user);
    }

    @Test
    public void countByUsername() {
        Integer count = userMapper.countByUsername("zmy");
    }

    @Test
    public void getByUsername() {
        User user = userMapper.getByUsername("zmy");
    }

    @Test
    public void queryByUsername() {
        List<User> userList = userMapper.queryByUsername("x");
    }

    @Test
    public void queryByRealName() {
        List<User> userList = userMapper.queryByRealName("张明远");
    }

    @Test
    public void getAll() {
        List<User> userList = userMapper.getAll();
    }

    @Test
    public void update() {
        User user = userMapper.getByUserId(1L);
        user.setRealName("张飞");
        user.getJob().setJobId(2L);

        userMapper.update(user);
    }
}