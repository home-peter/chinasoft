package org.chinasoft.dao;

import org.chinasoft.entity.Job;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class JobMapperTest {
    @Autowired
    private JobMapper jobMapper;

    @Test
    public void getByJobId() {
        Job job = jobMapper.getByJobId(1L);
    }

    @Test
    public void getByDepartmentId() {
        List<Job> jobList = jobMapper.getByDepartmentId(1L);
    }
}