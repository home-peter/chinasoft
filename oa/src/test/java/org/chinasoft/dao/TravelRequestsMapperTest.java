package org.chinasoft.dao;

import org.chinasoft.entity.TravelRequests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * Created by Fang on 2018/6/25.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TravelRequestsMapperTest {
    @Autowired
    private TravelRequestsMapper travelRequestsMapper;

    @Test
    public void getById() {
        TravelRequests travelRequests = travelRequestsMapper.getById(1);
        System.out.println(travelRequests);
    }

    @Test
    public void add(){
        TravelRequests travelRequests =new TravelRequests("1","1",new Date(),new Date(),"1","1","1","1",1,"1");
        travelRequestsMapper.add(travelRequests);
    }

    @Test
    public void delete(){
         travelRequestsMapper.delete(2);
    }

    @Test
    public void update(){
        TravelRequests travelRequests =new TravelRequests(2,"1","1",new Date(),new Date(),"1","1","1","1",1,"1");
        travelRequestsMapper.update(travelRequests);
    }

    @Test
    public void findAll(){
        travelRequestsMapper.findAll();
    }
}
