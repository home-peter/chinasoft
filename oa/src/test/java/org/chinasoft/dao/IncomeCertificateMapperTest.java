package org.chinasoft.dao;

import org.chinasoft.entity.IncomeCertificate;
import org.chinasoft.entity.TravelRequests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;


@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class IncomeCertificateMapperTest {
    @Autowired
    private IncomeCertificateMapper incomeCertificateMapper;

    @Test
    public void getById() {
        IncomeCertificate incomeCertificate = incomeCertificateMapper.getById(1);
    }

    @Test
    public void add(){
        IncomeCertificate incomeCertificate =new IncomeCertificate("1",new Date(),"1","1","1","1");
        incomeCertificateMapper.add(incomeCertificate);
    }

    @Test
    public void delete(){
        incomeCertificateMapper.delete(2);
    }

    @Test
    public void update(){
        IncomeCertificate incomeCertificate =new IncomeCertificate(2,"1",new Date(),"1","1","1","1");
        incomeCertificateMapper.update(incomeCertificate);
    }

    @Test
    public void findAll(){
        incomeCertificateMapper.findAll();
    }

}