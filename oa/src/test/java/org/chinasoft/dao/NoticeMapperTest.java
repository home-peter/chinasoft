package org.chinasoft.dao;

import org.chinasoft.entity.Notice;
import org.chinasoft.entity.PageBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lfy on 2018/6/25.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class NoticeMapperTest {
    @Autowired
    private NoticeMapper noticeMapper;
    @Test
    public void testFindNotice(){
        Notice n= noticeMapper.findNoticeById(1L);
        System.out.println(n);
    }
    @Test
    public void testAddNotice(){
        Notice notice=new Notice("333","3333",0,new Date(),new Date(),1,new Date(),1);
        System.out.println(noticeMapper.addNotice(notice));
    }
    @Test
    public void testUpdateNotice(){
        Notice notice=new Notice("G333","G3333",1,new Date(),new Date(),2,new Date(),2);
        notice.setId(3L);
        System.out.println(noticeMapper.updateNotice(notice));
    }
    @Test
    public void testFindAll(){
        List<Notice> noticeList= noticeMapper.findAll();
        for (Notice notice : noticeList) {
            System.out.println(notice);
        }
    }
    @Test
    public void testDelNotice(){
        System.out.println(noticeMapper.delNoticeById(3L));
    }
    @Test
    public void testGetCount(){
        System.out.println(noticeMapper.getCount());
    }
    @Test
    public void testFindByPage(){
        HashMap<String,Object> map=new HashMap<String, Object>();
        map.put("start",0);
        map.put("pageSize",2);
        map.put("flag",1);
        List<Notice> noticeList= noticeMapper.findByPage(map);
        for (Notice notice:noticeList) {
            System.out.println(notice);
        }
    }
    @Test
    public void testFindNoticeAndUser(){
        List<Notice> noticeList=noticeMapper.findAllNoticeAndUser();
        for (Notice notice:noticeList
             ) {
            System.out.println(notice);

        }
    }
}
